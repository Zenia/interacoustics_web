<?php

use App\Tenant;
use Illuminate\Database\Seeder;

class TenantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      if ( Tenant::count() == 0 )
      {
        Tenant::insert([ [ 'name' => 'Makers'
                         ],
                         [ 'name' => 'Test'
                         ]
        ]);
      }
    }
}
