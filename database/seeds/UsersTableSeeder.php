<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $id = ($role = Role::where('name', 'superadmin')->firstOrFail()) ? $role->id: 1;

            User::create([
                'name'           => 'superadmin',
                'email'          => 'admin@admin.com',
                'tenant_id'   => 0,
                'password'       => bcrypt('password'),
                'remember_token' => str_random(60),
                'role_id'        => $id,
            ]);
        }
    }
}
