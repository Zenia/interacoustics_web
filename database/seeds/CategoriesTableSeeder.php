<?php

use App\Tenant;
use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
      foreach ( Tenant::all() as $tenant )
      {
    
        $category = Category::firstOrNew([ 'slug' => 'news',
        ]);
        if ( ! $category->exists )
        {
          $category->fill([ 'name'      => 'News',
                            'tenant_id' => $tenant->id,
          ])->save();
        }
    
        $category = Category::firstOrNew([ 'slug' => 'offers',
        ]);
        if ( ! $category->exists )
        {
          $category->fill([ 'name'      => 'Offers',
                            'tenant_id' => $tenant->id,
      
          ])->save();
        }
      }
    }
}
