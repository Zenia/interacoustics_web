<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandModelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('brand_models', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('slug');
			$table->string('name');
			$table->integer('brand_id');
			$table->timestamps();
			$table->integer('tenant_id')->unsigned()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brand_models');
	}

}
