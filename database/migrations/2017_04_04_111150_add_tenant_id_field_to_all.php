<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTenantIdFieldToAll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::table('categories', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('pages', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('users', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('posts', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('permission_groups', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('data_rows', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('data_types', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('menu_items', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('menus', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('password_resets', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->before('email');
    });
    Schema::table('permissions', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('roles', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('settings', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
    Schema::table('translations', function (Blueprint $table) {
      $table->integer('tenant_id')->default(1)->after('id');
    });
  }
  
  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('categories', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('pages', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('posts', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('permission_groups', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('data_rows', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('data_types', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('menu_items', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('menus', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('password_resets', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });

    Schema::table('permissions', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('roles', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('settings', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
    Schema::table('translations', function (Blueprint $table) {
      $table->dropColumn('tenant_id');
    });
  }
}
