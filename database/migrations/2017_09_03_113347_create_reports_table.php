<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('slug')->nullable();
			$table->string('name')->nullable();
			$table->integer('user_id')->nullable();
			$table->string('image')->nullable();
			$table->text('description')->nullable();
			$table->boolean('created_at')->nullable();
			$table->boolean('updated_at')->nullable();
			$table->string('barcode')->nullable();
			$table->integer('brand_id')->nullable();
			$table->integer('model_id')->nullable();
			$table->integer('tenant_id')->unsigned()->default(1);
			$table->boolean('accepted')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reports');
	}

}
