<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrandSubBrandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('brand_sub_brand', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('brand_id')->unsigned()->index();
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
			$table->integer('sub_brand_id')->unsigned()->index();
			$table->foreign('sub_brand_id')->references('id')->on('sub_brands')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('brand_sub_brand');
	}

}
