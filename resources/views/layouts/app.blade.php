<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="welcome">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <title>@yield('page_title',env('APP_NAME'))</title>
  
  <link rel="shortcut icon" href="storage/{{Voyager::setting('fav_icon')}}" type="image/x-icon">
  <link rel="icon" href="storage/{{Voyager::setting('fav_icon')}}" type="image/x-icon">
  
  <script type="text/javascript" src="{{ voyager_asset('lib/js/jquery.min.js') }}"></script>
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/toastr.min.css') }}">
  
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="{{ voyager_asset('lib/css/bootstrap.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ voyager_asset('css/bootstrap-toggle.min.css') }}">
  <link rel="stylesheet" type="text/css"
        href="{{ voyager_asset('js/datetimepicker/bootstrap-datetimepicker.min.css') }}">
  
  <link rel="stylesheet" href="{{ voyager_asset('fonts/voyager/styles.css') }}">
  
  
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  @stack('css')
</head>
<body class="welcome">

<div id="app"
     @if((Auth::check()) && ($brand = Auth::user()->brand) && (isset($brand->color)))
     style="background: {{$brand->color}};
     @if($brand->gradient)
         background: -moz-linear-gradient(top, {{$brand->color}} 0%, {{$brand->bottom_color}} 100%);
         background: -webkit-linear-gradient(top, {{$brand->color}} 0%,{{$brand->bottom_color}} 100%);
         background: linear-gradient(to bottom, {{$brand->color}} 0%,{{$brand->bottom_color}} 100%);
         filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='{{$brand->color}}', endColorstr='{{$brand->bottom_color}}',GradientType=0 );
     @endif
         "
     @else
     style="background: #3c5064;
  background: -moz-linear-gradient(top, #3c5064 0%, #5a5a5a 100%);
  background: -webkit-linear-gradient(top, #3c5064 0%,#5a5a5a 100%);
  background: linear-gradient(to bottom, #3c5064 0%,#5a5a5a 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3c5064', endColorstr='#5a5a5a',GradientType=0 );"
    @endif
>
  {{--@include('vendor.voyager.partials.app-footer')--}}
  
  <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        
        <!-- Collapsed Hamburger -->
        @if(Auth::check())
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                  data-target="#app-navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      @endif
      
      <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
          @if((Auth::check()) && ($brand = Auth::user()->brand) && (isset($brand->logo)))
            <img src="{{ Voyager::image($brand->logo)}}">
          @else
            @yield('special_logo')
            <img src="storage/{{Voyager::setting('logo')}}">
          @endif
          {{--{{ config('app.name', 'Laravel') }}--}}
        </a>
      </div>
      
      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
      {{--<ul class="nav navbar-nav">--}}
      {{--&nbsp;--}}
      {{--</ul>--}}
      
      <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">
          <!-- Authentication Links -->
          @if (Auth::guest())
            {{--<li><a href="{{ route('login') }}">Login</a></li>--}}
          @else
            {{--<li class="dropdown">--}}
            {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
            {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
            {{--</a>--}}
            
            {{--<ul class="dropdown-menu" role="menu">--}}
            <li><a href="{{ url('/user/reports/create') }}">Create new Report</a></li>
            <li><a href="{{ url('/user/reports') }}">View my Reports</a></li>
            <li><a href="{{ url('/user/users/'.Auth::id().'/edit') }}">My Profile</a></li>
            
            <li>
              <a href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                Logout
              </a>
              
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </li>
            {{--</ul>--}}
            {{--</li>--}}
          @endif
        </ul>
      </div>
    </div>
  </nav>
  
  @yield('content')


</div>

<script type="text/javascript" src="{{ voyager_asset('lib/js/toastr.min.js') }}"></script>
{{--<script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>--}}
<script type="text/javascript" src="{{ voyager_asset('js/helpers.js') }}"></script>

<script>
      @if(Session::has('alerts'))
  let alerts = {!! json_encode(Session::get('alerts')) !!};

  displayAlerts(alerts, toastr);
  @endif
  
  @if(Session::has('message'))

  // TODO: change Controllers to use AlertsMessages trait... then remove this
  var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
  var alertMessage = {!! json_encode(Session::get('message')) !!};
  var alerter = toastr[alertType];

  if (alerter) {
    alerter(alertMessage);
  } else {
    toastr.error("toastr alert-type " + alertType + " is unknown");
  }
  
  @endif
</script>
@stack('scripts')
@yield('javascript')

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
