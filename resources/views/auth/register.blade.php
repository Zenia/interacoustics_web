@extends('layouts.app')

@push('css')
  <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
@endpush

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-12">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
          {{ csrf_field() }}
          
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="sr-only">Name</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-person"></i></div>
              <input id="name" placeholder="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required
                     autofocus>
            
            
            </div>
          </div>
          @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
          @endif
          
          
          <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
            <label for="company" class="sr-only">Company</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-company"></i></div>
              <input id="company" placeholder="company" type="text" class="form-control" name="company" value="{{ old('company') }}" required
                     autofocus>
            
            
            </div>
          </div>
          @if ($errors->has('company'))
            <span class="help-block">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
          @endif
          
          
          <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address" class="sr-only">Address</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-home"></i></div>
              <input id="address" placeholder="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required
                     autofocus>
            
            
            </div>
          </div>
          @if ($errors->has('address'))
            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
          @endif
          
          
          <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
            <label for="country" class="sr-only">Country</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-bookmark"></i></div>
              <input id="country" placeholder="country" type="text" class="form-control" name="country" value="{{ old('country') }}" required
                     autofocus>
            
            
            </div>
          </div>
          @if ($errors->has('country'))
            <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
          @endif
          
          
          <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
            <label for="phone" class="sr-only">Phone</label>
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-phone"></i></div>
              <input id="phone" placeholder="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required
                     autofocus>
            
            
            </div>
          </div>
          @if ($errors->has('phone'))
            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
          @endif
          
          
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="sr-only" for="email">E-Mail Address</label>
            
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-mail"></i></div>
              <input id="email" type="email" placeholder="email" class="form-control" name="email" value="{{ old('email') }}" required
                     autofocus>
            </div>
          </div>
          @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
          @endif
          
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            
            <label for="password" class="sr-only">Password</label>
            <div class="input-group">
              <div class="input-group-addon clear"><i class="voyager-lock"></i></div>
              <input id="password" type="password" class="form-control" name="password" placeholder="password" required>
            </div>
          </div>
          @if ($errors->has('password'))
            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
          @endif
          
          <div class="form-group">
            <label for="password-confirm" class="sr-only">Confirm Password</label>
            
            <div class="input-group">
              <div class="input-group-addon clear"><i class="voyager-lock"></i></div>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="confirm password" required>
            </div>
          </div>
          
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3 col-sm-12">
              <button type="submit" class="btn btn-primary btn-block">Send Request
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
