@extends('layouts.app')

@push('css')
  <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
  <link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endpush

@section('special_logo')
  <img class="special_logo" src="storage/{{Voyager::setting('special_logo')}}">
  @stop

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-sm-12">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('voyager.postlogin') }}">
          {{ csrf_field() }}
          
          
          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label class="sr-only" for="email">E-Mail Address</label>
            
            <div class="input-group">
              <div class="input-group-addon"><i class="voyager-mail"></i></div>
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                     autofocus>
            </div>
          </div>
          @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
          @endif
          
          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            
            <label for="password" class="sr-only">Password</label>
            <div class="input-group">
              <div class="input-group-addon clear"><i class="voyager-lock"></i></div>
              <input id="password" type="password" class="form-control" name="password" required>
            </div>
          </div>
          @if ($errors->has('password'))
            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
          @endif
          {{--<div class="form-group">--}}
          {{--<div class="col-md-6 col-md-offset-4">--}}
          {{--                                <a href="{{ url('/auth/github') }}" class="btn btn-github"><i class="fa fa-github"></i> Github</a>--}}
          {{--                                <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>--}}
          {{--<a href="{{ url('/login/facebook') }}" class="btn btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>--}}
          {{--</div>--}}
          {{--</div>--}}
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3 col-sm-12">
              <button type="submit" class="btn btn-default btn-block">
                Sign in
              </button>
            </div>
          </div>
          <div class="col-md-6 col-md-offset-3 col-sm-12 smallertext">
            <a class="btn btn-link btn-block btn-bottom" href="{{ route('password.request') }}">
              Forgot Your Password?
            </a><br>
            <a class="btn btn-link btn-block btn-bottom"
               href="{{ url('/register') }}"><span>Don´t have and account?</span>  <strong>Request one</strong></a>
          
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
