@extends('layouts.app')

@push('css')
  <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">
@endpush
@section('special_logo')
  <img class="special_logo" src="storage/{{Voyager::setting('special_logo')}}">
@stop
@section('content')
            <div class="content">
              @include('vendor.voyager.alerts')
  
              <div class="welcome-links links">
                    @if (Auth::check())
                      
                      <a href="{{ url('/user/reports/create') }}">Create new Report</a>
                      <a href="{{ url('/user/reports') }}">View my Reports</a>
                      <a href="{{ url('/user/users/'.Auth::id().'/edit') }}">My Profile</a>
                      <a href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Logout
                      </a>
      
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                    @else
                      <a href="{{ url('/login') }}">Login</a>
                    @endif
                </div>
            </div>
@endsection