@extends('emails.email')

@section('content')
  <table>
    <thead>
    <tr>
      <th>Subject:</th>
      <th>{{$subject}}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>Message</td>
      <td>{{$text}}</td>
    </tr>
    <tr>
      <th colspan="2">From:</th>
    </tr>
    <tr>
      <td>Name:</td>
      <td>{{$user->name}}</td>
    </tr>
    <tr>
      <td>Email:</td>
      <td>{{$user->email}}</td>
    </tr>
    @if($user->phone != null)
      <tr>
        <td>Phone:</td>
        <td>{{$user->phone}}</td>
      </tr>
    @endif
    <tr>
      <td>Brand:</td>
      <td>{{$user->brand->name}}</td>
    </tr>
    @if($user->company != null)
      <tr>
        <td>Company:</td>
        <td>{{$user->company}}</td>
      </tr>
    @endif
    </tbody>
  </table>
@endsection
