@extends('emails.email')

@section('content')
  <table>
    <thead>
    <tr>
      <th style="font-weight: bolder;">Subject:</th>
      <th>Brand change</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td style="font-weight: bolder;">Message</td>
      <td>User {{$user->name}} has a requested a brand change from {{$user->brand->name}} to {{$brand->name}}</td>
    </tr>
    <tr>
      <td style="font-weight: bolder;">Name:</td>
      <td>{{$user->name}}</td>
    </tr>
    <tr>
      <td style="font-weight: bolder;">Email:</td>
      <td>{{$user->email}}</td>
    </tr>
    @if($user->phone != null)
      <tr>
        <td style="font-weight: bolder;">Phone:</td>
        <td>{{$user->phone}}</td>
      </tr>
    @endif
    <tr>
      <td style="font-weight: bolder;">Current Brand:</td>
      <td>{{$user->brand->name}}</td>
    </tr>
    <tr>
      <td style="font-weight: bolder;">Requested Brand:</td>
      <td>{{$brand->name}}</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
      <td colspan="2"><a href="{{config('app.url')}}">{{config('app.name')}}</a></td>
    </tr>
    </tfoot>
  </table>
@endsection
