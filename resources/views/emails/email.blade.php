<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>{{(isset($title)) ? $title : env('COMPANY')}}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    /* Prevent WebKit and Windows mobile changing default text sizes */
    /* Allow smoother rendering of resized image in Internet Explorer */
    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
      font-size: inherit !important;
      font-family: inherit !important;
      font-weight: inherit !important;
      line-height: inherit !important;
    }
    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {
      .wrapper {
        width: 100% !important;
        max-width: 100% !important;
      }
      /* ADJUSTS LAYOUT OF LOGO IMAGE */
      .logo img {
        margin: 0 auto !important;
      }
      /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
      .mobile-hide {
        display: none !important;
      }
      .img-max {
        max-width: 100% !important;
        width: 100% !important;
        height: auto !important;
      }
      /* FULL-WIDTH TABLES */
      .responsive-table {
        width: 100% !important;
      }
      .padding {
        padding: 10px 5% 15px 5% !important;
      }
      .padding-meta {
        padding: 30px 5% 0px 5% !important;
        text-align: center;
      }
      .no-padding {
        padding: 0 !important;
      }
      .section-padding {
        padding: 50px 15px 50px 15px !important;
      }
      /* ADJUST BUTTONS ON MOBILE */
      .mobile-button-container {
        margin: 0 auto;
        width: 100% !important;
      }
      .mobile-button {
        padding: 15px !important;
        border: 0 !important;
        font-size: 16px !important;
        display: block !important;
      }
    }
    /* ANDROID CENTER FIX */
    @media only screen and (max-width: 596px) {
      .small-float-center {
        margin: 0 auto !important;
        float: none !important;
        text-align: center !important;
      }
      .small-text-center {
        text-align: center !important;
      }
      .small-text-left {
        text-align: left !important;
      }
      .small-text-right {
        text-align: right !important;
      }
    }

    @media only screen and (max-width: 596px) {
      table.body table.container .hide-for-large {
        display: block !important;
        width: auto !important;
        overflow: visible !important;
      }
    }

    @media only screen and (max-width: 596px) {
      table.body table.container .row.hide-for-large,
      table.body table.container .row.hide-for-large {
        display: table !important;
        width: 100% !important;
      }
    }

    @media only screen and (max-width: 596px) {
      table.body table.container .show-for-large {
        display: none !important;
        width: 0;
        mso-hide: all;
        overflow: hidden;
      }
    }

    @media only screen and (max-width: 596px) {
      table.body img {
        width: auto !important;
        height: auto !important;
      }
      table.body center {
        min-width: 0 !important;
      }
      table.body .container {
        width: 95% !important;
      }
      table.body .columns,
      table.body .column {
        height: auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding-left: 16px !important;
        padding-right: 16px !important;
      }
      table.body .columns .column,
      table.body .columns .columns,
      table.body .column .column,
      table.body .column .columns {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      table.body .collapse .columns,
      table.body .collapse .column {
        padding-left: 0 !important;
        padding-right: 0 !important;
      }
      td.small-1,
      th.small-1 {
        display: inline-block !important;
        width: 8.33333% !important;
      }
      td.small-2,
      th.small-2 {
        display: inline-block !important;
        width: 16.66667% !important;
      }
      td.small-3,
      th.small-3 {
        display: inline-block !important;
        width: 25% !important;
      }
      td.small-4,
      th.small-4 {
        display: inline-block !important;
        width: 33.33333% !important;
      }
      td.small-5,
      th.small-5 {
        display: inline-block !important;
        width: 41.66667% !important;
      }
      td.small-6,
      th.small-6 {
        display: inline-block !important;
        width: 50% !important;
      }
      td.small-7,
      th.small-7 {
        display: inline-block !important;
        width: 58.33333% !important;
      }
      td.small-8,
      th.small-8 {
        display: inline-block !important;
        width: 66.66667% !important;
      }
      td.small-9,
      th.small-9 {
        display: inline-block !important;
        width: 75% !important;
      }
      td.small-10,
      th.small-10 {
        display: inline-block !important;
        width: 83.33333% !important;
      }
      td.small-11,
      th.small-11 {
        display: inline-block !important;
        width: 91.66667% !important;
      }
      td.small-12,
      th.small-12 {
        display: inline-block !important;
        width: 100% !important;
      }
      .columns td.small-12,
      .column td.small-12,
      .columns th.small-12,
      .column th.small-12 {
        display: block !important;
        width: 100% !important;
      }
      .body .columns td.small-1,
      .body .column td.small-1,
      td.small-1 center,
      .body .columns th.small-1,
      .body .column th.small-1,
      th.small-1 center {
        display: inline-block !important;
        width: 8.33333% !important;
      }
      .body .columns td.small-2,
      .body .column td.small-2,
      td.small-2 center,
      .body .columns th.small-2,
      .body .column th.small-2,
      th.small-2 center {
        display: inline-block !important;
        width: 16.66667% !important;
      }
      .body .columns td.small-3,
      .body .column td.small-3,
      td.small-3 center,
      .body .columns th.small-3,
      .body .column th.small-3,
      th.small-3 center {
        display: inline-block !important;
        width: 25% !important;
      }
      .body .columns td.small-4,
      .body .column td.small-4,
      td.small-4 center,
      .body .columns th.small-4,
      .body .column th.small-4,
      th.small-4 center {
        display: inline-block !important;
        width: 33.33333% !important;
      }
      .body .columns td.small-5,
      .body .column td.small-5,
      td.small-5 center,
      .body .columns th.small-5,
      .body .column th.small-5,
      th.small-5 center {
        display: inline-block !important;
        width: 41.66667% !important;
      }
      .body .columns td.small-6,
      .body .column td.small-6,
      td.small-6 center,
      .body .columns th.small-6,
      .body .column th.small-6,
      th.small-6 center {
        display: inline-block !important;
        width: 50% !important;
      }
      .body .columns td.small-7,
      .body .column td.small-7,
      td.small-7 center,
      .body .columns th.small-7,
      .body .column th.small-7,
      th.small-7 center {
        display: inline-block !important;
        width: 58.33333% !important;
      }
      .body .columns td.small-8,
      .body .column td.small-8,
      td.small-8 center,
      .body .columns th.small-8,
      .body .column th.small-8,
      th.small-8 center {
        display: inline-block !important;
        width: 66.66667% !important;
      }
      .body .columns td.small-9,
      .body .column td.small-9,
      td.small-9 center,
      .body .columns th.small-9,
      .body .column th.small-9,
      th.small-9 center {
        display: inline-block !important;
        width: 75% !important;
      }
      .body .columns td.small-10,
      .body .column td.small-10,
      td.small-10 center,
      .body .columns th.small-10,
      .body .column th.small-10,
      th.small-10 center {
        display: inline-block !important;
        width: 83.33333% !important;
      }
      .body .columns td.small-11,
      .body .column td.small-11,
      td.small-11 center,
      .body .columns th.small-11,
      .body .column th.small-11,
      th.small-11 center {
        display: inline-block !important;
        width: 91.66667% !important;
      }
      table.body td.small-offset-1,
      table.body th.small-offset-1 {
        margin-left: 8.33333% !important;
        Margin-left: 8.33333% !important;
      }
      table.body td.small-offset-2,
      table.body th.small-offset-2 {
        margin-left: 16.66667% !important;
        Margin-left: 16.66667% !important;
      }
      table.body td.small-offset-3,
      table.body th.small-offset-3 {
        margin-left: 25% !important;
        Margin-left: 25% !important;
      }
      table.body td.small-offset-4,
      table.body th.small-offset-4 {
        margin-left: 33.33333% !important;
        Margin-left: 33.33333% !important;
      }
      table.body td.small-offset-5,
      table.body th.small-offset-5 {
        margin-left: 41.66667% !important;
        Margin-left: 41.66667% !important;
      }
      table.body td.small-offset-6,
      table.body th.small-offset-6 {
        margin-left: 50% !important;
        Margin-left: 50% !important;
      }
      table.body td.small-offset-7,
      table.body th.small-offset-7 {
        margin-left: 58.33333% !important;
        Margin-left: 58.33333% !important;
      }
      table.body td.small-offset-8,
      table.body th.small-offset-8 {
        margin-left: 66.66667% !important;
        Margin-left: 66.66667% !important;
      }
      table.body td.small-offset-9,
      table.body th.small-offset-9 {
        margin-left: 75% !important;
        Margin-left: 75% !important;
      }
      table.body td.small-offset-10,
      table.body th.small-offset-10 {
        margin-left: 83.33333% !important;
        Margin-left: 83.33333% !important;
      }
      table.body td.small-offset-11,
      table.body th.small-offset-11 {
        margin-left: 91.66667% !important;
        Margin-left: 91.66667% !important;
      }
      table.body table.columns td.expander,
      table.body table.columns th.expander {
        display: none !important;
      }
      table.body .right-text-pad,
      table.body .text-pad-right {
        padding-left: 10px !important;
      }
      table.body .left-text-pad,
      table.body .text-pad-left {
        padding-right: 10px !important;
      }
      table.menu {
        width: 100% !important;
      }
      table.menu td,
      table.menu th {
        width: auto !important;
        display: inline-block !important;
      }
      table.menu.vertical td,
      table.menu.vertical th,
      table.menu.small-vertical td,
      table.menu.small-vertical th {
        display: block !important;
      }
      table.menu[align="center"] {
        width: auto !important;
      }
      table.button.expand {
        width: 100% !important;
      }
    }

    @media print {
      *,
      *:before,
      *:after {
        color: #000 !important;
        text-shadow: none !important;
        background: transparent !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
      }
      a,
      a:visited {
        text-decoration: underline;
      }
      a[href]:after {
        content: " (" attr(href) ")";
      }
      abbr[title]:after {
        content: " (" attr(title) ")";
      }
      a[href^="#"]:after,
      a[href^="javascript:"]:after {
        content: "";
      }
      pre,
      blockquote {
        border: 1px solid #999;
        page-break-inside: avoid;
      }
      thead {
        display: table-header-group;
      }
      tr,
      img {
        page-break-inside: avoid;
      }
      img {
        max-width: 100% !important;
      }
      p,
      h2,
      h3 {
        orphans: 3;
        widows: 3;
      }
      h2,
      h3 {
        page-break-after: avoid;
      }
      .navbar {
        display: none;
      }
      .btn > .caret,
      .dropup > .btn > .caret {
        border-top-color: #000 !important;
      }
      .label {
        border: 1px solid #000;
      }
      .table {
        border-collapse: collapse !important;
      }
      .table td,
      .table th {
        background-color: #fff !important;
      }
      .table-bordered th,
      .table-bordered td {
        border: 1px solid #ddd !important;
      }
    }

    @media (min-width: 768px) {
      .lead {
        font-size: 21px;
      }
    }

    @media (min-width: 768px) {
      .dl-horizontal dt {
        float: left;
        width: 160px;
        overflow: hidden;
        clear: left;
        text-align: right;
        text-overflow: ellipsis;
        white-space: nowrap;
      }
      .dl-horizontal dd {
        margin-left: 180px;
      }
    }

    @media (min-width: 768px) {
      .container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {
      .container {
        width: 970px;
      }
    }

    @media (min-width: 1200px) {
      .container {
        width: 1170px;
      }
    }

    @media (min-width: 768px) {
      .col-sm-1,
      .col-sm-2,
      .col-sm-3,
      .col-sm-4,
      .col-sm-5,
      .col-sm-6,
      .col-sm-7,
      .col-sm-8,
      .col-sm-9,
      .col-sm-10,
      .col-sm-11,
      .col-sm-12 {
        float: left;
      }
      .col-sm-12 {
        width: 100%;
      }
      .col-sm-11 {
        width: 91.66666667%;
      }
      .col-sm-10 {
        width: 83.33333333%;
      }
      .col-sm-9 {
        width: 75%;
      }
      .col-sm-8 {
        width: 66.66666667%;
      }
      .col-sm-7 {
        width: 58.33333333%;
      }
      .col-sm-6 {
        width: 50%;
      }
      .col-sm-5 {
        width: 41.66666667%;
      }
      .col-sm-4 {
        width: 33.33333333%;
      }
      .col-sm-3 {
        width: 25%;
      }
      .col-sm-2 {
        width: 16.66666667%;
      }
      .col-sm-1 {
        width: 8.33333333%;
      }
      .col-sm-pull-12 {
        right: 100%;
      }
      .col-sm-pull-11 {
        right: 91.66666667%;
      }
      .col-sm-pull-10 {
        right: 83.33333333%;
      }
      .col-sm-pull-9 {
        right: 75%;
      }
      .col-sm-pull-8 {
        right: 66.66666667%;
      }
      .col-sm-pull-7 {
        right: 58.33333333%;
      }
      .col-sm-pull-6 {
        right: 50%;
      }
      .col-sm-pull-5 {
        right: 41.66666667%;
      }
      .col-sm-pull-4 {
        right: 33.33333333%;
      }
      .col-sm-pull-3 {
        right: 25%;
      }
      .col-sm-pull-2 {
        right: 16.66666667%;
      }
      .col-sm-pull-1 {
        right: 8.33333333%;
      }
      .col-sm-pull-0 {
        right: auto;
      }
      .col-sm-push-12 {
        left: 100%;
      }
      .col-sm-push-11 {
        left: 91.66666667%;
      }
      .col-sm-push-10 {
        left: 83.33333333%;
      }
      .col-sm-push-9 {
        left: 75%;
      }
      .col-sm-push-8 {
        left: 66.66666667%;
      }
      .col-sm-push-7 {
        left: 58.33333333%;
      }
      .col-sm-push-6 {
        left: 50%;
      }
      .col-sm-push-5 {
        left: 41.66666667%;
      }
      .col-sm-push-4 {
        left: 33.33333333%;
      }
      .col-sm-push-3 {
        left: 25%;
      }
      .col-sm-push-2 {
        left: 16.66666667%;
      }
      .col-sm-push-1 {
        left: 8.33333333%;
      }
      .col-sm-push-0 {
        left: auto;
      }
      .col-sm-offset-12 {
        margin-left: 100%;
      }
      .col-sm-offset-11 {
        margin-left: 91.66666667%;
      }
      .col-sm-offset-10 {
        margin-left: 83.33333333%;
      }
      .col-sm-offset-9 {
        margin-left: 75%;
      }
      .col-sm-offset-8 {
        margin-left: 66.66666667%;
      }
      .col-sm-offset-7 {
        margin-left: 58.33333333%;
      }
      .col-sm-offset-6 {
        margin-left: 50%;
      }
      .col-sm-offset-5 {
        margin-left: 41.66666667%;
      }
      .col-sm-offset-4 {
        margin-left: 33.33333333%;
      }
      .col-sm-offset-3 {
        margin-left: 25%;
      }
      .col-sm-offset-2 {
        margin-left: 16.66666667%;
      }
      .col-sm-offset-1 {
        margin-left: 8.33333333%;
      }
      .col-sm-offset-0 {
        margin-left: 0;
      }
    }

    @media (min-width: 992px) {
      .col-md-1,
      .col-md-2,
      .col-md-3,
      .col-md-4,
      .col-md-5,
      .col-md-6,
      .col-md-7,
      .col-md-8,
      .col-md-9,
      .col-md-10,
      .col-md-11,
      .col-md-12 {
        float: left;
      }
      .col-md-12 {
        width: 100%;
      }
      .col-md-11 {
        width: 91.66666667%;
      }
      .col-md-10 {
        width: 83.33333333%;
      }
      .col-md-9 {
        width: 75%;
      }
      .col-md-8 {
        width: 66.66666667%;
      }
      .col-md-7 {
        width: 58.33333333%;
      }
      .col-md-6 {
        width: 50%;
      }
      .col-md-5 {
        width: 41.66666667%;
      }
      .col-md-4 {
        width: 33.33333333%;
      }
      .col-md-3 {
        width: 25%;
      }
      .col-md-2 {
        width: 16.66666667%;
      }
      .col-md-1 {
        width: 8.33333333%;
      }
      .col-md-pull-12 {
        right: 100%;
      }
      .col-md-pull-11 {
        right: 91.66666667%;
      }
      .col-md-pull-10 {
        right: 83.33333333%;
      }
      .col-md-pull-9 {
        right: 75%;
      }
      .col-md-pull-8 {
        right: 66.66666667%;
      }
      .col-md-pull-7 {
        right: 58.33333333%;
      }
      .col-md-pull-6 {
        right: 50%;
      }
      .col-md-pull-5 {
        right: 41.66666667%;
      }
      .col-md-pull-4 {
        right: 33.33333333%;
      }
      .col-md-pull-3 {
        right: 25%;
      }
      .col-md-pull-2 {
        right: 16.66666667%;
      }
      .col-md-pull-1 {
        right: 8.33333333%;
      }
      .col-md-pull-0 {
        right: auto;
      }
      .col-md-push-12 {
        left: 100%;
      }
      .col-md-push-11 {
        left: 91.66666667%;
      }
      .col-md-push-10 {
        left: 83.33333333%;
      }
      .col-md-push-9 {
        left: 75%;
      }
      .col-md-push-8 {
        left: 66.66666667%;
      }
      .col-md-push-7 {
        left: 58.33333333%;
      }
      .col-md-push-6 {
        left: 50%;
      }
      .col-md-push-5 {
        left: 41.66666667%;
      }
      .col-md-push-4 {
        left: 33.33333333%;
      }
      .col-md-push-3 {
        left: 25%;
      }
      .col-md-push-2 {
        left: 16.66666667%;
      }
      .col-md-push-1 {
        left: 8.33333333%;
      }
      .col-md-push-0 {
        left: auto;
      }
      .col-md-offset-12 {
        margin-left: 100%;
      }
      .col-md-offset-11 {
        margin-left: 91.66666667%;
      }
      .col-md-offset-10 {
        margin-left: 83.33333333%;
      }
      .col-md-offset-9 {
        margin-left: 75%;
      }
      .col-md-offset-8 {
        margin-left: 66.66666667%;
      }
      .col-md-offset-7 {
        margin-left: 58.33333333%;
      }
      .col-md-offset-6 {
        margin-left: 50%;
      }
      .col-md-offset-5 {
        margin-left: 41.66666667%;
      }
      .col-md-offset-4 {
        margin-left: 33.33333333%;
      }
      .col-md-offset-3 {
        margin-left: 25%;
      }
      .col-md-offset-2 {
        margin-left: 16.66666667%;
      }
      .col-md-offset-1 {
        margin-left: 8.33333333%;
      }
      .col-md-offset-0 {
        margin-left: 0;
      }
    }

    @media (min-width: 1200px) {
      .col-lg-1,
      .col-lg-2,
      .col-lg-3,
      .col-lg-4,
      .col-lg-5,
      .col-lg-6,
      .col-lg-7,
      .col-lg-8,
      .col-lg-9,
      .col-lg-10,
      .col-lg-11,
      .col-lg-12 {
        float: left;
      }
      .col-lg-12 {
        width: 100%;
      }
      .col-lg-11 {
        width: 91.66666667%;
      }
      .col-lg-10 {
        width: 83.33333333%;
      }
      .col-lg-9 {
        width: 75%;
      }
      .col-lg-8 {
        width: 66.66666667%;
      }
      .col-lg-7 {
        width: 58.33333333%;
      }
      .col-lg-6 {
        width: 50%;
      }
      .col-lg-5 {
        width: 41.66666667%;
      }
      .col-lg-4 {
        width: 33.33333333%;
      }
      .col-lg-3 {
        width: 25%;
      }
      .col-lg-2 {
        width: 16.66666667%;
      }
      .col-lg-1 {
        width: 8.33333333%;
      }
      .col-lg-pull-12 {
        right: 100%;
      }
      .col-lg-pull-11 {
        right: 91.66666667%;
      }
      .col-lg-pull-10 {
        right: 83.33333333%;
      }
      .col-lg-pull-9 {
        right: 75%;
      }
      .col-lg-pull-8 {
        right: 66.66666667%;
      }
      .col-lg-pull-7 {
        right: 58.33333333%;
      }
      .col-lg-pull-6 {
        right: 50%;
      }
      .col-lg-pull-5 {
        right: 41.66666667%;
      }
      .col-lg-pull-4 {
        right: 33.33333333%;
      }
      .col-lg-pull-3 {
        right: 25%;
      }
      .col-lg-pull-2 {
        right: 16.66666667%;
      }
      .col-lg-pull-1 {
        right: 8.33333333%;
      }
      .col-lg-pull-0 {
        right: auto;
      }
      .col-lg-push-12 {
        left: 100%;
      }
      .col-lg-push-11 {
        left: 91.66666667%;
      }
      .col-lg-push-10 {
        left: 83.33333333%;
      }
      .col-lg-push-9 {
        left: 75%;
      }
      .col-lg-push-8 {
        left: 66.66666667%;
      }
      .col-lg-push-7 {
        left: 58.33333333%;
      }
      .col-lg-push-6 {
        left: 50%;
      }
      .col-lg-push-5 {
        left: 41.66666667%;
      }
      .col-lg-push-4 {
        left: 33.33333333%;
      }
      .col-lg-push-3 {
        left: 25%;
      }
      .col-lg-push-2 {
        left: 16.66666667%;
      }
      .col-lg-push-1 {
        left: 8.33333333%;
      }
      .col-lg-push-0 {
        left: auto;
      }
      .col-lg-offset-12 {
        margin-left: 100%;
      }
      .col-lg-offset-11 {
        margin-left: 91.66666667%;
      }
      .col-lg-offset-10 {
        margin-left: 83.33333333%;
      }
      .col-lg-offset-9 {
        margin-left: 75%;
      }
      .col-lg-offset-8 {
        margin-left: 66.66666667%;
      }
      .col-lg-offset-7 {
        margin-left: 58.33333333%;
      }
      .col-lg-offset-6 {
        margin-left: 50%;
      }
      .col-lg-offset-5 {
        margin-left: 41.66666667%;
      }
      .col-lg-offset-4 {
        margin-left: 33.33333333%;
      }
      .col-lg-offset-3 {
        margin-left: 25%;
      }
      .col-lg-offset-2 {
        margin-left: 16.66666667%;
      }
      .col-lg-offset-1 {
        margin-left: 8.33333333%;
      }
      .col-lg-offset-0 {
        margin-left: 0;
      }
    }

    @media screen and (max-width: 767px) {
      .table-responsive {
        width: 100%;
        margin-bottom: 15px;
        overflow-y: hidden;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        border: 1px solid #ddd;
      }
      .table-responsive > .table {
        margin-bottom: 0;
      }
      .table-responsive > .table > thead > tr > th,
      .table-responsive > .table > tbody > tr > th,
      .table-responsive > .table > tfoot > tr > th,
      .table-responsive > .table > thead > tr > td,
      .table-responsive > .table > tbody > tr > td,
      .table-responsive > .table > tfoot > tr > td {
        white-space: nowrap;
      }
      .table-responsive > .table-bordered {
        border: 0;
      }
      .table-responsive > .table-bordered > thead > tr > th:first-child,
      .table-responsive > .table-bordered > tbody > tr > th:first-child,
      .table-responsive > .table-bordered > tfoot > tr > th:first-child,
      .table-responsive > .table-bordered > thead > tr > td:first-child,
      .table-responsive > .table-bordered > tbody > tr > td:first-child,
      .table-responsive > .table-bordered > tfoot > tr > td:first-child {
        border-left: 0;
      }
      .table-responsive > .table-bordered > thead > tr > th:last-child,
      .table-responsive > .table-bordered > tbody > tr > th:last-child,
      .table-responsive > .table-bordered > tfoot > tr > th:last-child,
      .table-responsive > .table-bordered > thead > tr > td:last-child,
      .table-responsive > .table-bordered > tbody > tr > td:last-child,
      .table-responsive > .table-bordered > tfoot > tr > td:last-child {
        border-right: 0;
      }
      .table-responsive > .table-bordered > tbody > tr:last-child > th,
      .table-responsive > .table-bordered > tfoot > tr:last-child > th,
      .table-responsive > .table-bordered > tbody > tr:last-child > td,
      .table-responsive > .table-bordered > tfoot > tr:last-child > td {
        border-bottom: 0;
      }
    }

    @media screen and (-webkit-min-device-pixel-ratio: 0) {
      input[type="date"].form-control,
      input[type="time"].form-control,
      input[type="datetime-local"].form-control,
      input[type="month"].form-control {
        line-height: 34px;
      }
      input[type="date"].input-sm,
      input[type="time"].input-sm,
      input[type="datetime-local"].input-sm,
      input[type="month"].input-sm,
      .input-group-sm input[type="date"],
      .input-group-sm input[type="time"],
      .input-group-sm input[type="datetime-local"],
      .input-group-sm input[type="month"] {
        line-height: 30px;
      }
      input[type="date"].input-lg,
      input[type="time"].input-lg,
      input[type="datetime-local"].input-lg,
      input[type="month"].input-lg,
      .input-group-lg input[type="date"],
      .input-group-lg input[type="time"],
      .input-group-lg input[type="datetime-local"],
      .input-group-lg input[type="month"] {
        line-height: 46px;
      }
    }

    @media (min-width: 768px) {
      .form-inline .form-group {
        display: inline-block;
        margin-bottom: 0;
        vertical-align: middle;
      }
      .form-inline .form-control {
        display: inline-block;
        width: auto;
        vertical-align: middle;
      }
      .form-inline .form-control-static {
        display: inline-block;
      }
      .form-inline .input-group {
        display: inline-table;
        vertical-align: middle;
      }
      .form-inline .input-group .input-group-addon,
      .form-inline .input-group .input-group-btn,
      .form-inline .input-group .form-control {
        width: auto;
      }
      .form-inline .input-group > .form-control {
        width: 100%;
      }
      .form-inline .control-label {
        margin-bottom: 0;
        vertical-align: middle;
      }
      .form-inline .radio,
      .form-inline .checkbox {
        display: inline-block;
        margin-top: 0;
        margin-bottom: 0;
        vertical-align: middle;
      }
      .form-inline .radio label,
      .form-inline .checkbox label {
        padding-left: 0;
      }
      .form-inline .radio input[type="radio"],
      .form-inline .checkbox input[type="checkbox"] {
        position: relative;
        margin-left: 0;
      }
      .form-inline .has-feedback .form-control-feedback {
        top: 0;
      }
    }

    @media (min-width: 768px) {
      .form-horizontal .control-label {
        padding-top: 7px;
        margin-bottom: 0;
        text-align: right;
      }
    }

    @media (min-width: 768px) {
      .form-horizontal .form-group-lg .control-label {
        padding-top: 11px;
        font-size: 18px;
      }
    }

    @media (min-width: 768px) {
      .form-horizontal .form-group-sm .control-label {
        padding-top: 6px;
        font-size: 12px;
      }
    }

    @media (min-width: 768px) {
      .navbar-right .dropdown-menu {
        right: 0;
        left: auto;
      }
      .navbar-right .dropdown-menu-left {
        right: auto;
        left: 0;
      }
    }

    @media (min-width: 768px) {
      .nav-tabs.nav-justified > li {
        display: table-cell;
        width: 1%;
      }
      .nav-tabs.nav-justified > li > a {
        margin-bottom: 0;
      }
    }

    @media (min-width: 768px) {
      .nav-tabs.nav-justified > li > a {
        border-bottom: 1px solid #ddd;
        border-radius: 4px 4px 0 0;
      }
      .nav-tabs.nav-justified > .active > a,
      .nav-tabs.nav-justified > .active > a:hover,
      .nav-tabs.nav-justified > .active > a:focus {
        border-bottom-color: #fff;
      }
    }

    @media (min-width: 768px) {
      .nav-justified > li {
        display: table-cell;
        width: 1%;
      }
      .nav-justified > li > a {
        margin-bottom: 0;
      }
    }

    @media (min-width: 768px) {
      .nav-tabs-justified > li > a {
        border-bottom: 1px solid #ddd;
        border-radius: 4px 4px 0 0;
      }
      .nav-tabs-justified > .active > a,
      .nav-tabs-justified > .active > a:hover,
      .nav-tabs-justified > .active > a:focus {
        border-bottom-color: #fff;
      }
    }

    @media (min-width: 768px) {
      .navbar {
        border-radius: 4px;
      }
    }

    @media (min-width: 768px) {
      .navbar-header {
        float: left;
      }
    }

    @media (min-width: 768px) {
      .navbar-collapse {
        width: auto;
        border-top: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
      }
      .navbar-collapse.collapse {
        display: block !important;
        height: auto !important;
        padding-bottom: 0;
        overflow: visible !important;
      }
      .navbar-collapse.in {
        overflow-y: visible;
      }
      .navbar-fixed-top .navbar-collapse,
      .navbar-static-top .navbar-collapse,
      .navbar-fixed-bottom .navbar-collapse {
        padding-right: 0;
        padding-left: 0;
      }
    }

    @media (max-device-width: 480px) and (orientation: landscape) {
      .navbar-fixed-top .navbar-collapse,
      .navbar-fixed-bottom .navbar-collapse {
        max-height: 200px;
      }
    }

    @media (min-width: 768px) {
      .container > .navbar-header,
      .container-fluid > .navbar-header,
      .container > .navbar-collapse,
      .container-fluid > .navbar-collapse {
        margin-right: 0;
        margin-left: 0;
      }
    }

    @media (min-width: 768px) {
      .navbar-static-top {
        border-radius: 0;
      }
    }

    @media (min-width: 768px) {
      .navbar-fixed-top,
      .navbar-fixed-bottom {
        border-radius: 0;
      }
    }

    @media (min-width: 768px) {
      .navbar > .container .navbar-brand,
      .navbar > .container-fluid .navbar-brand {
        margin-left: -15px;
      }
    }

    @media (min-width: 768px) {
      .navbar-toggle {
        display: none;
      }
    }

    @media (max-width: 767px) {
      .navbar-nav .open .dropdown-menu {
        position: static;
        float: none;
        width: auto;
        margin-top: 0;
        background-color: transparent;
        border: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
      }
      .navbar-nav .open .dropdown-menu > li > a,
      .navbar-nav .open .dropdown-menu .dropdown-header {
        padding: 5px 15px 5px 25px;
      }
      .navbar-nav .open .dropdown-menu > li > a {
        line-height: 20px;
      }
      .navbar-nav .open .dropdown-menu > li > a:hover,
      .navbar-nav .open .dropdown-menu > li > a:focus {
        background-image: none;
      }
    }

    @media (min-width: 768px) {
      .navbar-nav {
        float: left;
        margin: 0;
      }
      .navbar-nav > li {
        float: left;
      }
      .navbar-nav > li > a {
        padding-top: 15px;
        padding-bottom: 15px;
      }
    }

    @media (min-width: 768px) {
      .navbar-form .form-group {
        display: inline-block;
        margin-bottom: 0;
        vertical-align: middle;
      }
      .navbar-form .form-control {
        display: inline-block;
        width: auto;
        vertical-align: middle;
      }
      .navbar-form .form-control-static {
        display: inline-block;
      }
      .navbar-form .input-group {
        display: inline-table;
        vertical-align: middle;
      }
      .navbar-form .input-group .input-group-addon,
      .navbar-form .input-group .input-group-btn,
      .navbar-form .input-group .form-control {
        width: auto;
      }
      .navbar-form .input-group > .form-control {
        width: 100%;
      }
      .navbar-form .control-label {
        margin-bottom: 0;
        vertical-align: middle;
      }
      .navbar-form .radio,
      .navbar-form .checkbox {
        display: inline-block;
        margin-top: 0;
        margin-bottom: 0;
        vertical-align: middle;
      }
      .navbar-form .radio label,
      .navbar-form .checkbox label {
        padding-left: 0;
      }
      .navbar-form .radio input[type="radio"],
      .navbar-form .checkbox input[type="checkbox"] {
        position: relative;
        margin-left: 0;
      }
      .navbar-form .has-feedback .form-control-feedback {
        top: 0;
      }
    }

    @media (max-width: 767px) {
      .navbar-form .form-group {
        margin-bottom: 5px;
      }
      .navbar-form .form-group:last-child {
        margin-bottom: 0;
      }
    }

    @media (min-width: 768px) {
      .navbar-form {
        width: auto;
        padding-top: 0;
        padding-bottom: 0;
        margin-right: 0;
        margin-left: 0;
        border: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
      }
    }

    @media (min-width: 768px) {
      .navbar-text {
        float: left;
        margin-right: 15px;
        margin-left: 15px;
      }
    }

    @media (min-width: 768px) {
      .navbar-left {
        float: left !important;
      }
      .navbar-right {
        float: right !important;
        margin-right: -15px;
      }
      .navbar-right ~ .navbar-right {
        margin-right: 0;
      }
    }

    @media (max-width: 767px) {
      .navbar-default .navbar-nav .open .dropdown-menu > li > a {
        color: #777;
      }
      .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
      .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
        color: #333;
        background-color: transparent;
      }
      .navbar-default .navbar-nav .open .dropdown-menu > .active > a,
      .navbar-default .navbar-nav .open .dropdown-menu > .active > a:hover,
      .navbar-default .navbar-nav .open .dropdown-menu > .active > a:focus {
        color: #555;
        background-color: #e7e7e7;
      }
      .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a,
      .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a:hover,
      .navbar-default .navbar-nav .open .dropdown-menu > .disabled > a:focus {
        color: #ccc;
        background-color: transparent;
      }
    }

    @media (max-width: 767px) {
      .navbar-inverse .navbar-nav .open .dropdown-menu > .dropdown-header {
        border-color: #080808;
      }
      .navbar-inverse .navbar-nav .open .dropdown-menu .divider {
        background-color: #080808;
      }
      .navbar-inverse .navbar-nav .open .dropdown-menu > li > a {
        color: #9d9d9d;
      }
      .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:hover,
      .navbar-inverse .navbar-nav .open .dropdown-menu > li > a:focus {
        color: #fff;
        background-color: transparent;
      }
      .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a,
      .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:hover,
      .navbar-inverse .navbar-nav .open .dropdown-menu > .active > a:focus {
        color: #fff;
        background-color: #080808;
      }
      .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a,
      .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a:hover,
      .navbar-inverse .navbar-nav .open .dropdown-menu > .disabled > a:focus {
        color: #444;
        background-color: transparent;
      }
    }

    @media screen and (min-width: 768px) {
      .jumbotron {
        padding-top: 48px;
        padding-bottom: 48px;
      }
      .container .jumbotron,
      .container-fluid .jumbotron {
        padding-right: 60px;
        padding-left: 60px;
      }
      .jumbotron h1,
      .jumbotron .h1 {
        font-size: 63px;
      }
    }

    @media (min-width: 768px) {
      .modal-dialog {
        width: 600px;
        margin: 30px auto;
      }
      .modal-content {
        -webkit-box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
        box-shadow: 0 5px 15px rgba(0, 0, 0, .5);
      }
      .modal-sm {
        width: 300px;
      }
    }

    @media (min-width: 992px) {
      .modal-lg {
        width: 900px;
      }
    }

    @media all and (transform-3d),
    (-webkit-transform-3d) {
      .carousel-inner > .item {
        -webkit-transition: -webkit-transform .6s ease-in-out;
        -o-transition: -o-transform .6s ease-in-out;
        transition: transform .6s ease-in-out;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-perspective: 1000px;
        perspective: 1000px;
      }
      .carousel-inner > .item.next,
      .carousel-inner > .item.active.right {
        left: 0;
        -webkit-transform: translate3d(100%, 0, 0);
        transform: translate3d(100%, 0, 0);
      }
      .carousel-inner > .item.prev,
      .carousel-inner > .item.active.left {
        left: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
      }
      .carousel-inner > .item.next.left,
      .carousel-inner > .item.prev.right,
      .carousel-inner > .item.active {
        left: 0;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
      }
    }

    @media screen and (min-width: 768px) {
      .carousel-control .glyphicon-chevron-left,
      .carousel-control .glyphicon-chevron-right,
      .carousel-control .icon-prev,
      .carousel-control .icon-next {
        width: 30px;
        height: 30px;
        margin-top: -10px;
        font-size: 30px;
      }
      .carousel-control .glyphicon-chevron-left,
      .carousel-control .icon-prev {
        margin-left: -10px;
      }
      .carousel-control .glyphicon-chevron-right,
      .carousel-control .icon-next {
        margin-right: -10px;
      }
      .carousel-caption {
        right: 20%;
        left: 20%;
        padding-bottom: 30px;
      }
      .carousel-indicators {
        bottom: 20px;
      }
    }

    @media (max-width: 767px) {
      .visible-xs {
        display: block !important;
      }
      table.visible-xs {
        display: table !important;
      }
      tr.visible-xs {
        display: table-row !important;
      }
      th.visible-xs,
      td.visible-xs {
        display: table-cell !important;
      }
    }

    @media (max-width: 767px) {
      .visible-xs-block {
        display: block !important;
      }
    }

    @media (max-width: 767px) {
      .visible-xs-inline {
        display: inline !important;
      }
    }

    @media (max-width: 767px) {
      .visible-xs-inline-block {
        display: inline-block !important;
      }
    }

    @media (min-width: 768px) and (max-width: 991px) {
      .visible-sm {
        display: block !important;
      }
      table.visible-sm {
        display: table !important;
      }
      tr.visible-sm {
        display: table-row !important;
      }
      th.visible-sm,
      td.visible-sm {
        display: table-cell !important;
      }
    }

    @media (min-width: 768px) and (max-width: 991px) {
      .visible-sm-block {
        display: block !important;
      }
    }

    @media (min-width: 768px) and (max-width: 991px) {
      .visible-sm-inline {
        display: inline !important;
      }
    }

    @media (min-width: 768px) and (max-width: 991px) {
      .visible-sm-inline-block {
        display: inline-block !important;
      }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
      .visible-md {
        display: block !important;
      }
      table.visible-md {
        display: table !important;
      }
      tr.visible-md {
        display: table-row !important;
      }
      th.visible-md,
      td.visible-md {
        display: table-cell !important;
      }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
      .visible-md-block {
        display: block !important;
      }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
      .visible-md-inline {
        display: inline !important;
      }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
      .visible-md-inline-block {
        display: inline-block !important;
      }
    }

    @media (min-width: 1200px) {
      .visible-lg {
        display: block !important;
      }
      table.visible-lg {
        display: table !important;
      }
      tr.visible-lg {
        display: table-row !important;
      }
      th.visible-lg,
      td.visible-lg {
        display: table-cell !important;
      }
    }

    @media (min-width: 1200px) {
      .visible-lg-block {
        display: block !important;
      }
    }

    @media (min-width: 1200px) {
      .visible-lg-inline {
        display: inline !important;
      }
    }

    @media (min-width: 1200px) {
      .visible-lg-inline-block {
        display: inline-block !important;
      }
    }

    @media (max-width: 767px) {
      .hidden-xs {
        display: none !important;
      }
    }

    @media (min-width: 768px) and (max-width: 991px) {
      .hidden-sm {
        display: none !important;
      }
    }

    @media (min-width: 992px) and (max-width: 1199px) {
      .hidden-md {
        display: none !important;
      }
    }

    @media (min-width: 1200px) {
      .hidden-lg {
        display: none !important;
      }
    }

    @media print {
      .visible-print {
        display: block !important;
      }
      table.visible-print {
        display: table !important;
      }
      tr.visible-print {
        display: table-row !important;
      }
      th.visible-print,
      td.visible-print {
        display: table-cell !important;
      }
    }

    @media print {
      .visible-print-block {
        display: block !important;
      }
    }

    @media print {
      .visible-print-inline {
        display: inline !important;
      }
    }

    @media print {
      .visible-print-inline-block {
        display: inline-block !important;
      }
    }

    @media print {
      .hidden-print {
        display: none !important;
      }
    }
  </style>
</head>
<body style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;height:100% !important;margin:0 !important;padding:0 !important;width:100% !important;margin: 0 !important; padding: 0 !important;">
<!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
  {{ env('COMPANY') }}
</div>
<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
  <tr>
    <td bgcolor="#ffffff" align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 70px 15px 70px 15px;" class="section-padding">
      <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td align="center" valign="top" width="500">
      <![endif]-->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width: 500px;" class="responsive-table">
        <tr>
          <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
            <!-- HERO IMAGE -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
              <tr>
                <td class="padding" align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                  <a href="{{env('SITE_URL')}}" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">
                    <img src="{{env('SITE_URL')}}/public/assets/images/{{env('COMPANY_LOGO')}}" border="0" alt="Insert alt text here" style="-ms-interpolation-mode:bicubic;border:0;line-height:100%;outline:none;text-decoration:none; width:30%;height:auto; border-radius:50%; display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;box-shadow:5px 5px 40px 1px black;" class="img-max"></a>
                </td>
              </tr>
              <tr>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                  <!-- COPY -->
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                    <tr>
                      <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding"></td>
                    </tr>
                    <tr>
                      <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding">
                        @yield('content')
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
    </td>
  </tr>
  <tr>
    <td bgcolor="#F5F7FA" align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 70px 15px 70px 15px;" class="section-padding">
      <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td align="center" valign="top" width="500">
      <![endif]-->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width: 500px;" class="responsive-table">
        <tr>
          <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
            <!-- TITLE SECTION AND COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
              <tr>
                <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;" class="padding">
                  <a style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;text-decoration: none; color: #666666;" href="{{env('COMPANY_URL')}}">
                    {{env('COMPANY')}}
                  </a>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center" height="100%" valign="top" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
              <tr>
                <td align="center" valign="top" width="500">
            <![endif]-->
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width:500px;">
              <tr>
                <td align="center" valign="top" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size:0;" class="padding">
                  <!--[if (gte mso 9)|(IE)]>
                  <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
                    <tr>
                      <td align="left" valign="top" width="240">
                  <![endif]-->
                  <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;" class="wrapper">
                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width:240px;" class="wrapper">
                      <tr>
                        <td align="center" valign="top" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                            <tr>
                              <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 20px 0 30px 0;">
                                <table cellpadding="0" cellspacing="0" border="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                                  <tr>
                                    <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;background-position: top; background-size: contain;background-repeat: no-repeat; background-image: url('{{env('SITE_URL')}}/public/assets/images/{{env('BACKGROUND_LOGO')}}');" valign="middle"><a href="{{env('SITE_URL')}}" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;"><img src="{{env('SITE_URL')}}/public/assets/images/email-transparent.png" width="240" height="130" style="-ms-interpolation-mode:bicubic;border:0;height:auto;line-height:100%;outline:none;text-decoration:none;display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 130px;" alt="Fluid images" border="0" class="img-max"></a></td>
                                  </tr>
                                  <tr>
                                    <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><a style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;text-decoration: none; color: #666666;" href="{{env('SITE_URL')}}">{{env('COMPANY')}}</a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!--[if (gte mso 9)|(IE)]>
                  </td>
                  <td width="20" style="font-size: 1px;">&nbsp;</td>
                  <td align="right" valign="top" width="240">
                  <![endif]-->
                  <div style="display:inline-block; max-width:50%; min-width:240px; vertical-align:top; width:100%;" class="wrapper">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                      <tr>
                        <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width:240px; float: right;" class="wrapper">
                            <tr>
                              <td align="center" valign="top" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                                  <tr>
                                    <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 20px 0 30px 0;">
                                      <table cellpadding="0" cellspacing="0" border="0" width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;">
                                        <tr>
                                          <td align="center" bgcolor="#F5F7FA" valign="middle" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;background-position: top; background-size: contain; background-repeat: no-repeat; background-image: url('{{env('SITE_URL')}}/public/assets/images/facebook.png');"><a href="{{env('FACEBOOK')}}" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;"><img src="{{env('SITE_URL')}}/public/assets/images/email-transparent.png" width="240" height="130" style="-ms-interpolation-mode:bicubic;border:0;height:auto;line-height:100%;outline:none;text-decoration:none;display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 130px;" alt="alt text here" border="0" class="img-max"/></a></td>
                                        </tr>
                                        <tr>
                                          <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><a style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;text-decoration: none; color: #666666;" href="{{env('FACEBOOK')}}">Follow us on Facebook</a></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!--[if (gte mso 9)|(IE)]>
                  </td>
                  </tr>
                  </table>
                  <![endif]-->
                </td>
              </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
          </td>
        </tr>
      </table>
      <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
    </td>
  </tr>
  <tr>
    <td bgcolor="#ffffff" align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;padding: 20px 0px;">
      <!--[if (gte mso 9)|(IE)]>
      <table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
        <tr>
          <td align="center" valign="top" width="500">
      <![endif]-->
      <!-- UNSUBSCRIBE COPY -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse !important;max-width: 500px;" class="responsive-table">
        <tr>
          <td align="center" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
            {{env('COMPANY_ADDRESS')}}
            <br>
          </td>
        </tr>
      </table>
      <!--[if (gte mso 9)|(IE)]>
      </td>
      </tr>
      </table>
      <![endif]-->
    </td>
  </tr>
</table>
</body>
</html>