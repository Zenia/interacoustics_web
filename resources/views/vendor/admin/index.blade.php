@extends('voyager::master')

@section('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
@stop


@section('page_header')
  <h1 class="page-title">
    <i class="voyager-helm"></i> Admin Settings
  </h1>
  @include('vendor.voyager.multilingual.language-selector')
@stop

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        
        <div class="panel panel-bordered">
          
          <div class="panel-heading">
            <h3 class="panel-title">Admin Settings</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form"
                class="form-edit-add"
                action="{{ route('voyager.adminsettings.store')}}"
                method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            
            <div class="panel-body">
              
              @if (count($errors) > 0)
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
              <table class="table table-striped">
                <tbody>
                <tr>
                  <td>
                    Sender Email
                  </td>
                  <td>
                    <input type="email" class="form-control" placeholder="Sender email" value="{{$sender_email}}" name="sender_email" id="sender_email">
                  </td>
                </tr>
                <tr>
                  <td>
                    Request for Brandchange Recipients
                  </td>
                  <td>
                    <select type="text" class="form-control select2" name="request_for_brand_change_users[]"
                            id="request_for_brand_change_users" multiple>
                    @foreach($adminUsers as $user)
                        <option value="{{$user->id}}"
                                @if(in_array($user->id,$request_for_brand_change_users)) selected @endif >{{$user->name}} {{$user->email}}</option>
                      @endforeach
                    </select>
                  </td>
                </tr>
                <tr>
                  <td>
                    Contact form Recipients
                  </td>
                  <td>
                    <select type="text" class="form-control select2" name="contact_form_users[]"
                            id="contact_form_users" multiple>
                      @foreach($adminUsers as $user)
                        <option value="{{$user->id}}"
                                @if(in_array($user->id,$contact_form_users)) selected @endif >{{$user->name}} {{$user->email}}</option>
                    @endforeach
                  </select>
                  </td>
                </tr>
                </tbody>
              </table>
            </div><!-- panel-body -->
            
            <div class="panel-footer">
              <button type="submit" class="btn btn-primary save">Save</button>
            </div>
          </form>
        
        </div>
      </div>
    </div>
  </div>
  <!-- End Delete File Modal -->
@stop

@section('javascript')
  <script>
    var params = {}
    var $image

    $('document').ready(function () {
      $('.toggleswitch').bootstrapToggle();

      //Init datepicker for date fields if data-datepicker attribute defined
      //or if browser does not handle date inputs
      $('.form-group input[type=date]').each(function (idx, elt) {
        if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
          elt.type = 'text';
          $(elt).datetimepicker($(elt).data('datepicker'));
        }
      });
      
      $('.side-body input[data-slug-origin]').each(function(i, el) {
        $(el).slugify();
      });

    $('[data-toggle="tooltip"]').tooltip();
    });
  </script>
  <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
  <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
  <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
  <script src="{{ voyager_asset('js/slugify.js') }}"></script>
@stop
