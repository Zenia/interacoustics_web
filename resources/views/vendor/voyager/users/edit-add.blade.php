@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )

@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/edituser.css') }}" rel="stylesheet">
  @endif

@endpush

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
  </h1>
@stop

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-12">
        
        <div class="panel panel-bordered">
          
          <div class="panel-heading">
            <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          
          <form class="form-edit-add" role="form"
                action="@if(isset($dataTypeContent->id)){{ route('user.users.update', $dataTypeContent->id) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
          @if(isset($dataTypeContent->id))
            {{ method_field("PUT") }}
          @endif
          
          <!-- CSRF TOKEN -->
            {{ csrf_field() }}
            
            <div class="panel-body">
              
              @if(isset($dataTypeContent->id))
                <input type="hidden" name="tenant_id" id="tenant_id" value="{{ $dataTypeContent->tenant_id }}">
              @elseif(!isSuperAdmin())
                <input type="hidden" name="tenant_id" id="tenant_id" value="{{ Auth::user()->tenant_id }}">
              @elseif(isSuperAdmin())
                
                <div class="form-group">
                  <label for="tenant">Tenant</label>
                  <select class="form-control" id="tenant" name="tenant_id">
                    @foreach(DB::table('tenants')->get() as $tenant)
                      <option value="{{$tenant->id}}"
                              @if(isset($dataTypeContent) && $dataTypeContent->tenant_id == $tenant->id) selected @endif
                      >{{$tenant->name}}</option>
                    @endforeach
                  </select>
                </div>
              @endif
              <div class="form-group">
                <label for="name">Name<span class="required_label">*</span></label>
                <input type="text" class="form-control" name="name"
                       placeholder="Name" id="name"
                       value="@if(isset($dataTypeContent->name)){{ old('name', $dataTypeContent->name) }}@else{{old('name')}}@endif" required>
              </div>
              
              <div class="form-group">
                <label for="name">Email<span class="required_label">*</span></label>
                <input type="text" class="form-control" name="email"
                       placeholder="Email" id="email"
                       value="@if(isset($dataTypeContent->email)){{ old('email', $dataTypeContent->email) }}@else{{old('email')}}@endif" required>
              </div>
              
              <div class="form-group">
                <label for="password">Password<span class="required_label">*</span></label>
                @if(isset($dataTypeContent->password))
                  <br>
                  <small>Leave empty to keep the same</small>
                @endif
                <input type="password" class="form-control" name="password"
                       placeholder="Password" id="password"
                       value=""                 @if(!isset($dataTypeContent->password))
                       required
                @endif>
              </div>
                @if(isset($dataTypeContent->id) && !isSuperAdmin() && !Auth::user()->hasRole('administrator'))
                  <input type="hidden" name="brand_id" id="brand_id" value="{{ $dataTypeContent->brand_id }}">
                @elseif(isSuperAdmin() || Auth::user()->hasRole('administrator'))
              <div class="form-group">
                
                <label for="brand">Brand <span class="required_label">*</span></label>
                
                <select class="form-control" id="brand" name="brand_id" required>
                  @foreach(App\Brand::all() as $Brand)
                    <option value="{{$Brand->id}}"
                            @if(isset($dataTypeContent) && $dataTypeContent->brand_id == $Brand->id) selected @endif
                    >{{$Brand->name}}</option>
                  @endforeach
                </select>
              </div>
                @endif
                @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                  <div class="form-group">
    
                  <label for="accepted">Accepted</label>
                  <input type="checkbox" name="accepted" class="toggleswitch"
                         data-on="Accepted" {!! $dataTypeContent->accepted ? 'checked="checked"' : '' !!}
                         data-off="Pending" >
                  </div>
                @else
                  @if($dataTypeContent->accepted)
                    <input type="hidden" name="accepted" id="accepted" value="{{$dataTypeContent->accepted}}">
                  @endif
                @endif
              <div class="form-group">
                <label for="avatar">Avatar</label>
                @if(isset($dataTypeContent->avatar))
                  <img src="{{ Voyager::image( $dataTypeContent->avatar ) }}"
                       style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                @endif
                <div class="input-group">
                  <input type="text" class="form-control" readonly>
                  <label class="input-group-btn">
                    <span class="btn btn-primary form-control notMe">
                      <i class="voyager-camera"></i> Browse <input type="file" name="avatar" id="avatar"
                                                                   style="display: none;">
                    </span>
                  </label>
                </div>
              </div>
              @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                <div class="form-group">
                  <label for="role">User Role</label>
                  <select name="role_id" id="role" class="form-control">
                    @if(isSuperAdmin())
                      <?php $roles = App\Role::all(); ?>
                    @else
                      <?php $roles = App\Role::whereNotIn('id', App\Role::whereName('superadmin')->pluck('id'))->get(); ?>
                    @endif
                    @foreach($roles as $role)
                      <option value="{{$role->id}}"
                              @if(isset($dataTypeContent) && $dataTypeContent->role_id == $role->id) selected @endif>{{$role->display_name}}</option>
                    @endforeach
                  </select>
                </div>
              
              @else
                @if(isset($dataTypeContent))
                  <input type="hidden" name="role_id" id="role_id" value="{{$dataTypeContent->role_id}}">
                @endif
              @endif
                
  
                <div class="form-group">
                  <label for="name">Phone</label>
                  <input type="text" class="form-control" name="phone"
                         placeholder="Phone" id="phone"
                         value="@if(isset($dataTypeContent->phone)){{ old('phone', $dataTypeContent->phone) }}@else{{old('phone')}}@endif">
                </div>
  
                <div class="form-group">
                  <label for="company">Company</label>
                  <input type="text" class="form-control" name="company"
                         placeholder="Company" id="company"
                         value="@if(isset($dataTypeContent->company)){{ old('company', $dataTypeContent->company) }}@else{{old('company')}}@endif">
                </div>
  
                <div class="form-group">
                  <label for="address">Address</label>
                  <textarea class="form-control" name="address" id="address" >@if(isset($dataTypeContent->address)){{ old('address', $dataTypeContent->address) }}@else{{old('name')}}@endif </textarea>
                </div>
  
                <div class="form-group">
                  <label for="country">Country</label>
                  <input type="text" class="form-control" name="country"
                         placeholder="Country" id="country"
                         value="@if(isset($dataTypeContent->country)){{ old('country', $dataTypeContent->country) }}@else{{old('country')}}@endif">
                </div>
              
              
              <button type="submit" class="btn btn-primary btn-block"
                      @if((Auth::check()) && ($brand = Auth::user()->brand) && (isset($brand->color)))
                      style="background-color: {{$brand->color}};
                          border-color: {{$brand->color}};
                  
                          "
                      @else
                      style="background: #3c5064;
                          border-color: #3c5064;
                          "
                  @endif
              >Submit
              </button>
            </div>
          </form>
          
          <!-- panel-body -->
          {{--<div class="panel-footer">--}}
          
          
          {{--<iframe id="form_target" name="form_target" style="display:none"></iframe>--}}
          {{--<form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"--}}
          {{--enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">--}}
          {{--<input name="image" id="upload_file" type="file"--}}
          {{--onchange="$('#my_form').submit();this.value='';">--}}
          {{--<input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">--}}
          {{--{{ csrf_field() }}--}}
          {{--</form>--}}
        
        </div>
        <div class="btn btn-block">
          <a style="color:white;" href="{{ route('requestbrandchange',Auth::id()) }}">Request Brand change</a>
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript')
  <script>
    $('document').ready(function () {
      $('.toggleswitch').bootstrapToggle();
    });
    $(document).on('change', ':file', function () {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready(function () {
      $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
          input.val(log);
        } else {
          if (log) alert(log);
        }

      });
    });
  </script>
  <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
  <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
@stop
