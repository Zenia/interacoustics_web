@extends('voyager::master')

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
    @if (Voyager::can('add_'.$dataType->name))
      <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success">
        <i class="voyager-plus"></i> Add New
      </a>
    @endif
  </h1>
@stop

<style>
  
  label.input-group-btn .btn{
    margin-top:0;
  }
  .accept,.sent, .resetpassword {
    margin-right: 5px;
  }
  .accept-request, .reject-request, .resetpassword, .accept{
    cursor: pointer;
  }
  .request-pending{
    background-color: rgba(255, 0, 0, 0.1);
  }
</style>

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-bordered">
          <div class="panel-body">
            <table id="dataTable" class="table table-hover">
              <thead>
              <tr>
                @if(isSuperAdmin())
                  <th>Tenant</th>
                @endif
                <th>Name</th>
                <th>Email</th>
                <th>Brand</th>
                <th>Avatar</th>
                <th>Role</th>
                <th class="actions">Actions</th>
              </tr>
              </thead>
              <tbody>
              @foreach($dataTypeContent as $data)
                @if(!isSuperAdmin() && $data->hasRole('superadmin'))
                @else
                  <tr @if(!$data->accepted) class="request-pending row-{{$data->id}}" @endif>
                    @if(isSuperAdmin())
                      @if($data->hasRole('superadmin'))
                        <td>All</td>
                      @else
                        {{--<td>{{dd($data)}}</td>--}}
                        <td>{{$data->tenantId['name']}}</td>
                      @endif
                    @endif
                    
                    <td>{{ucwords($data->name)}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{$data->brandId['name']}}</td>
                    <td>
                      <img
                          src="@if( strpos($data->avatar, 'http://') === false && strpos($data->avatar, 'https://') === false){{ Voyager::image( $data->avatar ) }}@else{{ $data->avatar }}@endif"
                          style="width:100px">
                    </td>
                    
                    <td>{{ $data->role ? $data->role->display_name : '' }}</td>
                    <td class="no-sort no-click">
                      @if (Voyager::can('delete_'.$dataType->name))
                        <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}"
                             id="delete-{{ $data->id }}">
                          <i class="voyager-trash"></i> Delete
                        </div>
                      @endif
                      @if (Voyager::can('edit_'.$dataType->name))
                        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}"
                           class="btn-sm btn-primary pull-right edit">
                          <i class="voyager-edit"></i> Edit
                        </a>
                      @endif
                      @if (Voyager::can('read_'.$dataType->name))
                        <a href="{{ route('voyager.'.$dataType->slug.'.show', $data->id) }}"
                           class="btn-sm btn-warning pull-right">
                          <i class="voyager-eye"></i> View
                        </a>
                      @endif
                      @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                        <div class="btn-sm btn-warning pull-right resetpassword" data-id="{{ $data->id }}">
                          <i class="voyager-lock"></i> <span>Reset Password</span>
                        </div>
                        @if(!$data->accepted && isset($data->brandId))
                          <div class="btn-sm btn-success pull-right accept-request buttons-{{$data->id}}"
                               data-id="{{ $data->id }}">
                            <i class="voyager-lock"></i> <span>Accept</span>
                          </div>
                          <div class="btn-sm btn-danger pull-right reject-request buttons-{{$data->id}}"
                               data-id="{{ $data->id }}">
                            <i class="voyager-lock"></i> <span>Reject</span>
                          </div>
                        @endif
                      @endif
                    
                    </td>
                  </tr>
                @endif
              @endforeach
              </tbody>
            </table>
            @if (isset($dataType->server_side) && $dataType->server_side)
              <div class="pull-left">
                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }}
                  to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries
                </div>
              </div>
              <div class="pull-right">
                {{ $dataTypeContent->links() }}
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="voyager-trash"></i> Are you sure you want to delete
            this {{ $dataType->display_name_singular }}?</h4>
        </div>
        <div class="modal-footer">
          <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
            {{ method_field("DELETE") }}
            {{ csrf_field() }}
            <input type="submit" class="btn btn-danger pull-right delete-confirm"
                   value="Yes, Delete This {{ $dataType->display_name_singular }}">
          </form>
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@stop

@section('javascript')
  <!-- DataTables -->
  <script>
    @if (!$dataType->server_side)
    $(document).ready(function () {
      $('#dataTable').DataTable({"order": []});
    });
    @endif

    $('td').on('click', '.delete', function (e) {
      var form = $('#delete_form')[0];

      form.action = parseActionUrl(form.action, $(this).data('id'));

      $('#delete_modal').modal('show');
    });
    $('td').on('click', '.resetpassword', function () {
      var button = $(this);
      var id = button.data('id');
      $.ajax({
        type: "GET",
        url: document.location.origin + '/users/' + id + '/resetpassword',
        datatype: "json",
//            data: {},
        success: function (data) {
          if (data['success']) {
            var icon = button.find('i');
            button.toggleClass('btn-warning btn-success resetpassword sent').find('span').text('Sent');
            icon.toggleClass('voyager-lock voyager-mail');
          }
        }

      });
    });
    $('td').on('click', '.accept-request', function () {
      var button = $(this);
      var id = button.data('id');
      $.ajax({
        type: "GET",
        url: document.location.origin + '/users/' + id + '/acceptrequest',
        datatype: "json",
//            data: {},
        success: function (data) {
          if (data['success']) {
            $('tr.request-pending.row-' + id).toggleClass('request-pending row-' + id);
            $('.buttons-' + id).remove();
          }
        }

      });
    });
    $('td').on('click', '.reject-request', function () {
      var button = $(this);
      var id = button.data('id');
      $.ajax({
        type: "GET",
        url: document.location.origin + '/users/' + id + '/rejectrequest',
        datatype: "json",
//            data: {},
        success: function (data) {
          if (data['success']) {
            $('tr.request-pending.row-' + id).toggleClass('request-pending row-' + id);
            $('.buttons-' + id).remove();
          }
        }

      });
    });

    function parseActionUrl(action, id) {
      return action.match(/\/[0-9]+$/)
          ? action.replace(/([0-9]+$)/, id)
          : action + '/' + id;
    }
  </script>
@stop
