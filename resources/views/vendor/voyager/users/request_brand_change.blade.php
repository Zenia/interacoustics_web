@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )

@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/edituser.css') }}" rel="stylesheet">
  @endif

@endpush

@section('page_header')
  <h1 class="page-title"> Request Brand change</h1>
@stop

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-12">
        
        <div class="panel panel-bordered">
          
          <div class="panel-heading">
            <h3 class="panel-title">Request Brand change</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          
          <form class="form-edit-add" role="form"
                action="{{ route('requestbrandchangepost',Auth::id()) }}"
                method="POST">
          
          <!-- CSRF TOKEN -->
            {{ csrf_field() }}
            
            <div class="panel-body">
              
              <div class="form-group">
                <label for="name">Name<span class="required_label">*</span></label>
                <input type="text" class="form-control" name="name"
                       placeholder="Name" id="name"
                       value="{{Auth::user()->name}}" required readonly>
              </div>
              
              <div class="form-group">
                <label for="old_brand">Current Brand</label>
                <input type="text" class="form-control" name="old_brand"
                       placeholder="{{Auth::user()->brand->name}}" id="old_brand"
                       value="{{Auth::user()->brand->name}}" readonly>
              </div>
              <div class="form-group">
                
                <label for="brand">New Brand <span class="required_label">*</span></label>
                <select class="form-control" id="brand" name="brand_id" required>
                  @foreach($brands as $Brand)
                    <option value="{{$Brand->id}}"
                            @if(isset($dataTypeContent) && $dataTypeContent->brand_id == $Brand->id) selected @endif
                    >{{$Brand->name}}</option>
                  @endforeach
                </select>
              </div>
              
              
              <button type="submit" class="btn btn-primary btn-block"
                      @if((Auth::check()) && ($brand = Auth::user()->brand) && (isset($brand->color)))
                      style="background-color: {{$brand->color}};
                          border-color: {{$brand->color}};
                  
                          "
                      @else
                      style="background: #3c5064;
                          border-color: #3c5064;
                          "
                  @endif
              >Submit
              </button>
            </div>
          </form>
          
          <!-- panel-body -->
          {{--<div class="panel-footer">--}}
          
          
          {{--<iframe id="form_target" name="form_target" style="display:none"></iframe>--}}
          {{--<form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"--}}
          {{--enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">--}}
          {{--<input name="image" id="upload_file" type="file"--}}
          {{--onchange="$('#my_form').submit();this.value='';">--}}
          {{--<input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">--}}
          {{--{{ csrf_field() }}--}}
          {{--</form>--}}
        
        </div>
      </div>
    </div>
  </div>
@stop
