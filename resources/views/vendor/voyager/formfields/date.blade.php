@php
if(isset($dataTypeContent->{$row->field})){
if(strpos($_SERVER['HTTP_USER_AGENT'],'Chrome') == false)
{$value =  gmdate('d-m-Y', strtotime(old($row->field, $dataTypeContent->{$row->field})));}
 else{
 $value = gmdate('Y-m-d', strtotime(old($row->field, $dataTypeContent->{$row->field})));
 }
 }else{
 $value = old($row->field);
 }
@endphp
<input @if(isset($options->required)) {{$options->required}} @endif type="date" class="form-control" name="{{ $row->field }}"
       placeholder="{{ $row->display_name }}"
       value="{{$value}}"
       @if(isset($options->datepicker))data-datepicker="{!! htmlentities(json_encode($options->datepicker), ENT_QUOTES, 'UTF-8') !!}"@endif>
