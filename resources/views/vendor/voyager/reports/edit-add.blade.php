@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )
@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/create.css') }}" rel="stylesheet">
  @endif
@endpush
@if(isset($dataTypeContent->id))
  @section('page_title','Edit '.$dataType->display_name_singular)
@else
  @section('page_title','Add '.$dataType->display_name_singular)
@endif

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
  </h1>
  @include('vendor.voyager.multilingual.language-selector')
@stop

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      @if( isSuperAdmin() || Auth::user()->hasRole('administrator'))
        <div class="col-md-6 col-md-offset-3 web-container">
          
          @else
            <div class="col-md-4 col-md-offset-4  web-container">
              @endif
              
              <div class="panel panel-bordered">
                
                <div class="panel-heading">
                  <h3 class="panel-title">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Add New' }}@endif {{ $dataType->display_name_singular }}</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-edit-add" role="form" action="@if($dataTypeContent->id){{ route('user.reports.update', $dataTypeContent->id) }}@else{{ route('user.reports.store') }}@endif" method="POST" enctype="multipart/form-data">
                  <!-- PUT Method if we are editing -->
                {{--@if( isSuperAdmin() || Auth::user()->hasRole('administrator'))--}}
  
                  @if(isset($dataTypeContent->id))
                  {{ method_field("PUT") }}
                @endif
                {{--@endif--}}
                
                <!-- CSRF TOKEN -->
                  {{ csrf_field() }}
                  
                  <div class="panel-body">
                    
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
                  
                  <!-- If we are editing -->
                    @if(isset($dataTypeContent->id))
                      <?php $dataTypeRows = $dataType->editRows; ?>
                    @else
                      <?php $dataTypeRows = $dataType->addRows; ?>
                    @endif
                    
                    @foreach($dataTypeRows as $row)
                      @if($row->field == 'tenant_id')
                        
                        @if(isSuperAdmin())
                          <div class="form-group">
                            <label for="tenant_id">Tenant</label>
                            <select class="form-control" name="tenant_id"
                                    placeholder="Tenant" id="tenant_id"
                                    value="@if(isset($dataTypeContent->tenant_id)){{ old('tenant_id', $dataTypeContent->tenant_id) }}@else{{old('tenant_id')}}@endif">
                              @foreach(DB::table('tenants')->get() as $tenant)
                                <option value="{{$tenant->id}}">{{$tenant->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        
                        @elseif(!isSuperAdmin())
                          <input type="hidden" id="tenant_id" name="tenant_id"
                                 value="@if(isset($dataTypeContent->id)){{ old('tenant_id', $dataTypeContent->tenant_id) }}@else{{Auth::user()->tenant_id}}@endif">
                        @endif
                      @elseif($row->field == 'sub_brand_id')
                          <div class="form-group">
                            <label for="sub_brand_id">Brand</label>
                            <select class="form-control" name="sub_brand_id"
                                    placeholder="sub_brand_id" id="sub_brand_id"
                                    value="@if(isset($dataTypeContent->sub_brand_id)){{ old('sub_brand_id', $dataTypeContent->sub_brand_id) }}@else{{old('sub_brand_id')}}@endif">
                              @foreach(Auth::user()->brand->subbrands as $sub_brand)
                                  <option value="{{$sub_brand->id}}"
                                          @if(isset($dataTypeContent->sub_brand_id) && ($sub_brand->id == $dataTypeContent->sub_brand_id))
                                          selected="selected"
                                      @endif
                                  >{{$sub_brand->name}}</option>
                              @endforeach
                            </select>
                          </div>
                      @elseif($row->field == 'user_id')
                        
                        @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                          <div class="form-group">
                            <label for="user_id">User</label>
                            <select class="form-control" name="user_id"
                                    placeholder="Tenant" id="user_id"
                                    value="@if(isset($dataTypeContent->user_id)){{ old('user_id', $dataTypeContent->user_id) }}@else{{old('user_id')}}@endif">
                              @foreach(DB::table('users')->get() as $user)
                                @if($user->role_id == DB::table('roles')->whereName('superadmin')->first()->id)
                                @else
                                  
                                  <option value="{{$user->id}}"
                                          @if(isset($dataTypeContent->user_id) && ($user->id == $dataTypeContent->user_id))
                                          selected="selected"
                                      @endif
                                  >{{$user->name}}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                        
                        @else
                          <input type="hidden" id="user_id" name="user_id"
                                 value="@if(isset($dataTypeContent->id)){{ old('user_id', $dataTypeContent->user_id) }}@else{{Auth::id()}}@endif">
                        @endif
                      @elseif($row->field == 'image')
                        <div class="form-group">
                          
                            <img class="images" id="imagepreview" style="width:100%;"
                                 src="@if(isset($dataTypeContent->image) ) {!! Voyager::image($dataTypeContent->image) !!} @endif ">
                          <div class="input-group">
                            <input type="text" class="form-control" readonly>
                            <label class="input-group-btn" for="image">
                    <span class="btn btn-primary form-control notMe">
                      <i class="voyager-camera"></i> Browse <input type="file" name="image" id="image" accept="image/*" style="display: none;">
                    </span>
                            </label>
                          </div>
                        </div>
                        @elseif($row->field == 'sent' && Auth::user()->hasRole('administrator') == false && !isSuperAdmin())
                      @else
                        <div class="form-group @if($row->type == 'hidden') hidden @endif">
                          <?php $options = json_decode($row->details); ?>
                          <label for="{{$row->name}}">{{ $row->display_name }}@if(isset($options->required)) <span class="required_label">*</span> @endif</label>
                          @include('vendor.voyager.multilingual.input-hidden-bread-edit-add')
                          {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                          
                          @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                          @endforeach
                        </div>
                      @endif
                    @endforeach
                    
                    
                    <button type="submit" class="btn btn-block btn-primary save"
                            @if((Auth::check()) && ($brand = Auth::user()->brand) && (isset($brand->color)))
                            style="background-color: {{$brand->color}};
                                border-color: {{$brand->color}};
    
                                "
                            @else
                            style="background: #3c5064;
                          border-color: #3c5064;
                          "
                        @endif
                    >Save</button>
                  </div>
                </form>
                
                <iframe id="form_target" name="form_target" style="display:none"></iframe>
                <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                      enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                  <input name="image" id="upload_file" type="file"
                         onchange="$('#my_form').submit();this.value='';">
                  <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                  {{ csrf_field() }}
                </form>
              
              </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade modal-danger" id="confirm_delete_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"
                    aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="voyager-warning"></i> Are You Sure</h4>
          </div>
          
          <div class="modal-body">
            <h4>Are you sure you want to delete '<span class="confirm_delete_name"></span>'</h4>
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger" id="confirm_delete">Yes, Delete it!
            </button>
          </div>
        </div>
      </div>
    </div>
    </div>
    <!-- End Delete File Modal -->
    @stop
    
    @section('javascript')
  
      <!-- Scripts -->
        @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
        <script type="text/javascript" src="{{ voyager_asset('lib/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ voyager_asset('js/bootstrap-toggle.min.js') }}"></script>
        <script type="text/javascript" src="{{ voyager_asset('js/moment-with-locales.min.js') }}"></script>
        <script type="text/javascript" src="{{ voyager_asset('js/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
  @endif
        <script>
        var params = {}
        var $image

        $('document').ready(function () {
          $('.toggleswitch').bootstrapToggle();

          //Init datepicker for date fields if data-datepicker attribute defined
          //or if browser does not handle date inputs
          $('.form-group input[type=date]').each(function (idx, elt) {
            if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
              elt.type = 'text';
              $(elt).datetimepicker($(elt).data('datepicker'));
            }
          });
          
          @if ($isModelTranslatable)
          $('.side-body').multilingual({"editing": true});
          @endif

          $('.side-body input[data-slug-origin]').each(function(i, el) {
            $(el).slugify();
          });

          $('.form-group').on('click', '.remove-multi-image', function (e) {
            $image = $(this).siblings('img');

            params = {
              slug:   '{{ $dataTypeContent->getTable() }}',
              image:  $image.data('image'),
              id:     $image.data('id'),
              field:  $image.parent().data('field-name'),
              _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text($image.data('image'));
            $('#confirm_delete_modal').modal('show');
          });

          $('#confirm_delete').on('click', function(){
            $.post('{{ route('voyager.media.remove') }}', params, function (response) {
              if ( response
                  && response.data
                  && response.data.status
                  && response.data.status == 200 ) {

                toastr.success(response.data.message);
                $image.parent().fadeOut(300, function() { $(this).remove(); })
              } else {
                toastr.error("Error removing image.");
              }
            });

            $('#confirm_delete_modal').modal('hide');
          });
          $('[data-toggle="tooltip"]').tooltip();
        });
        $(document).on('change', ':file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });
        $(document).ready( function() {
          $(':file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
              input.val(log);
            } else {
              if( log ) alert(log);
            }
            var file = event.target.files[0],
                reader = new FileReader();

            reader.onload = function(c){

              $('#imagepreview').attr('src',c.target.result);
            }
            reader.readAsDataURL(file);
          });
        });
      </script>
      @if($isModelTranslatable)
        <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
      @endif
      <script src="{{ voyager_asset('lib/js/tinymce/tinymce.min.js') }}"></script>
      <script src="{{ voyager_asset('js/voyager_tinymce.js') }}"></script>
      <script src="{{ voyager_asset('lib/js/ace/ace.js') }}"></script>
      <script src="{{ voyager_asset('js/voyager_ace_editor.js') }}"></script>
      <script src="{{ voyager_asset('js/slugify.js') }}"></script>
@stop
