@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )

@section('page_title','All '.$dataType->display_name_plural)
@push('css')
  
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/browse.css') }}" rel="stylesheet">
  @endif

@endpush

@section('page_header')
  <h1 class="page-title">
    <i class="voyager-news"></i> {{ $dataType->display_name_plural }}
    @if (Voyager::can('add_'.$dataType->name))
      <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success">
        <i class="voyager-plus"></i> Add New
      </a>
    @endif
  </h1>
  @include('vendor.voyager.multilingual.language-selector')
@stop

@section('content')
  
  <div class="page-content container-fluid">
    @include('vendor.voyager.alerts')
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-bordered welcome-panel">
          <div class="panel-body">
            <table id="dataTable" class="table">
              <thead>
              <tr>
                @foreach($dataType->browseRows as $row)
                  
                  @if(!isSuperAdmin() && $row->display_name == 'Tenant')
                  @elseif((!isSuperAdmin() && !Auth::user()->hasRole('administrator')) && $row->display_name == 'User')
                  
                  @else
                    <th>{{ $row->display_name }}</th>
                  @endif
                
                @endforeach
                <th class="actions">Actions</th>
              </tr>
              </thead>
              <tbody>
              @foreach($dataTypeContent as $data)
                <tr data-id="{{$data->id}}" @if($data->sent && (isSuperAdmin() || Auth::user()->hasRole('administrator'))) style="background-color: rgba(153,255,153,0.32)" @endif >
                  @foreach($dataType->browseRows as $row)
                    {{--{{dd($row)}}--}}
                    
                    @if(!isSuperAdmin() && $row->display_name == 'Tenant')
                    @elseif((!isSuperAdmin() && !Auth::user()->hasRole('administrator')) && $row->display_name == 'User')
                    @elseif($row->field == 'sent' && (isSuperAdmin() || Auth::user()->hasRole('administrator')))
                      <td class="sent_report">@if($data->{$row->field}) <div class="icon voyager-check-circle"></div> @endif</td>
                  @elseif($row->field == 'sent' && !(isSuperAdmin() || Auth::user()->hasRole('administrator')))
                    @else
                      <td class="{{$row->field}}">
                        @if($row->type == 'image' || $row->display_name == 'Image')
                          {{--{{$data->image}}--}}
                          {{--{{$row->field}}--}}
                          {{--{{strpos($data->{$row->field}, 'http://')}}--}}
                          {{--{{strpos($data->{$row->field}, 'https://')}}--}}
                          {{--{{Voyager::image( $data->{$row->field} )}}--}}
                          {{--{{ $data->{$row->field} }}--}}
                          <img
                              src="@if( strpos($data->{$row->field}, 'http://') === false && strpos($data->{$row->field}, 'https://') === false){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif"
                              style="width:100px">
                          {{--@elseif($row->display_name == 'Accepted')--}}
                          {{--@if($data->accepted)--}}
                          {{--<a href="#" id="accepted-{{$data->id}}"--}}
                          {{--class="btn-sm btn-success pull-left accepted disabled" role="button">--}}
                          {{--<i class="voyager-check-circle"></i> <span>Accepted</span>--}}
                          {{--</a>--}}
                          {{--@else--}}
                          {{--<a href="#" id="accepted-{{$data->id}}"--}}
                          {{--class="btn-sm btn-warning pull-left pending disabled" role="button">--}}
                          {{--<i class="voyager-exclamation"></i> <span>Pending</span>--}}
                          {{--</a>--}}
                          {{--@endif--}}
                        
                        @else
                          
                          @if(is_field_translatable($data, $row))
                            @include('vendor.voyager.multilingual.input-hidden', [
                                '_field_name'  => $row->field,
                                '_field_trans' => get_field_translations($data, $row->field)
                            ])
                          @endif
                          
                          <span>{{ $data->{$row->field} }}
                            @if($row->display_name == 'User')
                              {{($user = App\User::find($data->userId)) ? ' - '.$user[0]->email : ''}}
                            @endif
                                </span>
                        @endif
                      </td>
                    @endif
                  @endforeach
                  <td class="no-sort no-click"">
                    @if (Voyager::can('delete_'.$dataType->name))
                      @if ($data->author_id === Auth::user()->id || isSuperAdmin() || Auth::user()->hasRole('administrator'))
                        <div class="btn-sm btn-danger pull-right delete" data-id="{{ $data->id }}">
                          {{--<i class="voyager-trash"></i> --}}
                          <span>Delete</span>
                        </div>
                      @endif
                    @endif
                    
                    @if (Voyager::can('read_'.$dataType->name))
                      @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                        <a href="{{ route('voyager.'.$dataType->slug.'.show', $data->id) }}"
                           class="btn-sm btn-warning pull-right">
                          {{--<i class="voyager-eye"></i> --}}
                          <span>View</span>
                        </a>
                      @else
                        <a href="{{ route('user.'.$dataType->slug.'.show', $data->id) }}"
                           class="btn-sm btn-warning">
                          {{--<i class="voyager-eye"></i> --}}
                          View
                        </a>
                      @endif
                    
                    @endif
                    @if (Voyager::can('edit_'.$dataType->name))
                      
                      @if(isSuperAdmin() || Auth::user()->hasRole('administrator'))
                        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $data->id) }}"
                           class="btn-sm btn-primary pull-right edit">
                          {{--<i class="voyager-edit"></i> --}}
                          <span>Edit</span>
                        </a>
                      @elseif($data->getUserId() == Auth::id() && Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::parse($data->created_at)) <= 30  )
                        <a href="{{ route('user.'.$dataType->slug.'.edit', $data->id) }}"
                           class="btn-sm btn-primary edit">
                          {{--<i class="voyager-edit"></i> --}}
                          Edit
                          <em class="minutesleft">( {{30 - (Carbon\Carbon::now()->diffInMinutes(Carbon\Carbon::parse($data->created_at)))}} minutes left )</em>
                        </a>
                      @endif
                    @endif
                    {{--@if(isSuperAdmin() || Auth::user()->hasRole('administrator'))--}}
                    {{--@if(!$data->accepted)--}}
                    {{--<div class="btn-sm btn-success pull-right accept" data-id="{{ $data->id }}">--}}
                    {{--<i class="voyager-check"></i> Accept--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--@endif--}}
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
            @if (isset($dataType->server_side) && $dataType->server_side)
              <div class="pull-left">
                <div role="status" class="show-res" aria-live="polite">Showing {{ $dataTypeContent->firstItem() }}
                  to {{ $dataTypeContent->lastItem() }} of {{ $dataTypeContent->total() }} entries
                </div>
              </div>
              <div class="pull-right">
                {{ $dataTypeContent->links() }}
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  
  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">
            <i class="voyager-trash"></i> Are you sure you want to delete this {{ $dataType->display_name_singular }}?
          </h4>
        </div>
        <div class="modal-footer">
          <form action="{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}" id="delete_form"
                method="POST">
            {{ method_field("DELETE") }}
            {{ csrf_field() }}
            <input type="submit" class="btn btn-danger pull-right delete-confirm"
                   value="Yes, Delete This {{ $dataType->display_name_singular }}">
          </form>
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript')
  {{-- DataTables --}}
  <script>
    $(document).ready(function () {
      {{--@if (!$dataType->server_side)--}}
      {{--//      $('#dataTable').DataTable({"order": []});--}}
      {{--@endif--}}
      @if ($isModelTranslatable)
      $('.side-body').multilingual();
      @endif
    });

    $('td').on('click', '.delete', function (e) {
      $('#delete_form')[0].action = $('#delete_form')[0].action.replace('__id', $(this).data('id'));
      $('#delete_modal').modal('show');
    });
    //    $('td').on('click', '.accept', function () {
    //      var button = $(this);
    //      var id = button.data('id');
    //      $.ajax({
    //        type: "GET",
    //        url: document.location.origin + '/report/' + id + '/setaccepted',
    //        datatype: "json",
    //        success: function (data) {
    //          if (data['success']) {
    //            var div = $('#accepted-' + id),
    //                icon = div.find('i');
    //            div.toggleClass('btn-warning btn-success').find('span').text('Accepted');
    //            icon.toggleClass('voyager-check-circle voyager-exclamation');
    //            button.remove();
    //          }
    //        }
    //
    //      });
    //    });
  </script>
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <script>
      $('tr').on('click',function (e){
        var row = $(this),
            id = row.data('id');
        window.location.href = document.location.origin+'/user/reports/'+id;
      })
    </script>
  @endif
  @if($isModelTranslatable)
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
  @endif
@stop