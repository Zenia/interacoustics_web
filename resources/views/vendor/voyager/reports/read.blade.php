@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )

@push('css')
  
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/read.css') }}" rel="stylesheet">
  @endif

@endpush

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> Viewing {{ ucfirst($dataType->display_name_singular) }}
  </h1>
@stop

@section('content')
  <div class="page-content container-fluid">
    <div class="row">
      <div class="col-md-4 col-md-offset-4 col-sm-12">
        
        <div class="panel panel-bordered" style="padding-bottom:5px;">
          
          <!-- /.box-header -->
          <!-- form start -->
          <div class="panel-body" style="padding-top:0;">
            
            @foreach($dataType->readRows as $row)
              @if(!is_null($dataTypeContent->{$row->field}))
              @if(!isSuperAdmin() && $row->display_name == 'Tenant')
              @elseif($row->display_name == 'role_id')
              @elseif((!isSuperAdmin() && !Auth::user()->hasRole('administrator')) && $row->display_name == 'User')
              
              @elseif($row->display_name == 'Accepted')
                <div class="form-group" style="padding-top:0;">
                  <label><p>{{ $row->display_name }}:</p></label>
                  
                  @if($dataTypeContent->accepted)
                    <a href="#" class="btn btn-block btn-success disabled" role="button">
                      <i class="voyager-check-circle"></i> <span>Accepted</span>
                    </a>
                  @else
                    <a href="#" class="btn btn-block btn-warning disabled" role="button">
                      <i class="voyager-exclamation"></i> <span>Pending</span>
                    </a>
                  @endif
                </div>
              @elseif($row->type == "image" || $row->display_name == 'Image')
                
                <img style="max-width:640px"
                     src="{!! Voyager::image($dataTypeContent->{$row->field}) !!}">
              @elseif($row->display_name == 'Persons Injured' || $row->display_name == 'Product will be returned')
                <div class="form-group" style="padding-top:0;">
                  <label>{{ $row->display_name }}:</label>
                  
                  @if($row->type == 'text_area')
                    <textarea class="form-control readonly"
                              readonly>{{ ($dataTypeContent->{$row->field}) ? 'Yes' : 'No' }}</textarea>
                  @else
                    <input class="form-control readonly" readonly
                           value="{{ ($dataTypeContent->{$row->field}) ? 'Yes' : 'No' }}">
                  @endif
                </div>
              @else
                <div class="form-group" style="padding-top:0;">
                  <label>{{ $row->display_name }}:</label>
                  
                  @if($row->type == 'text_area')
                    <textarea class="form-control readonly"
                              readonly>{{ ($dataTypeContent->{$row->field}) }}</textarea>
                  @else
                    <input class="form-control readonly" readonly
                           value="{{ ($dataTypeContent->{$row->field}) }} @if($row->display_name == 'User'){{($user = App\User::find($dataTypeContent->userId)) ? ' - '.$user[0]->email : ''}}@endif">
                  @endif
                </div>
              @endif
              @endif
            @endforeach
          
          
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript')

@stop
