<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  if((Auth::check()) && (isSuperAdmin() || Auth::user()->hasRole('administrator'))) {
    return redirect(route('voyager.dashboard'));
  }
  else return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
 
  Voyager::routes();



// OAuth Routes
  Route::get('auth/{provider}', 'SocialController@redirectToProvider');
  Route::get('auth/{provider}/callback', 'SocialController@handleProviderCallback');
});

Route::group(['middleware' => ['web','auth']], function () {
  
  Route::get('user/reports', 'Voyager\Controllers\VoyagerBreadController@index')->name('user.reports.index');
  Route::get('user/reports/create', 'Voyager\Controllers\VoyagerBreadController@create')->name('user.reports.create');
  Route::get('user/reports/{report}/edit', 'Voyager\Controllers\VoyagerBreadController@edit')->name('user.reports.edit')
       ;
  Route::get('user/reports/{report}', 'Voyager\Controllers\VoyagerBreadController@show')->name('user.reports.show');
  Route::get('user/users/{id}/edit', 'Voyager\Controllers\VoyagerBreadController@edit')->name('user.users.edit');
  
  
  Route::match([ 'post',
                 'put'
  ], 'user/users/{id}/update', 'Voyager\Controllers\VoyagerBreadController@update')->name('user.users.update');
  
  Route::post('report/reports/store','Voyager\Controllers\VoyagerBreadController@store')->name('user.reports.store');
  Route::match(['post','put'],'report/reports/{id}/update','Voyager\Controllers\VoyagerBreadController@update')->name('user.reports.update');
  Route::get('report/{id}/setaccepted','Api\ReportsController@setAccepted');
  Route::get('users/{id}/resetpassword','AdminController@sendUserResetPasswordMail');
  Route::get('users/{id}/acceptrequest','AdminController@acceptrequest');
  Route::get('users/{id}/rejectrequest','AdminController@rejectrequest');
  
  Route::get('users/{id}/requestbrandchange','AdminController@requestbrandchange')->name('requestbrandchange');
  Route::post('users/{id}/sendrequestbrandchange','AdminController@sendrequestbrandchange')->name('requestbrandchangepost');


  Route::get('admin/admin','AdminController@index')->name('voyager.adminsettings.index');
  Route::post('admin/adminsettings','AdminController@storeadminsettings')->name('voyager.adminsettings.store');
  
});

Auth::routes();
Route::get('login/{provider}', 'Api\AuthController@redirectToProvider');
Route::get('login/{provider}/callback', 'Api\AuthController@handleProviderCallback');


Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
