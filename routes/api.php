<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$api = app('Dingo\Api\Routing\Router');
$api->version('v1', [ 'namespace' => 'App\Http\Controllers\Api' ], function ( $api ) {
  
  $api->post('register', 'AuthController@register');
  $api->post('authenticate', 'AuthController@authenticate');
//  $api->post('requestresetpasswordmail', 'AuthController@requestResetPasswordMail');
  
  $api->post('login/{provider}', 'AuthController@loginOrCreateWithProvider');
  $api->post('requestresetpasswordmail', 'UserController@requestresetpasswordmail');
  
});

$api->version('v1', [ 'middleware' => 'jwt.auth', 'namespace' => 'App\Http\Controllers\Api' ], function ( $api ) {
//$api->version('v1', [ 'namespace' => 'App\Http\Controllers\Api' ], function ( $api ) {
  $api->get('logout', 'AuthController@logout');
  
  $api->resource('users', 'UserController', [ 'except' => [ 'create', 'edit','show','update' ] ]);
  $api->get('gettenantbrands', 'TenantController@getBrands');
  $api->get('user', 'UserController@show');
  $api->get('user/reports', 'UserController@reports');
  $api->get('user/brand', 'UserController@brand');
  $api->get('user/tenant', 'UserController@tenant');
  $api->post('user/uploadavatar', 'UserController@uploadavatar');
  $api->post('user/update', 'UserController@update');
  $api->get('user/deleteavatar', 'UserController@deleteavatar');
  $api->resource('brands', 'BrandsController', [ 'except' => [ 'create', 'edit' ] ]);
  $api->resource('reports', 'ReportsController', [ 'except' => [ 'create', 'edit', 'update' ] ]);
  $api->post('reports/{report_id}/update', 'ReportsController@update');
  $api->post('report/{report_id}/uploadimage', 'ReportsController@uploadimage');
  $api->get('report/{report_id}/deleteimage', 'ReportsController@deleteimage');
  $api->resource('tenants', 'TenantController', [ 'except' => [ 'create', 'edit', 'show', 'destroy' ] ]);
  
  $api->get('report/{id}/setaccepted', 'ReportsController@setAccepted');
  
  $api->post('kontakt', 'UserController@kontakt');
  $api->post('sendrequestbrandchange','AuthController@sendrequestbrandchange');


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
});
