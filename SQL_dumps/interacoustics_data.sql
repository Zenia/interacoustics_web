-- --------------------------------------------------------
-- Vært:                         umvuni.dk
-- Server-version:               10.1.14-MariaDB-1~xenial - mariadb.org binary distribution
-- ServerOS:                     debian-linux-gnu
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table interacoustics.brands: ~4 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
REPLACE INTO `brands` (`id`, `slug`, `name`, `created_at`, `updated_at`, `tenant_id`) VALUES
	(1, 'koretoj', 'Køretøj', '2017-09-01 21:28:24', '2017-09-01 21:28:24', 1),
	(2, 'arstid', 'Årstid', '2017-09-01 21:29:32', '2017-09-01 21:29:32', 1),
	(3, 'frugt', 'Frugt', '2017-09-01 21:29:45', '2017-09-01 21:29:45', 1),
	(4, 'film', 'Film', '2017-09-01 21:29:58', '2017-09-01 21:29:58', 1);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping data for table interacoustics.brand_models: ~13 rows (approximately)
/*!40000 ALTER TABLE `brand_models` DISABLE KEYS */;
REPLACE INTO `brand_models` (`id`, `slug`, `name`, `brand_id`, `created_at`, `updated_at`, `tenant_id`) VALUES
	(1, 'motorcykel', 'Motorcykel', 1, '2017-09-01 21:30:13', '2017-09-01 21:30:13', 1),
	(2, 'kampvogn', 'Kampvogn', 1, '2017-09-01 21:30:22', '2017-09-01 21:30:22', 1),
	(3, 'bil', 'Bil', 1, '2017-09-01 21:30:33', '2017-09-01 21:30:33', 1),
	(4, 'cykel', 'Cykel', 1, '2017-09-01 21:30:43', '2017-09-01 21:30:43', 1),
	(5, 'sommer', 'Sommer', 2, '2017-09-01 21:30:54', '2017-09-01 21:30:54', 1),
	(6, 'vinter', 'Vinter', 2, '2017-09-01 21:32:09', '2017-09-01 21:32:09', 1),
	(7, 'efterar', 'Efterår', 2, '2017-09-01 21:32:21', '2017-09-01 21:32:21', 1),
	(8, 'forar', 'Forår', 2, '2017-09-01 21:32:31', '2017-09-01 21:32:31', 1),
	(9, 'vandmelon', 'Vandmelon', 3, '2017-09-01 21:32:40', '2017-09-01 21:32:40', 1),
	(10, 'paerer', 'Pærer', 3, '2017-09-01 21:32:48', '2017-09-01 21:32:48', 1),
	(11, 'citron', 'Citron', 3, '2017-09-01 21:32:58', '2017-09-01 21:32:58', 1),
	(12, 'the-fifth-element', 'The fifth element', 4, '2017-09-01 22:00:12', '2017-09-01 22:00:12', 1),
	(13, 'les-miserable', 'Les Miserable', 4, '2017-09-01 22:00:27', '2017-09-01 22:00:27', 1);
/*!40000 ALTER TABLE `brand_models` ENABLE KEYS */;

-- Dumping data for table interacoustics.categories: ~2 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
REPLACE INTO `categories` (`id`, `tenant_id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, 1, 'News', 'news', '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(2, 1, NULL, 1, 'Offers', 'offers', '2017-09-01 20:09:56', '2017-09-01 20:09:56');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table interacoustics.data_rows: ~86 rows (approximately)
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (1, 7, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (2, 6, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (3, 5, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (4, 4, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (5, 3, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (6, 1, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (7, 2, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (8, 2, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (9, 1, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (10, 1, 'author_id', 'TEXT', 'Author', 1, 0, 1, 1, 0, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (11, 1, 'category_id', 'select_dropdown', 'Post Category', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (12, 1, 'price', 'NUMBER', 'Price', 0, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (13, 1, 'tags', 'TEXT', 'tags', 0, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (14, 1, 'title', 'TEXT', 'Titel', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (15, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (16, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (17, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height": "null"},"quality": "70%","upsize": TRUE,"thumbnails": [{"name":"medium","scale": "50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (18, 1, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (19, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (20, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (21, 1, 'STATUS', 'select_dropdown', 'STATUS', 1, 1, 1, 1, 1, 1, '{"default":"DRAFT","options": {"PUBLISHED":"published","DRAFT": "draft","PENDING": "pending"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (22, 1, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (23, 1, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (24, 3, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (25, 3, 'author_id', 'TEXT', 'author_id', 1, 0, 0, 0, 0, 0, '{"relationship":{"key":"id","label": "name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (26, 3, 'title', 'TEXT', 'Titel', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (27, 3, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (28, 3, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (29, 3, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (30, 3, 'meta_description', 'TEXT', 'meta_description', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (31, 3, 'meta_keywords', 'TEXT', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (32, 3, 'STATUS', 'select_dropdown', 'STATUS', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE", "ACTIVE":"ACTIVE"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (33, 3, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (34, 3, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (35, 3, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (36, 4, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, NULL, 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (37, 4, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, NULL, 1, 2);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (38, 4, 'email', 'TEXT', 'email', 1, 1, 1, 1, 1, 1, NULL, 1, 3);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (39, 4, 'PASSWORD', 'PASSWORD', 'PASSWORD', 1, 0, 0, 1, 1, 0, NULL, 1, 4);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (40, 4, 'remember_token', 'TEXT', 'remember_token', 0, 0, 0, 0, 0, 0, NULL, 1, 5);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (41, 4, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 6);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (42, 4, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 7);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (43, 4, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000", "height":"null"},"quality":"70%", "upsize":true,"thumbnails":[{"name":"medium", "scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300", "height":"250"}}]}',1, 8);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (44, 6, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (45, 6, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (46, 6, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (47, 6, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (48, 5, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (49, 5, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{"default":"", "null":"","options":{"":"-- None --"},"relationship":{"key":"id", "label":"name"}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)VALUES (50, 5, 'ORDER', 'NUMBER', 'ORDER', 1, 1, 1, 1, 1, 1, '{"default":1}', 1, 1);INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (51, 5, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (52, 5, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (53, 5, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 1, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (54, 5, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (55, 5, 'background_image', 'TEXT', 'background_image', 0, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (56, 5, 'background_color', 'TEXT', 'background_color', 0, 0, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (57, 7, 'id', 'NUMBER', 'id', 1, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (58, 7, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (59, 7, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (60, 7, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (61, 7, 'display_name', 'TEXT', 'Display NAME ', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (62, 1, 'seo_title', 'TEXT', 'seo_title', 0, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (63, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (64, 4, 'role_id', 'select_dropdown', 'Role', 0, 1, 1, 1, 1, 1, '', 1, 9);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (65, 8, 'id', 'NUMBER', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (66, 8, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 2);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (67, 8, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 0, NULL, 1, 3);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (68, 8, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 4);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (69, 8, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 5);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (70, 8, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 0, '{"relationship":{"key":"id","label": "name"}}', 1, 6);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (71, 9, 'id', 'NUMBER', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (72, 9, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 2);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (73, 9, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, NULL, 1, 3);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (74, 9, 'brand_id', 'select_dropdown', 'Brand', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 4);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (75, 9, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 5);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (76, 9, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 6);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (77, 9, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 7);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (78, 10, 'id', 'NUMBER', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (79, 10, 'slug', 'TEXT', 'slug', 0, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 6);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (80, 10, 'NAME', 'TEXT', 'NAME', 0, 1, 1, 1, 1, 1, NULL, 1, 5);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (81, 10, 'user_id', 'select_dropdown', 'USER', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 7);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (82, 10, 'image', 'FILE', 'Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000", "height":"null"},  "quality":"70%", "upsize":true,  "thumbnails":[{"name":"medium", "scale":"50%"},  {"name":"small","scale":"25%"},  {"name":"cropped","crop":{"width":"300", "height":"250"}}]}',  1, 3);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (83, 10, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, NULL, 1, 10);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (84, 10, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 12);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (85, 10, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 11);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (87, 10, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 2);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (88, 11, 'id', 'NUMBER', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1, 1);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (89, 11, 'slug', 'TEXT', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name", "forceUpdate":true}}', 1, 2);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (90, 11, 'NAME', 'TEXT', 'NAME', 1, 1, 1, 1, 1, 1, NULL, 1, 3);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (91, 11, 'brand_id', 'select_dropdown', 'Brand', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 4);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (92, 11, 'created_at', 'TIMESTAMP', 'Created AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 5);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (93, 11, 'updated_at', 'TIMESTAMP', 'Updated AT ', 0, 0, 0, 0, 0, 0, NULL, 1, 6);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (94, 11, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 7);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (97, 10, 'barcode', 'TEXT', 'Stregkode', 0, 1, 1, 1, 1, 1, NULL, 1, 4);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (98, 10, 'brand_id', 'select_dropdown', 'Brand', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 8);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (99, 10, 'model_id', 'select_dropdown', 'Model', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label": "name"}}', 1, 9);
INSERT INTO data_rows (id, data_type_id, field, `type`, display_name, required, browse, `read`, edit, `add`, `delete`, details, tenant_id, `order`)
VALUES (100, 10, 'accepted', 'checkbox', 'Accepted', 1, 1, 1, 1, 0, 0, '{"on":"Accepted", "off":"Pending","checked":"true"}', 1, 13);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Dumping data for table interacoustics.data_types: ~10 rows (approximately)
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
REPLACE INTO `data_types` (`id`, `tenant_id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
	(1, 1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'App\\Post', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(2, 1, 'tenants', 'tenants', 'Tenant', 'Tenants', 'voyager-world', 'App\\Tenant', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(3, 1, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'App\\Page', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(4, 1, 'users', 'users', 'User', 'Users', 'voyager-person', 'App\\User', NULL, NULL, 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:36:58'),
	(5, 1, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'App\\Category', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(6, 1, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'App\\Menu', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(7, 1, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'App\\Role', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(8, 1, 'brands', 'brands', 'Brand', 'Brands', NULL, 'App\\Brand', NULL, NULL, 1, 0, '2017-09-01 20:32:27', '2017-09-01 20:32:27'),
	(9, 1, 'brandmodels', 'models', 'Model', 'Models', NULL, 'App\\BrandModel', NULL, NULL, 1, 0, '2017-09-01 20:34:05', '2017-09-01 20:40:48'),
	(10, 1, 'reports', 'reports', 'Report', 'Reports', 'voyager-file-text', 'App\\Report', NULL, NULL, 1, 0, '2017-09-01 20:35:50', '2017-09-02 09:56:36');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Dumping data for table interacoustics.menus: ~0 rows (approximately)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
REPLACE INTO `menus` (`id`, `tenant_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'admin', '2017-09-01 20:09:28', '2017-09-01 20:09:28');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping data for table interacoustics.menu_items: ~12 rows (approximately)
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
REPLACE INTO `menu_items` (`id`, `tenant_id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 1, 'Dashboard', '/admin', '_self', 'voyager-boat', NULL, NULL, 1, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL, NULL),
	(2, 1, 1, 'Cities', '/admin/tenants', '_self', 'voyager-world', NULL, NULL, 2, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL, NULL),
	(3, 1, 1, 'Media', '/admin/media', '_self', 'voyager-images', NULL, NULL, 6, '2017-09-01 20:09:28', '2017-09-01 20:40:29', NULL, NULL),
	(5, 1, 1, 'Users', '/admin/users', '_self', 'voyager-person', NULL, NULL, 7, '2017-09-01 20:09:28', '2017-09-01 20:40:29', NULL, NULL),
	(8, 1, 1, 'Roles', '/admin/roles', '_self', 'voyager-lock', NULL, NULL, 8, '2017-09-01 20:09:28', '2017-09-01 20:40:29', NULL, NULL),
	(9, 1, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2017-09-01 20:09:28', '2017-09-01 20:40:29', NULL, NULL),
	(10, 1, 1, 'Menu Builder', '/admin/menus', '_self', 'voyager-list', NULL, 9, 1, '2017-09-01 20:09:28', '2017-09-01 20:39:29', NULL, NULL),
	(11, 1, 1, 'Database', '/admin/database', '_self', 'voyager-data', NULL, 9, 2, '2017-09-01 20:09:28', '2017-09-01 20:39:30', NULL, NULL),
	(12, 1, 1, 'Settings', '/admin/settings', '_self', 'voyager-settings', NULL, NULL, 9, '2017-09-01 20:09:28', '2017-09-01 20:40:29', NULL, NULL),
	(13, 1, 1, 'Reports', 'admin/reports', '_self', 'voyager-file-text', '#000000', NULL, 3, '2017-09-01 20:38:49', '2017-09-01 22:37:15', NULL, ''),
	(14, 1, 1, 'Brands', 'admin/brands', '_self', 'voyager-tag', '#000000', NULL, 4, '2017-09-01 20:40:06', '2017-09-01 20:40:09', NULL, ''),
	(15, 1, 1, 'Models', 'admin/models', '_self', 'voyager-puzzle', '#000000', NULL, 5, '2017-09-01 20:40:26', '2017-09-01 22:38:03', NULL, '');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Dumping data for table interacoustics.migrations: ~25 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
REPLACE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_01_01_000000_create_pages_table', 1),
	(6, '2016_01_01_000000_create_posts_table', 1),
	(7, '2016_02_15_204651_create_categories_table', 1),
	(8, '2016_05_19_173453_create_menu_table', 1),
	(9, '2016_10_21_190000_create_roles_table', 1),
	(10, '2016_10_21_190000_create_settings_table', 1),
	(11, '2016_11_30_135954_create_permission_table', 1),
	(12, '2016_11_30_141208_create_permission_role_table', 1),
	(13, '2016_12_26_201236_data_types__add__server_side', 1),
	(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(15, '2017_01_14_005015_create_translations_table', 1),
	(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
	(17, '2017_01_15_000000_create_permission_groups_table', 1),
	(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(20, '2017_04_04_111150_add_tenant_id_field_to_all', 1),
	(21, '2017_04_12_104217_create_tenant_table', 1),
	(22, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(23, '2017_09_03_113347_create_brand_models_table', 0),
	(24, '2017_09_03_113347_create_brands_table', 0),
	(25, '2017_09_03_113347_create_reports_table', 0);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping data for table interacoustics.pages: ~0 rows (approximately)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
REPLACE INTO `pages` (`id`, `tenant_id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/AAgCCnqHfLlRub9syUdw.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping data for table interacoustics.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
REPLACE INTO `password_resets` (`email`, `token`, `created_at`, `tenant_id`) VALUES
	('admin@admin.com', '$2y$10$MjjXAHF5xEN70CSLG3m3ouyIDsFYA.KZ0ygBztAiAI5YYNxH/zsre', '2017-09-02 22:50:00', 1);
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping data for table interacoustics.permissions: ~49 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
REPLACE INTO `permissions` (`id`, `tenant_id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
	(1, 1, 'browse_admin', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(2, 1, 'browse_database', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(3, 1, 'browse_media', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(4, 1, 'browse_settings', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(5, 1, 'browse_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(6, 1, 'read_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(7, 1, 'edit_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(8, 1, 'add_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(9, 1, 'delete_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(10, 1, 'browse_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(11, 1, 'read_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(12, 1, 'edit_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(13, 1, 'add_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(14, 1, 'delete_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(15, 1, 'browse_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(16, 1, 'read_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(17, 1, 'edit_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(18, 1, 'add_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(19, 1, 'delete_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(20, 1, 'browse_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(21, 1, 'read_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(22, 1, 'edit_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(23, 1, 'add_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(24, 1, 'delete_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(25, 1, 'browse_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(26, 1, 'read_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(27, 1, 'edit_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(28, 1, 'add_posts', 'posts', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(29, 1, 'delete_posts', 'posts', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(30, 1, 'browse_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(31, 1, 'read_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(32, 1, 'edit_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(33, 1, 'add_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(34, 1, 'delete_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(35, 1, 'browse_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(36, 1, 'read_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(37, 1, 'edit_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(38, 1, 'add_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(39, 1, 'delete_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(40, 1, 'browse_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(41, 1, 'read_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(42, 1, 'edit_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(43, 1, 'add_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(44, 1, 'delete_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(45, 1, 'browse_reports', 'reports', '2017-09-01 20:35:50', '2017-09-01 20:35:50', NULL),
	(46, 1, 'read_reports', 'reports', '2017-09-01 20:35:50', '2017-09-01 20:35:50', NULL),
	(47, 1, 'edit_reports', 'reports', '2017-09-01 20:35:50', '2017-09-01 20:35:50', NULL),
	(48, 1, 'add_reports', 'reports', '2017-09-01 20:35:50', '2017-09-01 20:35:50', NULL),
	(49, 1, 'delete_reports', 'reports', '2017-09-01 20:35:50', '2017-09-01 20:35:50', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping data for table interacoustics.permission_groups: ~0 rows (approximately)
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;

-- Dumping data for table interacoustics.permission_role: ~78 rows (approximately)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
REPLACE INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(1, 3),
	(2, 1),
	(2, 3),
	(3, 1),
	(3, 3),
	(4, 1),
	(4, 3),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(20, 3),
	(21, 1),
	(21, 3),
	(22, 1),
	(22, 3),
	(23, 1),
	(23, 3),
	(24, 1),
	(24, 3),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(31, 1),
	(32, 1),
	(33, 1),
	(34, 1),
	(35, 1),
	(35, 3),
	(36, 1),
	(36, 3),
	(37, 1),
	(37, 3),
	(38, 1),
	(38, 3),
	(39, 1),
	(39, 3),
	(40, 1),
	(40, 3),
	(41, 1),
	(41, 3),
	(42, 1),
	(42, 3),
	(43, 1),
	(43, 3),
	(44, 1),
	(44, 3),
	(45, 1),
	(45, 2),
	(45, 3),
	(46, 1),
	(46, 2),
	(46, 3),
	(47, 1),
	(47, 2),
	(47, 3),
	(48, 1),
	(48, 2),
	(48, 3),
	(49, 1),
	(49, 2),
	(49, 3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping data for table interacoustics.posts: ~7 rows (approximately)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
REPLACE INTO `posts` (`id`, `tenant_id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 1, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/nlje9NZQ7bTMYOUG4lF1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(2, 2, 1, 2, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/7uelXHi85YOfZKsoS6Tq.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(3, 2, 1, 1, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/9txUSY6wb7LTBSbDPrD9.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(4, 2, 1, 1, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(5, 1, 1, 1, 'Yarr Post Host', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-host', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(6, 1, 1, 2, 'Yarr Post 2', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-2', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(7, 1, 1, 2, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-3', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping data for table interacoustics.reports: ~0 rows (approximately)
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
REPLACE INTO `reports` (`id`, `slug`, `name`, `user_id`, `image`, `description`, `created_at`, `updated_at`, `barcode`, `brand_id`, `model_id`, `tenant_id`, `accepted`) VALUES
	(1, NULL, 'Test 1', 1, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'jg', '2017-09-02 00:48:00', '2017-09-02 14:15:50', '321654', 2, 1, 1, 1),
	(2, NULL, 'Test 23', 3, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'qwe dette er test nr 2', '2017-09-02 00:56:19', '2017-09-03 01:15:05', '123321', 4, 1, 1, 0),
	(3, NULL, 'Test 3', 1, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', '123', '2017-09-02 01:02:15', '2017-09-02 14:15:47', '123123123', 1, 1, 1, 1),
	(4, NULL, 'test postman 2', 4, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 02:59:50', '2017-09-03 02:59:50', 'julemand 2', 1, 3, 1, 0),
	(5, NULL, 'test postman 2', 3, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:01:53', '2017-09-03 03:01:53', 'julemand 2', 1, 3, 1, 0),
	(6, NULL, 'test postman 2', 4, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:02:59', '2017-09-03 03:02:59', 'julemand 2', 1, 3, 1, 0),
	(7, NULL, 'test postman 2', 3, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:03:34', '2017-09-03 03:03:34', 'julemand 2', 1, 3, 1, 0),
	(8, NULL, 'test postman 2', 4, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:08:07', '2017-09-03 03:08:07', 'julemand 2', 1, 3, 1, 0),
	(9, NULL, 'test postman 2', 4, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:08:08', '2017-09-03 03:08:08', 'julemand 2', 1, 3, 1, 0),
	(10, NULL, 'test postman 2', 3, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:08:10', '2017-09-03 03:08:10', 'julemand 2', 1, 3, 1, 0),
	(11, NULL, 'test postman 2', 4, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 2', '2017-09-03 03:17:28', '2017-09-03 03:17:29', 'julemand 2', 1, 3, 1, 0),
	(12, NULL, 'test postman 2', 4, 'reports/September2017/0YMBs1hSChkAnkPfN4mr.JPG', 'test postman description 2', '2017-09-03 03:18:45', '2017-09-03 03:18:45', 'julemand 2', 1, 3, 1, 0),
	(13, NULL, 'test postman 2', 4, 'reports/September2017/KUrD4kSAdXCLp1sBB4xi.JPG', 'test postman description 2', '2017-09-03 03:20:20', '2017-09-03 03:20:20', 'julemand 2', 1, 3, 1, 0),
	(14, 'test-postman-234', 'test postman', 3, 'reports/September2017/4hRwDRIT0tcnLOs7o9yV.JPG', 'test postman description 234', '2017-09-03 04:04:01', '2017-09-03 04:09:57', 'julemand 234', 1, 3, 1, 0);
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping data for table interacoustics.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `tenant_id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'superadmin', 'Superadmin', '2017-09-01 20:09:28', '2017-09-03 23:44:50'),
	(2, 1, 'user', 'Normal User', '2017-09-01 20:09:28', '2017-09-01 20:09:28'),
	(3, 1, 'administrator', 'Administrator', '2017-09-03 23:41:53', '2017-09-03 23:41:53');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table interacoustics.settings: ~9 rows (approximately)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
REPLACE INTO `settings` (`id`, `tenant_id`, `key`, `display_name`, `value`, `details`, `type`, `order`) VALUES
	(1, 1, 'title', 'Site Title', 'Site Title', '', 'text', 1),
	(2, 1, 'description', 'Site Description', 'Site Description', '', 'text', 2),
	(3, 1, 'logo', 'Site Logo', '', '', 'image', 3),
	(4, 1, 'admin_bg_image', 'Admin Background Image', '', '', 'image', 9),
	(5, 1, 'admin_title', 'Admin Title', 'Voyager', '', 'text', 4),
	(6, 1, 'admin_description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 5),
	(7, 1, 'admin_loader', 'Admin Loader', '', '', 'image', 6),
	(8, 1, 'admin_icon_image', 'Admin Icon Image', '', '', 'image', 7),
	(9, 1, 'google_analytics_client_id', 'Google Analytics Client ID', '', '', 'text', 9);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping data for table interacoustics.tenants: ~2 rows (approximately)
/*!40000 ALTER TABLE `tenants` DISABLE KEYS */;
REPLACE INTO `tenants` (`id`, `name`) VALUES
	(1, 'Makers'),
	(2, 'Test');
/*!40000 ALTER TABLE `tenants` ENABLE KEYS */;

-- Dumping data for table interacoustics.translations: ~14 rows (approximately)
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
REPLACE INTO `translations` (`id`, `tenant_id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
	(1, 1, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(2, 1, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(3, 1, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(4, 1, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(5, 1, 'menu_items', 'title', 3, 'pt', 'Media', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(6, 1, 'menu_items', 'title', 4, 'pt', 'Publicações', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(7, 1, 'menu_items', 'title', 5, 'pt', 'Utilizadores', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(8, 1, 'menu_items', 'title', 6, 'pt', 'Categorias', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(9, 1, 'menu_items', 'title', 7, 'pt', 'Páginas', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(10, 1, 'menu_items', 'title', 8, 'pt', 'Funções', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(11, 1, 'menu_items', 'title', 9, 'pt', 'Ferramentas', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(12, 1, 'menu_items', 'title', 10, 'pt', 'Menus', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(13, 1, 'menu_items', 'title', 11, 'pt', 'Base de dados', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(14, 1, 'menu_items', 'title', 12, 'pt', 'Configurações', '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping data for table interacoustics.users: ~8 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `tenant_id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'superadmin', 'admin@admin.com', 'users/default.png', '$2y$10$/7HatCaPmPec2ydAl/r9C.mq6ivZLQujV7eZx/036TREER0Cdd8oG', 'yj1LjXjpAG69AM4hShBaLRfLcvQsHN7tTA6Y1VCgZ2gWrPY5FZU1jgk83JR6', '2017-09-01 20:09:56', '2017-09-03 02:39:09'),
	(2, 1, 3, 'Dennis', 'dennis@makers.dk', 'users/default.png', '$2y$10$YIGpkqCEK745VICSOysLNuS3dRm4ggFyoFKXKqjtFh/uhWmjoCJt2', NULL, '2017-09-03 02:20:11', '2017-09-03 23:44:18'),
	(3, 1, 2, 'Thomas', 'thomas@makers.dk', 'users/default.png', '$2y$10$0WTPONmgZrOqWFIJMh/Mw.PKMe.DS87z9NYcnlA06WR/Ss6UQXOva', 'RaYNfgEimSgR2S0rQkMAGqp9jeTbDQdJqDi93HyiDLvVGLlc8viLMK0UpRQ0', '2017-09-03 02:21:51', '2017-09-03 23:44:33'),
	(4, 1, 2, 'Jacob', 'jacob@makers.dk', 'users/default.png', '$2y$10$FFbeu3knZ1aKxYdJ2QWpeuIEtsd5lB9YyT/OQ.LmVfMARlSjWPNRy', NULL, '2017-09-03 02:26:58', '2017-09-03 02:26:58'),
	(5, 1, 2, 'Peter', 'peter@makers.dk', 'users/default.png', '$2y$10$/dTZSXjLt0PjlLMUslJaceQ/eDB0QUw1NdNZ8h3h3eWcRGnHgtid6', NULL, '2017-09-03 02:28:20', '2017-09-03 02:28:20'),
	(6, 2, 2, 'Malte', 'malte@makers.dk', 'users/default.png', '$2y$10$tdKsSuw7sBfILQsC7Qzt8O9LmtKFGoApcSawX/tJCYgMNldHAbHlC', NULL, '2017-09-03 02:31:47', '2017-09-03 02:31:47'),
	(7, 2, 2, 'Michael', 'Michael@makers.dk', 'users/default.png', '$2y$10$b1AlKg4Cw/grt0yzZ9mJ7OqkAtxDaq8/cq27oaUEwJAMEVQ5niFdK', NULL, '2017-09-03 02:32:26', '2017-09-03 02:32:26'),
	(8, 2, 2, 'Per', 'Per@makers.dk', 'users/default.png', '$2y$10$mUnE7w0IZnaQuUXMoQvnHughJ5AFgsgp/x6Za4qvLVFhFHpV3jYIy', NULL, '2017-09-03 02:34:25', '2017-09-03 02:34:25');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
