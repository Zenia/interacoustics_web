-- --------------------------------------------------------
-- Vært:                         umvuni.dk
-- Server-version:               10.1.14-MariaDB-1~xenial - mariadb.org binary distribution
-- ServerOS:                     debian-linux-gnu
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for tabel interacoustics.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tenant_id` int(10) unsigned NOT NULL DEFAULT '0',
  `user` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gradient` tinyint(4) DEFAULT NULL,
  `bottom_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_brands` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.brands: ~3 rows (approximately)
DELETE FROM `brands`;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` (`id`, `name`, `created_at`, `updated_at`, `tenant_id`, `user`, `logo`, `color`, `gradient`, `bottom_color`, `sub_brands`) VALUES
	(5, 'Diatec', '2017-09-22 08:33:33', '2017-09-22 14:36:59', 1, NULL, '1-interacoustics/brands/September2017/nLrUwbsR2Go5ugNXBngh.png', '#003d58', 0, NULL, NULL),
	(6, 'E3', '2017-09-22 08:58:35', '2017-09-22 14:36:29', 1, NULL, '1-interacoustics/brands/September2017/lDVLzMoTOHybjIdsR2QS.png', '#367ab9', 1, '#2c6294', NULL),
	(11, 'Interacoustics', '2017-09-22 14:35:31', '2017-09-22 14:35:31', 1, NULL, '1-interacoustics/brands/September2017/bxHH8XiHp3vDZuS8zVpe.png', '#00071f', 1, '#00b4b3', NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.brand_sub_brand
CREATE TABLE IF NOT EXISTS `brand_sub_brand` (
  `brand_id` int(10) unsigned NOT NULL,
  `sub_brand_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table interacoustics.brand_sub_brand: ~17 rows (approximately)
DELETE FROM `brand_sub_brand`;
/*!40000 ALTER TABLE `brand_sub_brand` DISABLE KEYS */;
INSERT INTO `brand_sub_brand` (`brand_id`, `sub_brand_id`) VALUES
	(5, 12),
	(6, 12),
	(5, 9),
	(6, 9),
	(1, 1),
	(5, 10),
	(5, 11),
	(6, 10),
	(6, 11),
	(5, 14),
	(6, 14),
	(11, 14),
	(11, 10),
	(11, 12),
	(6, 15),
	(6, 16),
	(6, 17);
/*!40000 ALTER TABLE `brand_sub_brand` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.brand_user
CREATE TABLE IF NOT EXISTS `brand_user` (
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table interacoustics.brand_user: ~1 rows (approximately)
DELETE FROM `brand_user`;
/*!40000 ALTER TABLE `brand_user` DISABLE KEYS */;
INSERT INTO `brand_user` (`user_id`, `brand_id`) VALUES
	(1, 5);
/*!40000 ALTER TABLE `brand_user` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.categories: ~3 rows (approximately)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `tenant_id`, `parent_id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, 'Skade', 'news', '2017-09-01 20:09:56', '2017-09-11 14:55:52'),
	(2, 1, NULL, 'Fejl', 'offers', '2017-09-01 20:09:56', '2017-09-11 14:56:01'),
	(3, 1, NULL, 'Defekt', 'defekt', '2017-09-11 14:57:05', '2017-09-11 14:57:05');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.data_rows
CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_rows_id_pk` (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.data_rows: ~111 rows (approximately)
DELETE FROM `data_rows`;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `tenant_id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
	(1, 1, 7, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(2, 1, 6, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(3, 1, 5, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 3),
	(4, 1, 4, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 2),
	(5, 1, 3, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(6, 1, 1, 'tenant_id', 'select_dropdown', 'tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(7, 1, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(8, 1, 2, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, NULL, 2),
	(9, 1, 1, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(10, 1, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(11, 1, 1, 'category_id', 'select_dropdown', 'Post Category', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(12, 1, 1, 'price', 'number', 'Price', 0, 1, 1, 1, 1, 1, '', 1),
	(13, 1, 1, 'tags', 'text', 'tags', 0, 1, 1, 1, 1, 1, '', 1),
	(14, 1, 1, 'title', 'text', 'Titel', 1, 1, 1, 1, 1, 1, '', 1),
	(15, 1, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 1),
	(16, 1, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 1),
	(17, 1, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '\r\n{\r\n    "resize": {\r\n        "width": "1000",\r\n        "height": "null"\r\n    },\r\n    "quality": "70%",\r\n    "upsize": true,\r\n    "thumbnails": [\r\n        {\r\n            "name": "medium",\r\n            "scale": "50%"\r\n        },\r\n        {\r\n            "name": "small",\r\n            "scale": "25%"\r\n        },\r\n        {\r\n            "name": "cropped",\r\n            "crop": {\r\n                "width": "300",\r\n                "height": "250"\r\n            }\r\n        }\r\n    ]\r\n}', 1),
	(18, 1, 1, 'slug', 'text', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name","forceUpdate":true}}', 1),
	(19, 1, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 1),
	(20, 1, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 1),
	(21, 1, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '\r\n{\r\n    "default": "DRAFT",\r\n    "options": {\r\n        "PUBLISHED": "published",\r\n        "DRAFT": "draft",\r\n        "PENDING": "pending"\r\n    }\r\n}', 1),
	(22, 1, 1, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 1),
	(23, 1, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 1),
	(24, 1, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(25, 1, 3, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '{"relationship":{"key":"id","label":"name"}}', 1),
	(26, 1, 3, 'title', 'text', 'Titel', 1, 1, 1, 1, 1, 1, '', 1),
	(27, 1, 3, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 1),
	(28, 1, 3, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 1),
	(29, 1, 3, 'slug', 'text', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name","forceUpdate":true}}', 1),
	(30, 1, 3, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 1),
	(31, 1, 3, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 1),
	(32, 1, 3, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 1),
	(33, 1, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 1),
	(34, 1, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 1),
	(35, 1, 3, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '', 1),
	(36, 1, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(37, 1, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '{"required":"required"}', 3),
	(38, 1, 4, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, '{"required":"required"}', 4),
	(39, 1, 4, 'password', 'password', 'password', 1, 0, 0, 1, 1, 0, '{"required":"required"}', 5),
	(40, 1, 4, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, NULL, 7),
	(41, 1, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 8),
	(42, 1, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 9),
	(43, 1, 4, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 10),
	(44, 1, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(45, 1, 6, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 1),
	(46, 1, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 1),
	(47, 1, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 1),
	(48, 1, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(49, 1, 5, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 0, 0, 0, 0, '{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}', 2),
	(51, 1, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, NULL, 4),
	(52, 1, 5, 'slug', 'text', 'slug', 1, 0, 0, 0, 1, 0, '{"slugify":{"origin":"name","forceUpdate":true}}', 5),
	(53, 1, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
	(54, 1, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
	(57, 1, 7, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
	(58, 1, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 1),
	(59, 1, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 1),
	(60, 1, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 1),
	(61, 1, 7, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 1),
	(62, 1, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 1),
	(63, 1, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 1),
	(64, 1, 4, 'role_id', 'select_dropdown', 'Role', 0, 1, 1, 1, 1, 1, '{}', 11),
	(65, 1, 8, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(67, 1, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 0, '{"validation":{"rule":"required"},"required":"required"}', 2),
	(68, 1, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
	(69, 1, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
	(70, 1, 8, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 0, '{"relationship":{"key":"id","label":"name"}}', 5),
	(71, 1, 9, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 2),
	(72, 1, 9, 'slug', 'text', 'slug', 1, 0, 0, 0, 0, 0, '{"slugify":{"origin":"name","forceUpdate":true}}', 3),
	(73, 1, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
	(74, 1, 9, 'brand_id', 'select_dropdown', 'Brand', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 1),
	(75, 1, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 5),
	(76, 1, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
	(77, 1, 9, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 7),
	(88, 1, 11, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 15),
	(91, 1, 11, 'user_id', 'select_dropdown', 'User', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 4),
	(92, 1, 11, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, NULL, 16),
	(93, 1, 11, 'description', 'text_area', 'Error description', 1, 0, 1, 1, 1, 1, '{"validation":{"rule":"required"},"required":"required"}', 5),
	(94, 1, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 17),
	(95, 1, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 18),
	(96, 1, 11, 'barcode', 'text', 'Serial number of the affected device', 0, 1, 1, 1, 1, 1, NULL, 9),
	(97, 1, 11, 'brand_id', 'select_dropdown', 'Brand', 0, 0, 0, 0, 0, 0, '{"relationship":{"key":"id","label":"name"}}', 14),
	(99, 1, 11, 'tenant_id', 'select_dropdown', 'Tenant', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 19),
	(102, 1, 4, 'accepted', 'checkbox', 'Accepted', 1, 1, 1, 1, 1, 1, NULL, 12),
	(103, 1, 11, 'ref_nr', 'text', 'Ref Nr', 0, 0, 0, 0, 0, 0, NULL, 20),
	(106, 1, 13, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
	(108, 1, 13, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{"validation":{"rule":"required"},"required":"required"}', 2),
	(110, 1, 13, 'tenant_id', 'select_dropdown', 'Tenant', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 3),
	(112, 1, 11, 'model', 'text', 'Model or process', 1, 1, 1, 1, 1, 1, '{"validation":{"rule":"required"},"required":"required"}', 8),
	(113, 1, 11, 'sub_brand_id', 'select_dropdown', 'Brand', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"},"validation":{"rule":"required"}}', 7),
	(114, 1, 8, 'user', 'select_multiple', 'User', 0, 1, 1, 0, 0, 0, '{"relationship":{"key":"id","label":"name"}}', 6),
	(115, 1, 4, 'brand_id', 'select_dropdown', 'Brand', 1, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 6),
	(116, 1, 11, 'persons_injured', 'checkbox', 'Persons Injured', 0, 0, 1, 1, 1, 1, '{"on":"Yes","off":"No","checked":"false"}', 1),
	(117, 1, 11, 'product_return', 'checkbox', 'Product will be returned', 0, 0, 1, 1, 1, 1, '{"on":"Yes","off":"No","checked":"false"}', 2),
	(118, 1, 11, 'date', 'date', 'Date of occurence', 1, 1, 1, 1, 1, 1, '{"validation":{"rule":"required"},"required":"required"}', 3),
	(119, 1, 11, 'steps', 'text_area', 'Steps to follow that make the error occur', 0, 0, 1, 1, 1, 1, NULL, 6),
	(120, 1, 11, 'accesories', 'text', 'Serial no. of failing accessories ( if relevant )', 0, 0, 1, 1, 1, 1, NULL, 10),
	(121, 1, 11, 'firmware', 'text', 'Firmware version number', 0, 0, 1, 1, 1, 1, NULL, 11),
	(122, 1, 11, 'database', 'text', 'Database used', 0, 0, 1, 1, 1, 1, NULL, 12),
	(123, 1, 11, 'dateofpurchase', 'date', 'Date of purchase', 0, 0, 1, 1, 1, 1, NULL, 13),
	(124, 1, 8, 'logo', 'image', 'Logo', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}],"validation":{"rule":"image"}}', 7),
	(125, 1, 8, 'color', 'text', 'Color', 0, 1, 1, 1, 1, 1, NULL, 8),
	(127, 1, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
	(128, 1, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
	(129, 1, 8, 'gradient', 'checkbox', 'Gradient', 0, 1, 1, 1, 1, 1, NULL, 9),
	(130, 1, 8, 'bottom_color', 'text', 'Bottom Color', 0, 1, 1, 1, 1, 1, NULL, 10),
	(135, 1, 8, 'sub_brands', 'select_multiple', 'Sub Brands', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 11),
	(136, 1, 13, 'brands', 'select_multiple', 'Brands', 0, 1, 1, 1, 1, 1, '{"relationship":{"key":"id","label":"name"}}', 6),
	(137, 1, 4, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, NULL, 13),
	(138, 1, 4, 'Company', 'text', 'Company', 0, 1, 1, 1, 1, 1, NULL, 14),
	(139, 1, 4, 'address', 'text_area', 'Address', 0, 1, 1, 1, 1, 1, NULL, 15),
	(140, 1, 4, 'Country', 'text', 'Country', 0, 1, 1, 1, 1, 1, NULL, 16),
	(141, 1, 11, 'costumer_name', 'text', 'Costumer Name', 0, 0, 1, 1, 1, 1, NULL, 21),
	(142, 1, 11, 'costumer_email', 'text', 'Costumer Email', 0, 0, 1, 1, 1, 1, NULL, 22),
	(143, 1, 11, 'costumer_address', 'text_area', 'Costumer Address', 0, 0, 1, 1, 1, 1, NULL, 23);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.data_types
CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.data_types: ~11 rows (approximately)
DELETE FROM `data_types`;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `tenant_id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
	(1, 1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'App\\Post', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(2, 1, 'tenants', 'tenants', 'Tenant', 'Tenants', 'voyager-world', 'App\\Tenant', NULL, NULL, 1, 0, '2017-09-01 20:09:27', '2017-09-13 14:02:19'),
	(3, 1, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'App\\Page', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(4, 1, 'users', 'users', 'User', 'Users', 'voyager-person', 'App\\User', NULL, NULL, 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:36:58'),
	(5, 1, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'App\\Category', NULL, NULL, 1, 0, '2017-09-01 20:09:27', '2017-09-13 15:54:24'),
	(6, 1, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'App\\Menu', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(7, 1, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'App\\Role', '', '', 1, 0, '2017-09-01 20:09:27', '2017-09-01 20:09:27'),
	(8, 1, 'brands', 'brands', 'Brand', 'Brands', NULL, 'App\\Brand', NULL, NULL, 1, 0, '2017-09-01 20:32:27', '2017-09-01 20:32:27'),
	(9, 1, 'brandmodels', 'models', 'Model', 'Models', NULL, 'App\\BrandModel', NULL, NULL, 1, 0, '2017-09-01 20:34:05', '2017-09-01 20:40:48'),
	(11, 1, 'reports', 'reports', 'Report', 'Reports', NULL, 'App\\Report', NULL, NULL, 1, 0, '2017-09-06 12:48:02', '2017-09-06 12:48:02'),
	(13, 1, 'sub_brands', 'sub-brands', 'Brand', 'Brands', NULL, 'App\\SubBrand', NULL, NULL, 1, 0, '2017-09-18 12:24:30', '2017-09-18 12:24:30');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.menus: ~0 rows (approximately)
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `tenant_id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'admin', '2017-09-01 20:09:28', '2017-09-01 20:09:28');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.menu_items: ~12 rows (approximately)
DELETE FROM `menu_items`;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `tenant_id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
	(1, 1, 1, 'Dashboard', '/admin', '_self', 'voyager-boat', NULL, NULL, 1, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL, NULL),
	(2, 1, 1, 'Cities', '/admin/tenants', '_self', 'voyager-world', NULL, NULL, 2, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL, NULL),
	(3, 1, 1, 'Media', '/admin/media', '_self', 'voyager-images', NULL, NULL, 7, '2017-09-01 20:09:28', '2017-09-11 14:48:47', NULL, NULL),
	(5, 1, 1, 'Users', '/admin/users', '_self', 'voyager-person', NULL, NULL, 8, '2017-09-01 20:09:28', '2017-09-11 14:48:47', NULL, NULL),
	(8, 1, 1, 'Roles', '/admin/roles', '_self', 'voyager-lock', NULL, NULL, 9, '2017-09-01 20:09:28', '2017-09-11 14:48:47', NULL, NULL),
	(9, 1, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 11, '2017-09-01 20:09:28', '2017-09-11 14:48:47', NULL, NULL),
	(10, 1, 1, 'Menu Builder', '/admin/menus', '_self', 'voyager-list', NULL, 9, 1, '2017-09-01 20:09:28', '2017-09-01 20:39:29', NULL, NULL),
	(11, 1, 1, 'Database', '/admin/database', '_self', 'voyager-data', NULL, 9, 2, '2017-09-01 20:09:28', '2017-09-01 20:39:30', NULL, NULL),
	(12, 1, 1, 'Settings', '/admin/settings', '_self', 'voyager-settings', NULL, NULL, 10, '2017-09-01 20:09:28', '2017-09-11 14:48:47', NULL, NULL),
	(13, 1, 1, 'Reports', 'admin/reports', '_self', 'voyager-file-text', '#000000', NULL, 3, '2017-09-01 20:38:49', '2017-09-01 22:37:15', NULL, ''),
	(14, 1, 1, 'Brands', 'admin/brands', '_self', 'voyager-tag', '#000000', NULL, 5, '2017-09-01 20:40:06', '2017-09-11 14:48:47', NULL, ''),
	(15, 1, 1, 'Sub-brands', 'admin/sub-brands', '_self', 'voyager-puzzle', '#000000', NULL, 6, '2017-09-01 20:40:26', '2017-09-18 12:28:09', NULL, '');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.migrations: ~26 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2016_01_01_000000_add_voyager_user_fields', 1),
	(4, '2016_01_01_000000_create_data_types_table', 1),
	(5, '2016_01_01_000000_create_pages_table', 1),
	(6, '2016_01_01_000000_create_posts_table', 1),
	(7, '2016_02_15_204651_create_categories_table', 1),
	(8, '2016_05_19_173453_create_menu_table', 1),
	(9, '2016_10_21_190000_create_roles_table', 1),
	(10, '2016_10_21_190000_create_settings_table', 1),
	(11, '2016_11_30_135954_create_permission_table', 1),
	(12, '2016_11_30_141208_create_permission_role_table', 1),
	(13, '2016_12_26_201236_data_types__add__server_side', 1),
	(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
	(15, '2017_01_14_005015_create_translations_table', 1),
	(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
	(17, '2017_01_15_000000_create_permission_groups_table', 1),
	(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
	(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
	(20, '2017_04_04_111150_add_tenant_id_field_to_all', 1),
	(21, '2017_04_12_104217_create_tenant_table', 1),
	(22, '2017_04_21_000000_add_order_to_data_rows_table', 1),
	(23, '2017_09_03_113347_create_brand_models_table', 0),
	(24, '2017_09_03_113347_create_brands_table', 0),
	(25, '2017_09_03_113347_create_reports_table', 0),
	(26, '2017_09_18_124519_create_brand_sub_brand_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.pages: ~0 rows (approximately)
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `tenant_id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
	(1, 1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/AAgCCnqHfLlRub9syUdw.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.password_resets: ~6 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`, `tenant_id`) VALUES
	('admin@admin.com', '$2y$10$MjjXAHF5xEN70CSLG3m3ouyIDsFYA.KZ0ygBztAiAI5YYNxH/zsre', '2017-09-02 22:50:00', 1),
	('Per@makers.dk', '$2y$10$AWKiDm6S/HvZ0wqCPRSct.WHWKhTPeDI.W5/buad71Kx6OFp7yj9m', '2017-09-07 16:45:15', 1),
	('malte@makers.dk', '$2y$10$QDHJGxu2ze0aEQkeT5m46epFFjt8kI7HanKEZA5uXIu.aWY.ghJVa', '2017-09-07 16:47:58', 1),
	('jacob@makers.dk', '$2y$10$Xi87LRvXJpg910SxSXp4peHHRzLzPUfVOqyCGcvd9VPQ4Iz4uQGuq', '2017-09-10 21:41:10', 1),
	('nilu@interacoustics.com', '$2y$10$dv/UHa8e1EBABItZZYPqPel1Qm0u4Xg4ZQizRSiy8g4KUr5zH/2xu', '2017-09-22 14:52:34', 1),
	('test@interacoustics.com', '$2y$10$0ykBlRNcg2uY0xx69UAEBOfVdMIQGNMTGHN.pMnzORG.KT1JEiq0K', '2017-09-26 16:06:54', 1);
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.permissions: ~59 rows (approximately)
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `tenant_id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
	(1, 1, 'browse_admin', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(2, 1, 'browse_database', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(3, 1, 'browse_media', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(4, 1, 'browse_settings', NULL, '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(5, 1, 'browse_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(6, 1, 'read_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(7, 1, 'edit_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(8, 1, 'add_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(9, 1, 'delete_menus', 'menus', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(10, 1, 'browse_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(11, 1, 'read_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(12, 1, 'edit_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(13, 1, 'add_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(14, 1, 'delete_pages', 'pages', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(15, 1, 'browse_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(16, 1, 'read_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(17, 1, 'edit_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(18, 1, 'add_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(19, 1, 'delete_roles', 'roles', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(20, 1, 'browse_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(21, 1, 'read_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(22, 1, 'edit_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(23, 1, 'add_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(24, 1, 'delete_users', 'users', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(25, 1, 'browse_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(26, 1, 'read_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(27, 1, 'edit_posts', 'posts', '2017-09-01 20:09:28', '2017-09-01 20:09:28', NULL),
	(28, 1, 'add_posts', 'posts', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(29, 1, 'delete_posts', 'posts', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(30, 1, 'browse_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(31, 1, 'read_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(32, 1, 'edit_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(33, 1, 'add_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(34, 1, 'delete_categories', 'categories', '2017-09-01 20:09:29', '2017-09-01 20:09:29', NULL),
	(35, 1, 'browse_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(36, 1, 'read_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(37, 1, 'edit_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(38, 1, 'add_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(39, 1, 'delete_brands', 'brands', '2017-09-01 20:32:27', '2017-09-01 20:32:27', NULL),
	(40, 1, 'browse_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(41, 1, 'read_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(42, 1, 'edit_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(43, 1, 'add_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(44, 1, 'delete_brandmodels', 'brandmodels', '2017-09-01 20:34:05', '2017-09-01 20:34:05', NULL),
	(50, 1, 'browse_reports', 'reports', '2017-09-06 12:48:02', '2017-09-06 12:48:02', NULL),
	(51, 1, 'read_reports', 'reports', '2017-09-06 12:48:02', '2017-09-06 12:48:02', NULL),
	(52, 1, 'edit_reports', 'reports', '2017-09-06 12:48:02', '2017-09-06 12:48:02', NULL),
	(53, 1, 'add_reports', 'reports', '2017-09-06 12:48:02', '2017-09-06 12:48:02', NULL),
	(54, 1, 'delete_reports', 'reports', '2017-09-06 12:48:02', '2017-09-06 12:48:02', NULL),
	(55, 1, 'browse_tenants', 'tenants', '2017-09-13 14:02:19', '2017-09-13 14:02:19', NULL),
	(56, 1, 'read_tenants', 'tenants', '2017-09-13 14:02:19', '2017-09-13 14:02:19', NULL),
	(57, 1, 'edit_tenants', 'tenants', '2017-09-13 14:02:19', '2017-09-13 14:02:19', NULL),
	(58, 1, 'add_tenants', 'tenants', '2017-09-13 14:02:19', '2017-09-13 14:02:19', NULL),
	(59, 1, 'delete_tenants', 'tenants', '2017-09-13 14:02:19', '2017-09-13 14:02:19', NULL),
	(60, 1, 'browse_sub_brands', 'sub_brands', '2017-09-18 12:24:30', '2017-09-18 12:24:30', NULL),
	(61, 1, 'read_sub_brands', 'sub_brands', '2017-09-18 12:24:30', '2017-09-18 12:24:30', NULL),
	(62, 1, 'edit_sub_brands', 'sub_brands', '2017-09-18 12:24:30', '2017-09-18 12:24:30', NULL),
	(63, 1, 'add_sub_brands', 'sub_brands', '2017-09-18 12:24:30', '2017-09-18 12:24:30', NULL),
	(64, 1, 'delete_sub_brands', 'sub_brands', '2017-09-18 12:24:30', '2017-09-18 12:24:30', NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.permission_groups
CREATE TABLE IF NOT EXISTS `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.permission_groups: ~0 rows (approximately)
DELETE FROM `permission_groups`;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.permission_role
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.permission_role: ~97 rows (approximately)
DELETE FROM `permission_role`;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(1, 3),
	(2, 1),
	(3, 1),
	(3, 3),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(20, 3),
	(21, 1),
	(21, 2),
	(21, 3),
	(22, 1),
	(22, 2),
	(22, 3),
	(23, 1),
	(23, 3),
	(24, 1),
	(24, 3),
	(25, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(30, 1),
	(30, 3),
	(31, 1),
	(31, 3),
	(32, 1),
	(32, 3),
	(33, 1),
	(33, 3),
	(34, 1),
	(34, 3),
	(35, 1),
	(35, 3),
	(36, 1),
	(36, 3),
	(37, 1),
	(37, 3),
	(38, 1),
	(38, 3),
	(39, 1),
	(39, 3),
	(40, 1),
	(40, 3),
	(41, 1),
	(41, 3),
	(42, 1),
	(42, 3),
	(43, 1),
	(43, 3),
	(44, 1),
	(44, 3),
	(50, 1),
	(50, 2),
	(50, 3),
	(51, 1),
	(51, 2),
	(51, 3),
	(52, 1),
	(52, 2),
	(52, 3),
	(53, 1),
	(53, 2),
	(53, 3),
	(54, 1),
	(54, 3),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(60, 3),
	(61, 1),
	(61, 3),
	(62, 1),
	(62, 3),
	(63, 1),
	(63, 3),
	(64, 1),
	(64, 3);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.posts: ~7 rows (approximately)
DELETE FROM `posts`;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `tenant_id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 1, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/nlje9NZQ7bTMYOUG4lF1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(2, 2, 1, 2, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/7uelXHi85YOfZKsoS6Tq.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(3, 2, 1, 1, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/9txUSY6wb7LTBSbDPrD9.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(4, 2, 1, 1, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(5, 1, 1, 1, 'Yarr Post Host', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-host', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(6, 1, 1, 2, 'Yarr Post 2', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-2', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:56', '2017-09-01 20:09:56'),
	(7, 1, 1, 2, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/yuk1fBwmKKZdY2qR1ZKM.jpg', 'yarr-post-3', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.reports
CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_id` int(10) unsigned DEFAULT '1',
  `ref_nr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_brand_id` int(11) NOT NULL,
  `persons_injured` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `product_return` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'no',
  `date` date NOT NULL,
  `steps` text COLLATE utf8mb4_unicode_ci,
  `accesories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firmware` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `database` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateofpurchase` datetime DEFAULT NULL,
  `costumer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costumer_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `costumer_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.reports: ~0 rows (approximately)
DELETE FROM `reports`;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.roles: ~3 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `tenant_id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
	(1, 1, 'superadmin', 'Superadmin', '2017-09-01 20:09:28', '2017-09-03 23:44:50'),
	(2, 1, 'user', 'Normal User', '2017-09-01 20:09:28', '2017-09-01 20:09:28'),
	(3, 1, 'administrator', 'Administrator', '2017-09-03 23:41:53', '2017-09-03 23:41:53');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.settings: ~10 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `tenant_id`, `key`, `display_name`, `value`, `details`, `type`, `order`) VALUES
	(1, 1, 'title', 'Site Title', 'Interacoustics', '', 'text', 1),
	(2, 1, 'description', 'Site Description', 'Incident Reports', '', 'text', 2),
	(3, 1, 'logo', 'Site Logo', '1-interacoustics/settings/September2017/zOXHSOd8fSeleAxhXvm4.png', '', 'image', 3),
	(4, 1, 'admin_bg_image', 'Admin Background Image', '', '', 'image', 9),
	(5, 1, 'admin_title', 'Admin Title', 'Interacoustics', '', 'text', 4),
	(6, 1, 'admin_description', 'Admin Description', 'Incident Reports', '', 'text', 5),
	(7, 1, 'admin_loader', 'Admin Loader', '1-interacoustics/settings/September2017/JDPe59yzteHTWpSejXCI.png', '', 'image', 6),
	(8, 1, 'admin_icon_image', 'Admin Icon Image', '1-interacoustics/settings/September2017/y8fDPmHgOqYa5NHzTzLt.png', '', 'image', 7),
	(9, 1, 'google_analytics_client_id', 'Google Analytics Client ID', '', '', 'text', 9),
	(10, 1, 'special_logo', 'Special Logo', '1-interacoustics/settings/September2017/g18JsTFk5zbJnwuhZbU2.png', NULL, 'image', 10);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.sub_brands
CREATE TABLE IF NOT EXISTS `sub_brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tenant_id` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table interacoustics.sub_brands: ~8 rows (approximately)
DELETE FROM `sub_brands`;
/*!40000 ALTER TABLE `sub_brands` DISABLE KEYS */;
INSERT INTO `sub_brands` (`id`, `name`, `tenant_id`, `created_at`, `updated_at`, `brands`) VALUES
	(9, 'Maico', 1, '2017-09-22 04:49:00', '2017-09-22 09:52:44', NULL),
	(10, 'Interacoustics', 1, '2017-09-22 00:49:00', '2017-09-22 14:38:58', NULL),
	(11, 'MedRx', 1, '2017-09-22 04:49:00', '2017-09-22 09:50:39', NULL),
	(12, 'Sanibel', 1, '2017-09-21 21:35:00', '2017-09-22 14:39:25', NULL),
	(14, 'RadioEar', 1, '2017-09-22 14:38:42', '2017-09-22 14:38:42', NULL),
	(15, 'Amplivox', 1, '2017-09-22 14:39:46', '2017-09-22 14:39:46', NULL),
	(16, 'GSI', 1, '2017-09-22 14:39:58', '2017-09-22 14:39:58', NULL),
	(17, 'Micromedical', 1, '2017-09-22 14:40:18', '2017-09-22 14:40:18', NULL);
/*!40000 ALTER TABLE `sub_brands` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.tenants
CREATE TABLE IF NOT EXISTS `tenants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenants_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.tenants: ~1 rows (approximately)
DELETE FROM `tenants`;
/*!40000 ALTER TABLE `tenants` DISABLE KEYS */;
INSERT INTO `tenants` (`id`, `name`) VALUES
	(1, 'Interacoustics');
/*!40000 ALTER TABLE `tenants` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.translations
CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.translations: ~14 rows (approximately)
DELETE FROM `translations`;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `tenant_id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
	(1, 1, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(2, 1, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(3, 1, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(4, 1, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(5, 1, 'menu_items', 'title', 3, 'pt', 'Media', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(6, 1, 'menu_items', 'title', 4, 'pt', 'Publicações', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(7, 1, 'menu_items', 'title', 5, 'pt', 'Utilizadores', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(8, 1, 'menu_items', 'title', 6, 'pt', 'Categorias', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(9, 1, 'menu_items', 'title', 7, 'pt', 'Páginas', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(10, 1, 'menu_items', 'title', 8, 'pt', 'Funções', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(11, 1, 'menu_items', 'title', 9, 'pt', 'Ferramentas', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(12, 1, 'menu_items', 'title', 10, 'pt', 'Menus', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(13, 1, 'menu_items', 'title', 11, 'pt', 'Base de dados', '2017-09-01 20:09:57', '2017-09-01 20:09:57'),
	(14, 1, 'menu_items', 'title', 12, 'pt', 'Configurações', '2017-09-01 20:09:57', '2017-09-01 20:09:57');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

-- Dumping structure for tabel interacoustics.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '1',
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `accepted` tinyint(4) NOT NULL DEFAULT '0',
  `brand_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table interacoustics.users: ~5 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `tenant_id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `accepted`, `brand_id`, `phone`, `company`, `address`, `country`) VALUES
	(1, 1, 1, 'superadmin', 'admin@admin.com', '1-interacoustics/users/default.png', '$2y$10$AFKFDF4k3EGEdvKpMP28T.c0apLJWe9ix6Q65EBt1mowtIJGsfxXy', 'vZwDVWWA212AUaQ3Vf0wx3A9IVcHYughgZ1b65U0N4jUoYnFKRQfZC1vlx26', '2017-09-01 20:09:56', '2017-09-22 14:53:25', 1, 5, NULL, NULL, NULL, NULL),
	(2, 1, 3, 'Dennis', 'dennis@makers.dk', '1-interacoustics/users/October2017/gxtYsOejMkYxATmGJmM8.jpg', '$2y$10$U8c8V5fap6xfi0n4KseKwedANOkyREfIEMaRbRqSwYfWs4wc0S3La', '4bDI3cUFnXOyRZ7AuFGjV3YSFSiqrbj0l0lhmurWEp0Y7etzPkQkDwtbuW6z', '2017-09-03 02:20:11', '2017-11-27 09:38:26', 1, 6, NULL, NULL, NULL, NULL),
	(9, 1, 3, 'Niclas Lund', 'nilu@interacoustics.com', '1-interacoustics/users/default.png', '$2y$10$C4RgOY.a2QOIF5SialbZKu80QUUZ0m31VY9lV09BhEPHKhXrfFm0K', 'SQe9eJ0pBQpaFcUxtGYHaB2hNOHE4UNDWWWPMHFDlQBPQ28fystcYdsm0n9t', '2017-09-13 13:45:22', '2017-09-25 13:42:31', 1, 11, NULL, NULL, NULL, NULL),
	(22, 1, 2, 'Niclas Lund', 'web@interacoustics.com', 'users/default.png', '$2y$10$7cn48/hLJpC.yPbNoF9lReBWZ8cOVOfFew4TnF5QDnVuewzrVrP1q', NULL, '2017-10-16 14:03:07', '2017-10-16 14:03:08', 0, NULL, NULL, NULL, NULL, NULL),
	(28, 1, 2, 'Mathu Mou', 'mathu@makers.dk', 'users/default.png', '$2y$10$9SS9Z0QgDuCVIC24VSXc0eFSBHAo/5OgW81zax9QZQt4KjbuvMC.C', NULL, '2017-10-16 15:53:51', '2017-10-16 16:11:07', 1, 5, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
