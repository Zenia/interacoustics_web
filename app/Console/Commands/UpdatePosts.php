<?php

namespace App\Console\Commands;

use App\Post;
use App\User;
use Illuminate\Console\Command;
use Log;

class UpdatePosts extends Command {
  
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:UpdatePosts';
  
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Getting logitude and latitude for all posts that don´t have allready';
  
  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }
  
  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
//    $posts = Post::all();
//    $posts = Post::where('longitude', null)
    $posts = Post::all();
//    $post = Post::where('id','>=', 3529)->where('longitude', null)->first();
    foreach ( $posts as $post )
    {
      echo $post->id.' - ';
      $coords = null;
      if ( isset($post->address))
      {

        if ( !isset($post->address_line1))
        {

            $newaddress = explode(',', $post->address);
            if ( count($newaddress) >= 0 )
            {
              $post->address_line1 = $newaddress[0];
            }
            if ( count($newaddress) > 2 )
            {
              $post->address_line2 = end($newaddress);
            }
            if ( count($newaddress) == 2 )
            {
              $post->address_line2 = $newaddress[1];
            }

        }
//        if ( $coords = getLongAndLatFromAddress($post->address) )
//        {
//          $post->longitude = (isset($coords['long'])) ? $coords['long'] : null;
//          $post->latitude = (isset($coords['lat'])) ? $coords['lat'] : null;
//        }
      }
      elseif ( isset($post->address_line1))
      {
        if ( ! isset($post->address) || $post->address === 0 || $post->address === '0' )
        {
          $post->address = $post->address_line1 . ', ' . $post->address_line2;
        }

//        if ( $coords = getLongAndLatFromAddress($post->address_line1 . ', ' . $post->address_line2) )
//        {
//
//          $post->longitude = (isset($coords['long'])) ? $coords['long'] : null;
//          $post->latitude = (isset($coords['lat'])) ? $coords['lat'] : null;
//        }
      }
      if ( ! isset($post->tlf) && isset($post->address_phone) )
      {
        $post->tlf = $post->address_phone;
      }
      elseif ( isset($post->tlf) && ! isset($post->address_phone) )
      {
        $post->address_phone = $post->tlf;
      }
      if ( ! isset($post->excerpt) || $post->excerpt === "" )
      {
        $post->excerpt = ( isset($post->body) ) ? substr($post->body, 0, 48) : "";
      }
      if(isset($post->address_line1) && isset($post->address_line2))
      {
        if ( $coords = getLongAndLatFromAddress($post->address_line1 . ', ' . $post->address_line2) )
        {

          $post->longitude = ( isset($coords['long']) ) ? $coords['long'] : null;
          $post->latitude  = ( isset($coords['lat']) ) ? $coords['lat'] : null;
        }
      }
      $post->save();
    }
//      return $post;
    }
  
}
