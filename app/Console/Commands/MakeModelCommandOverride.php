<?php

namespace App\Console\Commands;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ModelMakeCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;


class MakeModelCommandOverride extends ModelMakeCommand
{
  /**
   * The console command name.
   *
   * @var string
   */
  protected $name = 'voyager:make:modelOverride';
  
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create a new Voyager model class';
  
  /**
   * Get the stub file for the generator.
   *
   * @return string
   */
  protected function getStub()
  {
    return __DIR__.'/../../Overrides/stubs/model.stub';
  }
  
  /**
   * Build the class with the given name.
   *
   * @param string $name
   *
   * @return string
   */
  protected function buildClass($name)
  {
    $stub = $this->files->get($this->getStub());
    
    return $this->addSoftDelete($stub)->replaceNamespace($stub, $name)->replaceClass($stub, $name);
  }
  /**
   * Execute the console command.
   *
   * @return void
   */
  public function fire()
  {
    if (parent::fire() === false) {
      return;
    }
    
    if ($this->option('migrationOverride')) {
      $table = Str::plural(Str::snake(class_basename($this->argument('name'))));
      
      $this->call('make:migrationOverride', [
          'name' => "create_{$table}_table",
          '--create' => $table,
      ]);
    }
    
    if ($this->option('controller')) {
      $controller = Str::studly(class_basename($this->argument('name')));
      
      $this->call('make:controller', [
          'name' => "{$controller}Controller",
          '--resource' => $this->option('resource'),
      ]);
    }
    $insert = "$";
    $VName = $this->argument('name');
    $funcName = class_basename($VName);
    $filesystem = new Filesystem();
    $TenantFile = $filesystem->get(base_path('app/Tenant.php'));
//    dd($TenantFile);
    $TenantFile = rtrim($TenantFile,'}');
    $TenantFile .= "\n\npublic function ".$funcName."(){\n return ".$insert."this->hasMany('$VName'); \n } \n }";
    $filesystem->put(base_path('app/Tenant.php'),$TenantFile);
  }
  
  /**
   * Add SoftDelete to the given stub.
   *
   * @param string $stub
   *
   * @return $this
   */
  protected function addSoftDelete(&$stub)
  {
    $traitIncl = $trait = '';
    
    if ($this->option('softdelete')) {
      $traitIncl = 'use Illuminate\Database\Eloquent\SoftDeletes;';
      $trait = 'use SoftDeletes;';
    }
    
    $stub = str_replace('//DummySDTraitInclude', $traitIncl, $stub);
    $stub = str_replace('//DummySDTrait', $trait, $stub);
    
    return $this;
  }
  
  /**
   * Get the console command options.
   *
   * @return array
   */
  protected function getOptions()
  {
    $options = [
        ['softdelete', 'd', InputOption::VALUE_NONE, 'Add soft-delete field to Model'],
        ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the model.'],
        ['migrationOverride', 'mo', InputOption::VALUE_NONE, 'Create a new migration file for the model.'],

        ['controller', 'c', InputOption::VALUE_NONE, 'Create a new controller for the model.'],

        ['resource', 'r', InputOption::VALUE_NONE, 'Indicates if the generated controller should be a resource controller'],
    ];
    
    return array_merge($options, parent::getOptions());
  }

}
