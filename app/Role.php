<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Role as VoyagerRole;

class Role extends VoyagerRole
{
  use belongsToTenant;
  
//  public function user(  )
//  {
//    return $this->belongsToMany('App\User','users','role_id','id');
//  }
}
