<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;


class Brand extends Model {
  
  use belongsToTenant;
  protected $hidden =[
      'created_at',
      'updated_at',
      'user',
      'tenant_id',
      'sub_brands'
  ];
  public function user(  ) {
    return $this->belongsToMany(User::class);
  }

  public function reports(  )
  {
    return $this->hasMany(Report::class);
  }
  public function subBrands(){
    return $this->belongsToMany(SubBrand::class,'brand_sub_brand');
  }
  
  public function sub_brands(  ) {
    return $this->subBrands();
  }
}
