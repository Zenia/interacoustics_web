<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialController extends Controller {
  
  protected $redirectTo = '/';
  
  
  public function redirectToProvider( $provider )
  {
    return Socialite::driver($provider)->redirect();
  }
  
  /**
   * Obtain the user information from provider.  Check if the user already exists in our
   * database by looking up their provider_id in the database.
   * If the user exists, log them in. Otherwise, create a new user then log them in. After that
   * redirect them to the authenticated users homepage.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function handleProviderCallback( $provider )
  {
    $user     = Socialite::driver($provider)->user();
    $authUser = $this->findOrCreateUser($user, $provider);
    Auth::login($authUser, true);
    
    return redirect('/admin');
  }
  
  /**
   * If a user has registered before using social auth, return the user
   * else, create a new user object.
   *
   * @param  $user     Socialite user object
   * @param  $provider Social auth provider
   *
   * @return  User
   */
  public function findOrCreateUser( $user, $provider )
  {
    if ( $authUser = User::where('provider_id', $user->id)->first() )
    {
      return $authUser;
    }
    if ( ! $authUser = User::where([ 'email'     => $user->getEmail(),
                                     'tenant_id' => 1,
    ])->first()
    )
    {
      return User::create([ 'name'           => $user->name,
                            'password'       => Hash::make(str_random(8)),
                            'tenant_id'      => 1,
                            'email'          => $user->email,
                            'avatar'         => $user->getAvatar(),
                            'provider'       => $provider,
                            'provider_id'    => $user->id,
                            'provider_token' => $user->token,
      ]);
    }
    else
    {
      $authUser->update([ 'avatar'         => $user->getAvatar(),
                          'provider'       => $provider,
                          'provider_id'    => $user->id,
                          'provider_token' => $user->token,
      ]);
      
      return $authUser;
    }
  }
  
  public function loginOrCreateWithFacebookToken( Request $request )
  {
    $input    = $request->all();
    $user     = Socialite::driver('facebook')->userFromToken($input['facebook_token']);
    $authUser = $this->findOrCreateUser($user, 'facebook');
    Auth::login($authUser, true);
    $user = Auth::user();
//    if ( $request->ajax() || $request->wantsJson() )
//    {
    return collect([ 'token' => JWTAuth::fromUser($user),
                     'user'  => $user,
    ]);
  }
}
