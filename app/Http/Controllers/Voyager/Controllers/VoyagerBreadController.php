<?php

namespace App\Http\Controllers\Voyager\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

class VoyagerBreadController extends Controller {
  
  use BreadRelationshipParser;
  
  //***************************************
  //               ____
  //              |  _ \
  //              | |_) |
  //              |  _ <
  //              | |_) |
  //              |____/
  //
  //      Browse our Data Type (B)READ
  //
  //****************************************
  
  public function index( Request $request )
  {
    // GET THE SLUG, ex. 'posts', 'pages', etc.
    $slug = $this->getSlug($request);
    
    // GET THE DataType based on the slug
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('browse_' . $dataType->name);
    
    $getter = $dataType->server_side ? 'paginate' : 'get';
    
    // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
    if ( strlen($dataType->model_name) != 0 )
    {
      $model = app($dataType->model_name);
      
      $relationships = $this->getRelationships($dataType);
      
      if ( $model->timestamps )
      {
        $dataTypeContent = call_user_func([ $model->with($relationships)->latest(), $getter ]);
      }
      else
      {
        $dataTypeContent = call_user_func([ $model->with($relationships)->orderBy($model->getKeyName(), 'DESC'),
                                            $getter,
        ]);
      }
      
      //Replace relationships' keys for labels and create READ links if a slug is provided.
      $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
    }
    else
    {
      // If Model doesn't exist, get data from table name
      $dataTypeContent = call_user_func([ DB::table($dataType->name), $getter ]);
      $model           = false;
    }
    
    // Check if BREAD is Translatable
    $isModelTranslatable = is_bread_translatable($model);
    
    $view = 'vendor.voyager.bread.browse';
    
    if ( view()->exists("vendor.voyager.".$slug.".browse") )
    {
      $view = "vendor.voyager.".$slug.".browse";
    }
    if ( isSuperAdmin() )
    {}
    elseif (Auth::user()->hasRole('administrator')){
      $dataTypeContent = $dataTypeContent->where('tenant_id', Auth::user()->tenantId->name)->where('role_id', '!=',Voyager::model('Role')->where('name', 'superadmin')->first()->id);
      if ( $dataType->name == 'roles' )
      {
        $dataTypeContent = $dataTypeContent->where('display_name', '!=', 'superadmin')->where('display_name', '!=', 'Administrator');
        
      }
    }
    else{
      $filtered = collect([]);
  for($i = 0; $i < count($dataTypeContent); $i++){
    if($dataTypeContent[$i]->userId != null && $dataTypeContent[$i]->userId->id == Auth::id()){
      $filtered->push($dataTypeContent[$i]);
    }
  }
      $dataTypeContent = $filtered;
    }
    
      return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
   
    
  }
  //***************************************
  //                _____
  //               |  __ \
  //               | |__) |
  //               |  _  /
  //               | | \ \
  //               |_|  \_\
  //
  //  Read an item of our Data Type B(R)EAD
  //
  //****************************************
  
  public function show( Request $request, $id )
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('read_' . $dataType->name);
    
    $relationships = $this->getRelationships($dataType);
    if ( strlen($dataType->model_name) != 0 )
    {
      $model           = app($dataType->model_name);
      $dataTypeContent = call_user_func([ $model->with($relationships), 'findOrFail' ], $id);
    }
    else
    {
      // If Model doest exist, get data from table name
      $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
    }
    
    //Replace relationships' keys for labels and create READ links if a slug is provided.
    $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);
    
    // Check if BREAD is Translatable
    $isModelTranslatable = is_bread_translatable($dataTypeContent);
    
    $view = 'vendor.voyager.bread.read';
    
    if ( view()->exists("vendor.voyager.".$slug.".read") )
    {
      $view = "voyager::$slug.read";
    }
    
    return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
  }
  
  //***************************************
  //                ______
  //               |  ____|
  //               | |__
  //               |  __|
  //               | |____
  //               |______|
  //
  //  Edit an item of our Data Type BR(E)AD
  //
  //****************************************
  
  public function edit( Request $request, $id )
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('edit_' . $dataType->name);
    
    $relationships = $this->getRelationships($dataType);
    
    $dataTypeContent = ( strlen($dataType->model_name) != 0 ) ? app($dataType->model_name)->with($relationships)->findOrFail($id) : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name
    
    // Check if BREAD is Translatable
    $isModelTranslatable = is_bread_translatable($dataTypeContent);
    
    $view = 'vendor.voyager.bread.edit-add';
    
    if ( view()->exists("vendor.voyager.".$slug.".edit-add") )
    {
      $view = "vendor.voyager.".$slug.".edit-add";
    }
    
    return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
  }
  
  // POST BR(E)AD
  /*
  public function update( Request $request, $id )
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    // Check permission
    Voyager::canOrFail('edit_' . $dataType->name);
    
    //Validate fields with ajax
    $val = $this->validateBread($request->all(), $dataType->editRows);
    
    if ( $val->fails() )
    {
      //return response()->json([ 'errors' => $val->messages() ]);
      
      return back()->withErrors($val->errors())->withInput($request->all());
    }
    
    if ( ! $request->ajax() )
    {
      $data = call_user_func([ $dataType->model_name, 'findOrFail' ], $id);
      $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
      if(!isSuperAdmin() && !Auth::user()->hasRole('administrator') && $slug == 'reports')
      {
        return redirect()->route("user.reports.index");
      }elseif(!isSuperAdmin() && !Auth::user()->hasRole('administrator') && $slug == 'users')
      {
        return redirect()->route("user.users.edit", [ 'id' => $id ]);
      }
  else{
      return redirect()->route("voyager.{$dataType->slug}.index", [ 'id' => $id ])->with([ 'message'    => "Successfully Updated {$dataType->display_name_singular}",
      ]);
    }
    }
  }
  */
  public function update(Request $request, $id)
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('edit_'.$dataType->name);
    
    //Validate fields with ajax
    $val = $this->updateValidateBread($request->all(), $dataType->editRows,$slug,$id);
    
    if ($val->fails()) {
      return response()->json(['errors' => $val->messages()]);
    }
    
    if (!$request->ajax()) {
      $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
      
      $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
      
      return redirect()
          ->route("voyager.{$dataType->slug}.edit", ['id' => $id])
          ->with([
              'message'    => "Successfully Updated {$dataType->display_name_singular}",
              'alert-type' => 'success',
          ]);
    }
  }
  
  public function updateValidateBread($request, $data, $slug,$id)
  {
    $rules = [];
    $messages = [];
    
    foreach ($data as $row) {
      $options = json_decode($row->details);
      
      if (isset($options->validation)) {
        if (isset($options->validation->rule)) {
          if (!is_array($options->validation->rule)) {
            if( strpos(strtoupper($options->validation->rule), 'UNIQUE') !== false )
            {
              $options->validation->rule = str_replace('unique:'.$slug, '', $options->validation->rule);
              
              $explodedRules = explode('|', $options->validation->rule);
              
              $explodedRules[] = \Illuminate\Validation\Rule::unique($slug)->ignore($id);
              
              $rules[$row->field] = $explodedRules;
              
              
            }
            else {
              $rules[$row->field] = explode('|', $options->validation->rule);
            }
          } else {
            $rules[$row->field] = $options->validation->rule;
          }
        }
        
        if (isset($options->validation->messages)) {
          foreach ($options->validation->messages as $key => $msg) {
            $messages[$row->field.'.'.$key] = $msg;
          }
        }
      }
    }
    
    return Validator::make($request, $rules, $messages);
  }
  //***************************************
  //
  //                   /\
  //                  /  \
  //                 / /\ \
  //                / ____ \
  //               /_/    \_\
  //
  //
  // Add a new item of our Data Type BRE(A)D
  //
  //****************************************
  
  public function create( Request $request )
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('add_' . $dataType->name);
    
    $dataTypeContent = ( strlen($dataType->model_name) != 0 ) ? new $dataType->model_name() : false;
    
    // Check if BREAD is Translatable
    $isModelTranslatable = is_bread_translatable($dataTypeContent);
    
    $view = 'vendor.voyager.bread.edit-add';
    
    if ( view()->exists("vendor.voyager.".$slug.".edit-add") )
    {
      $view = "vendor.voyager.".$slug.".edit-add";
    }
    
    return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
  }
  
  // POST BRE(A)D
  public function store( Request $request )
  {
if($request->has('date')){
$request->offsetSet('date', gmdate('Y-m-d H:i:s', strtotime($request->get('date'))));
}
if($request->has('dateofpurchase')){
$request->offsetSet('dateofpurchase', gmdate('Y-m-d H:i:s', strtotime($request->get('dateofpurchase'))));
}

    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('add_' . $dataType->name);
    
    //Validate fields with ajax
    $val = $this->validateBread($request->all(), $dataType->addRows);
    
    if ( $val->fails() )
    {
      if ( ! $request->ajax() ) {
 return back()->withInput()->with([ 'errors'  => $val->messages(),
                            'success' => false,
      ]);
      }else{
      return response()->json([ 'errors' => $val->messages() ]);
    }
    }
    if ( ! $request->ajax() ) {
      $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
  
      if(!isSuperAdmin() && !Auth::user()->hasRole('administrator') && $slug == 'reports')
      {
        return redirect()->route("user.reports.index", [ 'id' => $data->id ]);
      }elseif(!isSuperAdmin() && !Auth::user()->hasRole('administrator') && $slug == 'users')
      {
        return redirect()->route("user.users.read", [ 'id' => $data->id ]);
      }
      else{
        if($dataType->display_name_singular == 'Report'){
          return redirect()->route("voyager.{$dataType->slug}.index", [ 'id' => $data->id ])->with([ 'message'    => "Thank you for reporting your incident.<br>
We will use the incident for evaluation and improvement of our devices.<br>
We will only contact you in case we have any follow-up questions.<br>",
                    'alert-type' => 'success',
          ]);
  
        }
        else {
          return redirect()->route("voyager.{$dataType->slug}.index", [ 'id' => $data->id ])->with([ 'message'    => "Successfully Added New {$dataType->display_name_singular}",
                                                                                                     'alert-type' => 'success',
          ]);
        }
      }
  }
  }
  
  //***************************************
  //                _____
  //               |  __ \
  //               | |  | |
  //               | |  | |
  //               | |__| |
  //               |_____/
  //
  //         Delete an item BREA(D)
  //
  //****************************************
  
  public function destroy( Request $request, $id )
  {
    $slug = $this->getSlug($request);
    
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
    
    // Check permission
    Voyager::canOrFail('delete_' . $dataType->name);
    
    $data = call_user_func([ $dataType->model_name, 'findOrFail' ], $id);
    
    // Delete Translations, if present
    if ( is_bread_translatable($data) )
    {
      $data->deleteAttributeTranslations($data->getTranslatableAttributes());
    }
    
    foreach ( $dataType->deleteRows as $row )
    {
      if ( $row->type == 'image' )
      {
        $this->deleteFileIfExists('/uploads/' . $data->{$row->field});
        
        $options = json_decode($row->details);
        
        if ( isset($options->thumbnails) )
        {
          foreach ( $options->thumbnails as $thumbnail )
          {
            $ext       = explode('.', $data->{$row->field});
            $extension = '.' . $ext[ count($ext) - 1 ];
            
            $path = str_replace($extension, '', $data->{$row->field});
            
            $thumb_name = $thumbnail->name;
            
            $this->deleteFileIfExists('/uploads/' . $path . '-' . $thumb_name . $extension);
          }
        }
      }
    }
    
    $data = $data->destroy($id) ? [ 'message'    => "Successfully Deleted {$dataType->display_name_singular}",
                                    'alert-type' => 'success',
    ] : [ 'message'    => "Sorry it appears there was a problem deleting this {$dataType->display_name_singular}",
          'alert-type' => 'error',
    ];
    
    return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
  }
  
}
