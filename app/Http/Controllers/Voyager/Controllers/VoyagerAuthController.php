<?php

namespace App\Http\Controllers\Voyager\Controllers;

use App\Tenant;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class VoyagerAuthController extends Controller {
  
  use AuthenticatesUsers;
  
  public function login()
  {
    return view('vendor.voyager.login');
  }
  
  public function postLogin( Request $request )
  {
    if($user = User::where('email',$request->get('email'))->first()){
      if(!$user->accepted){
        $errors = [$this->username() => 'your account have not yet been approved, please contact your admin'];
      
        if ($request->expectsJson()) {
          return response()->json($errors, 422);
        }
      
        return redirect()->back()
                         ->withInput($request->only($this->username(), 'remember'))
                         ->withErrors($errors);
      }
    }
    $this->validateLogin($request);
    
    // If the class is using the ThrottlesLogins trait, we can automatically throttle
    // the login attempts for this application. We'll key this by the username and
    // the IP address of the client making these requests into this application.
    if ( $this->hasTooManyLoginAttempts($request) )
    {
      $this->fireLockoutEvent($request);
      
      return $this->sendLockoutResponse($request);
    }
    
    $credentials = $this->credentials($request);
    
    if ( $this->guard()->attempt($credentials, $request->has('remember')) )
    {
      return $this->sendLoginResponse($request);
    }
    
    // If the login attempt was unsuccessful we will increment the number of attempts
    // to login and redirect the user back to the login form. Of course, when this
    // user surpasses their maximum number of attempts they will get locked out.
    $this->incrementLoginAttempts($request);
    
    return $this->sendFailedLoginResponse($request);
  }
  
  /*
   * Preempts $redirectTo member variable (from RedirectsUsers trait)
   */
  public function redirectTo()
  {
    return route('voyager.dashboard');
  }
  public function validateLogin( Request $request )
  {
    $this->validate($request, [ $this->username() => ['required',
                                                      Rule::exists('users')->where(function ($query) {
                                                        $query->where('accepted', 1);
                                                      })],
                                'password'        => 'required',
    ]);
  }
}
