<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Traits\AlertsMessages;

class RegisterController extends Controller {
  
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */
  
  use RegistersUsers, AlertsMessages;
  
  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/home';
  
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
    $this->middleware('guest');
  }
  
  public function register( Request $request ) {
    $this->validator($request->all())->validate();
    
    $this->create($request->all());
    
    
    return redirect('/')->with([ 'message'    => "your request has been registered, please wait for admin to confirm",
                                 'alert-type' => 'success',
    ]);
  }
  
  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array $data
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator( array $data ) {
    return Validator::make($data, [
        'name'     => 'required|string|max:255',
        'email'    => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
        'company'  => 'required|string',
        'address'  => 'required|string',
        'country'  => 'required|string',
        'phone'    => 'required|string',
    ]);
  }
  
  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array $data
   *
   * @return User
   */
  protected function create( array $data ) {
    return User::create([
        'name'      => $data['name'],
        'email'     => $data['email'],
        'company'   => $data['company'],
        'address'   => $data['address'],
        'country'   => $data['country'],
        'phone'     => $data['phone'],
        'password'  => bcrypt($data['password']),
        'tenant_id' => ( Auth::check() ) ? Auth::user()->tenant_id : 1,
    ]);
  }
}
