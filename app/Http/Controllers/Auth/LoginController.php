<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Tenant;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
  
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */
  
  use AuthenticatesUsers;
  
  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  protected $redirectTo = '/admin';
  
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }
  
  public function showLoginForm()
  {
    return view('auth.login');
  }
  public function validateLogin( Request $request )
  {
    $this->validate($request, [ $this->username() => 'required',
                                'password'        => 'required'
    ]);
  }
  public function login(Request $request)
  {
    if($user = User::where('email',$request->get('email'))->first()){
      dd('dav');
      if(!$user->accepted){
        $errors = [$this->username() => 'your account have not yet been approved, please contact your admin'];
  
        if ($request->expectsJson()) {
          return response()->json($errors, 422);
        }
  
        return redirect()->back()
                         ->withInput($request->only($this->username(), 'remember'))
                         ->withErrors($errors);
      }
    }
    
    
    
    $this->validateLogin($request);
    
    
    // If the class is using the ThrottlesLogins trait, we can automatically throttle
    // the login attempts for this application. We'll key this by the username and
    // the IP address of the client making these requests into this application.
    if ($this->hasTooManyLoginAttempts($request)) {
      $this->fireLockoutEvent($request);
      
      return $this->sendLockoutResponse($request);
    }
    
    if ($this->attemptLogin($request)) {
      return $this->sendLoginResponse($request);
    }
    
    // If the login attempt was unsuccessful we will increment the number of attempts
    // to login and redirect the user back to the login form. Of course, when this
    // user surpasses their maximum number of attempts they will get locked out.
    $this->incrementLoginAttempts($request);
    
    return $this->sendFailedLoginResponse($request);
  }
  public function logout(Request $request  ) {
    $this->guard()->logout();
  
    $request->session()->invalidate();
    Auth::logout();
  
    return redirect('/login');
}


}
