<?php namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ReportController extends Controller {
  
  /**
   * Display a listing of the resource.
   * GET /report
   *
   * @return Response
   */
  public function index() {
    //
  }
  
  /**
   * Show the form for creating a new resource.
   * GET /report/create
   *
   * @return Response
   */
  public function create() {
    //
  }
  
  /**
   * Store a newly created resource in storage.
   * POST /report
   *
   * @return Response
   */
  public function store( Request $request ) {
    $validation = Validator::make($request->all(), [
        'date'        => 'required',
        'description' => 'required',
        'model'       => 'required',
    
    ]);
    if ( $validation->fails() ) {
      return back()->with([ 'errors'  => $validation->messages(),
                            'success' => false,
      ]);
    }
    try {
      
      $report        = Report::create($request->except('image')
//                                 'user_id'     => $request->get('user_id'),
//                                 'description' => $request->get('description'),
//                                 'barcode'     => $request->get('barcode'),
//                                 'category_id'    => $request->get('category_id'),
//                                 'brand_id'    => $request->get('brand_id'),
//                                 'model_id'    => $request->get('model_id'),
//                                 'tenant_id'   => $request->get('tenant_id'),
//                                 'image'       => $fullPath,
//                                 'accepted'    => 0,
      );
      $report->brand_id = Auth::user()->brand_id;
      $report->save();
      if ( $request->hasFile('image') )
      {
        if ( $request->file('image')->isValid() )
        {
          $slug     = 'reports';
          $file     = $request->file('image');
          $filename = Str::random(20);
          $path     = tenantFolder().'/'.$slug . '/' . date('F') . date('Y') . '/';
          $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();
          $request->file('image')->storeAs($path, $filename . '.' . $file->getClientOriginalExtension(), config('voyager.storage.disk', 'public'));
          $report->image = $fullPath;
          $report->save();
      
      
      
        }
      }
      
      if ( isSuperAdmin() || Auth::user()->hasRole('administrator') ) {
        return redirect()->route('voyager.reports.index')->with([ 'message'    => "Thank you for reporting your incident.
We will use the incident for evaluation and improvement of our devices.
We will only contact you in case we have any follow-up questions.",
                                                                  'alert-type' => 'success',
        ]);
      }
      else {
        return redirect()->route("user.reports.index")->with([ 'message'    => "Thank you for reporting your incident.
We will use the incident for evaluation and improvement of our devices.
We will only contact you in case we have any follow-up questions.",
                                                               'alert-type' => 'success',
        ]);
      }
    } catch ( Exception $e ) {
      return back()->with([ 'errors' => 'something went wrong' ]);
    }
  }
  
  /**
   * Display the specified resource.
   * GET /report/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show( $id ) {
    //
  }
  
  /**
   * Show the form for editing the specified resource.
   * GET /report/{id}/edit
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit( $id ) {
    //
  }
  
  /**
   * Update the specified resource in storage.
   * PUT /report/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update( Request $request, $report_id ) {
    
    try {
      $r      = new Report();
      $fields = array_except($r->getFillable(), [ 'image' ]);
      Report::whereId($report_id)->update(
          $request->only($fields)
//                                            'user_id'     => $request->get('user_id'),
//                                            'description' => $request->get('description'),
//                                            'barcode'     => $request->get('barcode'),
//                                            'category_id'    => $request->get('category_id'),
//                                            'brand_id'    => $request->get('brand_id'),
//                                            'model_id'    => $request->get('model_id'),
//                                            'tenant_id'   => $request->get('tenant_id'),
//                                            'accepted'    => 0,
      );
      $report = Report::find($report_id);
  
      if ( ( $request->hasFile('image') ) && $image = $request->file('image') ) {
    
        if ( File::exists($report->image) ) {
          File::delete($report->image);
        }
        $slug     = 'reports';
        $filename = $filename = Str::random(20) . '.' . $image->getClientOriginalExtension();
        $fullPath     = tenantFolder().'/'.$slug . '/' . date('F') . date('Y') . '/';
    
        $image->storeAs(
            $fullPath,
            $filename,
            config('voyager.storage.disk', 'public')
        );
          $report->image = $fullPath;
          $report->save();
        }
      
      
      return redirect()->route("voyager.reports.index");
    } catch ( Exception $e ) {
      return back()->with([ 'error' => 'something went wrong' ]);
    }
  }
  
  /**
   * Remove the specified resource from storage.
   * DELETE /report/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy( $id ) {
    //
  }
  
}