<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Report;
use App\User;
use http\Exception;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function tenantFolder;

class UserController extends Controller {
  
  use ResetsPasswords;
  
  public function index()
  {
    $users = User::with(['brand'])->get();
    foreach ($users as $user){
      if ($user->brand) {
        $user->brand->load('subbrands');
      }
    }
    return $users;
  }
  
  public function show()
  {
    try{
      $user = User::whereId(Auth::id())->with(['brand','reports'])->first();
        $user->brand->load('subbrands');
    
        return $user;
    } catch (Exception $e){
    return collect(['message' => $e->getMessage(),
    'success' => false ]);
    }
   
  }
  
  public function tenant($id)
  {
    return User::find($id)->tenantId;
  }
  
  public function reports( )
  {
    if($user = User::find(Auth::id()))
    {
      $reports = Report::whereUserId(Auth::id())->with('subbrand')->orderBy('created_at','desc')->limit(10)->get();
      foreach ($reports as $report){
        $report->persons_injured = ($report->persons_injured == '1');
        $report->product_return = ($report->product_return == '1');
      }
      return collect([ 'data'    => collect([ 'reports' => $reports,
      ]),
                       'success' => true,
      ]);
    }
    else{
      return collect([ 'data'    => collect([]),
                       'message' => 'There were no user with that id',
                       'success' => true,
      ]);
    }
  }
  
  public function brand(  ) {
    $brand = Auth::user()->brand;
    return $brand->load('subbrands');
  }
  
  public function update(Request $request  ) {
    try{
      $validation = Validator::make($request->all(), [
          'name'     => 'required',
          'email'     => 'required|unique:users,email,'.Auth::id(),
      ]);
      if ( $validation->fails() ) {
        return collect([ 'error'   => $validation->messages(),
                         'success' => false,
        ]);
      }
      User::whereId(Auth::id())->update($request->except(['avatar','password']));
      $user = User::whereId(Auth::id())->with(['brand'])->first();
      if($request->get('password')){
        $user->password = bcrypt($request->get('password'));
        $user->save();
      }
      $user->brand->load('subbrands');
  
      return $user;
    } catch (Exception $e){
    return collect(['message' => $e->getMessage(),
    'success' => false ]);
    }
  }
  public function uploadavatar( Request $request) {
    try {
      $validation = Validator::make($request->all(), [
          'avatar'     => 'required|image',
      ]);
      if ( $validation->fails() ) {
        return collect([ 'error'   => $validation->messages(),
                         'success' => false,
        ]);
      }
      $user = User::find(Auth::id());
      if ( $request->hasFile('avatar') ) {
        if ( $request->file('avatar')->isValid() ) {
          if ( File::exists($user->avatar) ) {
            File::delete($user->avatar);
          }
        
        
          $slug     = 'users';
          $file     = $request->file('avatar');
          $filename = Str::random(20);
          $path     = tenantFolder() . '/' . $slug . '/' . date('F') . date('Y') . '/';
          $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();
//          $request->file('avatar')->storeAs($path, $filename . '.' . $file->getClientOriginalExtension(), config('voyager.storage.disk', 'public'));
          $image = Image::make($file)->encode($file->getClientOriginalExtension(), 75);
          Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public');
  
          $user->avatar = $fullPath;
          $user->save();
        }
        
      }
      $user = User::whereId(Auth::id())->with(['brand'])->first();
      $user->brand->load('subbrands');
      return $user;
    } catch ( Exception $e ) {
      return collect([ 'message' => $e->getMessage(),
                       'success' => false,
      ]);
    }
  }
  
  public function deleteavatar() {
    try{
      $user = User::find(Auth::id());
      if ( File::exists($user->avatar) ) {
        File::delete($user->avatar);
      }
    User::whereId(Auth::id())->update(['avatar'=>'']);
      return collect([ 'message' => 'There avatar was deleted',
                       'success' => true,
      ]);
    } catch (Exception $e){
    return collect(['message' => $e->getMessage(),
    'success' => false ]);
    }
  }
  public function kontakt(Request $request  ) {
      Log::info('kontakt1',$request->all());
    try{
      $user = User::find(Auth::user()->id);
      $contact_form_recipients = explode(',',DB::table('settings')->where('key','contact_form_users')->first()->value);
      $contact_form_recipients[] = $user->email;
      Mail::send('emails.contactform', [ 'user' => $user, 'subject' => $request->emne, 'text' => $request->besked],
      function (
        $message )
    use (
        $user, $contact_form_recipients ) {
      $message->from($user->email, $user->name);
      $message->to($contact_form_recipients, 'Interacoustics')
              ->subject('Interacoustics App Contact Form');
    });
    return response()->json(['status' => true])->setStatusCode(200);
  } catch(Exception $e){
return response()->json([
'error' => $e->getMessage(),
'status' => false,
    'request' => $request->all()])->setStatusCode(500);
}
}
  
  public function requestresetpasswordmail(Request $request) {
    try{
      $validation = Validator::make($request->all(), [
          'email'     => 'required|email',
      ]);
      if ( $validation->fails() ) {
        return response()->json([ 'error'   => $validation->messages(),
                                  'succes' => false ])->setStatusCode(500);
      }
      $response = $this->broker()->sendResetLink(
          ['email'=>$request->email]
      );
      return $response == Password::RESET_LINK_SENT ?
          response()->json(['succes' => true])
          :
          response()->json([
              'message' => 'something went wrong',
              'succes' => false ]);
    
    } catch(Exception $e){
      return response()->json([
          'message' => 'something went wrong',
          'succes' => false ])->setStatusCode(500);
    }
  }
  
  
}
