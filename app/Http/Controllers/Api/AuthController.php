<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Traits\ValidateCreateUsers;
use App\User;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use function response;

//use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller {
  
  use RegistersUsers, ValidateCreateUsers,SendsPasswordResetEmails;
  
  public function authenticate( Request $request )
  {
    return $this->JWTauthenticate($request);
  }
  
  public function JWTauthenticate( $request )
  {
    
    $credentials = $request->only('email', 'password');
    
    try
    {
      // attempt to verify the credentials and create a token for the user
      if ( ! $token = JWTAuth::attempt($credentials) )
      {
        return response()->json([ 'error' => 'invalid_credentials' ], 401);
      }
    } catch ( JWTException $e )
    {
      // something went wrong whilst attempting to encode the token
      return response()->json([ 'error' => 'could_not_create_token' ], 500);
    }
    
    // all good so return the token
    $user = User::whereId(Auth::id())->with('brand')->first();
  
    if(!$user->accepted){
      return response()->json([ 'error' => 'your account have not yet been approved, please contact your admin' ],
          200);
    }
    
    
    if($user->brand) {$user->brand->load('subbrands'); }
    
    
    return response()->json(compact('token', 'user'))->setStatusCode(200);
  }
  
  public function payload()
  {
    return Auth::user()->tenantID;
    
  }
  
  public function logout()
  {
//        Auth::logout();
    JWTAuth::invalidate(JWTAuth::getToken());
    $response = [ "success" => 'logout' ];
    
    return response()->json($response);
  }
  
  public function redirecttoProvider( Request $request, $provider )
  {
    return Socialite::driver($provider)->redirect();
  }
  
  public function handleProviderCallback( Request $request, $provider )
  {
    $user = Socialite::driver($provider)->user();
    
    $authUser = $this->findOrCreateUser($user, $provider);
    Auth::login($authUser, true);
    
    return redirect('/');
    
  }
  
  private function findOrCreateUser( $user, $provider )
  {
    if ( $authUser = User::where('provider_id', $user->id)->first() )
    {
      $authUser->update([ 'provider_token' => $user->token,
      ]);
      
      return $authUser;
    }
    
    if ( $authUser = User::where('email', $user->email)->first() )
    {
      $authUser->update([ 'provider'       => $provider,
                          'provider_id'    => $user->id,
                          'provider_token' => $user->token,
      ]);
      
      return $authUser;
    }
    
    return User::create([ 'name'           => $user->name,
                          'email'          => $user->email,
                          'provider'       => $provider,
                          'provider_id'    => $user->id,
                          'provider_token' => $user->token,
    ]);
  }
  
  public function loginOrCreateWithProvider( Request $request, $provider )
  {
    $user     = Socialite::driver($provider)->userFromToken($request->get('token'));
    $authUser = $this->findOrCreateUser($user, $provider);
    Auth::login($authUser, true);
    
    if ( $request->get('password') )
    {
      Auth::user()->update([ 'password' => bcrypt($request->get('password')),
      ]);
    }
    
    $token        = JWTAuth::fromUser(Auth::user());
    $user         = Auth::user()->with(array( 'role' => function ( $query ) {
      $query->addSelect([ 'id', 'name', 'display_name' ]);
    },
    ))->find(Auth::id())->makeHidden(array( 'role_id',
                                            'fbid',
                                            'created_at',
                                            'updated_at',
    ));
    $has_password = $user->password ? true : false;
    $statusCode   = 200;
    $response     = compact('token', 'user', 'has_password');
    
    return response()->json($response, $statusCode);
  }
  
  protected function registered( Request $request, $user )
  {
    $user->avatar = tenantFolder().'/'.$user->avatar;
    $user->save();
    if(!$user->accepted){
      return response()->json([ 'message' => 'your request have been registered, please contact your admin for
      approval','success' => true ],
          200);
    }else{
      return response()->json([ 'success' => true ],
          200);
    }
    
    return $this->JWTauthenticate($request);
  }
  public function register(Request $request)
  {
    Log::info($request->all());
    $validation = $this->validator($request->all());
    if ( $validation->fails() ) {
      return response()->json([ 'success' => false,
                                'errors' => $validation->messages() ]);

    }
//    $this->validator($request->all())->validate();
    
    event(new Registered($user = $this->create($request->all())));
    
//    $this->guard()->login($user);
    
    return $this->registered($request, $user);

  }
  
  public function requestResetPasswordMail(Request $request)
  {
    try{
      $validation = Validator::make($request->all(), ['email' => 'required|email']);
      if ( $validation->fails() ) {
        return response()->json([ 'errors' => $validation->messages() ]);
    
      }
      $response = $this->broker()->sendResetLink(
          $request->only('email')
      );
      return $response == Password::RESET_LINK_SENT ?
          true
          :
          false;
      
    } catch(Exception $e){
      Log::error('requestResetPasswordMail',[$e->getMessage()]);
      return collect([
          'message' => 'something went wrong',
          'success' => false]);
    }
  }
  public function sendrequestbrandchange(Request $request) {
    try{
      $user = User::find(Auth::id());
      $brand = Brand::find($request->brand_id);
      Log::info($user->email);
      $recip = DB::table('settings')->where('key','request_for_brand')->first()->value;
      Log::info($recip);
      $recipients = [];
      if($recip != ''){
      $recipients = explode(',',$recip);
    }
      if(count($recipients) == 0){
        $recipients[] = config('app.brandchangerecipient');
      }
  Log::info('recip',$recipients);
      Mail::send('emails.request_brand_change', ['user'=> $user, 'brand' => $brand], function ( $message ) use (
          $user, $recipients )
      {
        $message->from($user->email, $user->name);
        $message->to($recipients, 'Interacoustics Incident Report')
                ->subject('Interacoustics Incident Report - Brand Change');
      });
      return response()->json([ 'message' => "Your Request had been sent",'success' => true ],
          200);
    } catch(Exception $e){
      Log::error('sendrequestbrandchange',[$e->getMessage()]);
      return collect([
        'error' => $e->getMessage(),
          'message' => 'something went wrong',
          'success' => false]);
    }
  }
}
