<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\Http\Controllers\Controller;

class BrandsController extends Controller {
  
  public function index()
  {
    if($brands = Brand::with('subbrands')->get())
    {
      return collect([ 'data'    => collect([ 'brands' => $brands,
      ]),
                       'success' => true,
      ]);
    }
    else
    {
      return collect([ 'data'    => collect([ 'brand' => collect([]),
      ]),
                       'message' => 'There were no brands to find',
                       'success' => false,
      ]);
    }
  }
  
  public function show( $id )
  {
    if ( $brand = Brand::whereId($id)->with('subbrands')->first() )
    {
      return collect([ 'data'    => collect([ 'brand' => $brand,
      ]),
                       'success' => true,
      ]);
    }
    else
    {
      return collect([ 'data'    => collect([ 'brand' => collect([]),
      ]),
                       'message' => 'There were no Brand by that Id',
                       'success' => false,
      ]);
    }
  }
  
  public function subbrands( $id )
  {
    if ( $brand = Brand::whereId($id)->with('subbrands')->first() )
    {
      
      if ( ( $subbrands = $brand->subbrands ) && ( count($subbrands) != 0 ) )
      {
        return collect([ 'data'    => collect([ 'subbrands' => $subbrands,
        ]),
                         'success' => true,
        ]);
      }
      else
      {
        return collect([ 'data'    => collect([ 'subbrands' => collect([]),
        ]),
                         'message' => 'There are no subbrands attached to that brand',
                         'success' => false,
        ]);
      }
    }
    else
    {
      return collect([ 'data'    => collect([ 'subbrands' => collect([]),
      ]),
                       'message' => 'There were no Brand by that Id',
                       'success' => false,
      ]);
    }
  }
}
