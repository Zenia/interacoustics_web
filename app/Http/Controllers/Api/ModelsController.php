<?php namespace App\Http\Controllers\Api;

use App\Brand;
use App\BrandModel;
use App\Http\Controllers\Controller;

class ModelsController extends Controller {
  
  /**
   * Display a listing of the resource.
   * GET /models
   *
   * @return Response
   */
  public function index()
  {
    return collect([ 'data'    => collect([ 'models' => BrandModel::all(),
    ]),
                     'success' => true,
    ]);
  }
  
  /**
   * Show the form for creating a new resource.
   * GET /models/create
   *
   * @return Response
   */
  public function create()
  {
  
  }
  
  /**
   * Store a newly created resource in storage.
   * POST /models
   *
   * @return Response
   */
  public function store()
  {
    //
  }
  
  /**
   * Display the specified resource.
   * GET /models/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show( $id )
  {
    return collect([ 'data'    => collect([ 'model' => BrandModel::find($id),
    ]),
                     'success' => true,
    ]);
  }
  
  /**
   * Show the form for editing the specified resource.
   * GET /models/{id}/edit
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit( $id )
  {
    //
  }
  
  /**
   * Update the specified resource in storage.
   * PUT /models/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update( $id )
  {
    //
  }
  
  /**
   * Remove the specified resource from storage.
   * DELETE /models/{id}
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy( $id )
  {
    //
  }
  
  public function modelOptions()
  {
    $models = [];
    foreach ( Brand::all() as $brand )
    {
      $models[ $brand->id ] = '';
      if ( $brand->models()->count() != 0 )
      {
        foreach ( $brand->models as $model )
        {
          $models[$brand->id ] = $models[$brand->id ].'<option value="' . $model->id . '">' . $model->name . '</option>';
        }
      }
    }
    
    return $models;
  }
  
}