<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Report;
use App\Tenant as Tenant;
use http\Exception;
use Illuminate\Support\Facades\Auth;

class TenantController extends Controller {
  
  public function index()
  {
    return Tenant::all();
  }
  
  public function getBrands(  ) {
    try{
      return collect([ 'data'    => collect([ 'brands' =>    $brands = Brand::where('tenant_id',Auth::user()->tenant_id)->get(),
      ]),
                       'success' => true,
      ]);
    } catch (Exception $e){
    return response()->json(['error' => $e->getMessage(),
    'status' => false ])->setStatusCode(200);
    }
  }
}
