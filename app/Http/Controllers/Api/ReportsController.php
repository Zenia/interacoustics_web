<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Report;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use function tenantFolder;


class ReportsController extends Controller {
  
  public function index() {
    return collect([ 'data'    => collect([ 'reports' => Report::with([ 'brandId', 'subBrandId', 'userId', 'tenantId' ])
                                                               ->get(),
    ]),
                     'success' => true,
    ]);
  }
  
  public function show( $id ) {
    return collect([ 'data'    => collect([ 'report' => Report::whereId($id)->with([ 'brandId',
                                                                                     'subBrandId',
                                                                                     'userId',
                                                                                     'tenantId',
    ])->first(),
    ]),
                     'success' => true,
    ]);
  }
  
  public function setAccepted( $report_id ) {
    if ( isSuperAdmin() || Auth::user()->hasRole('administrator') ) {
      try {
        $report = Report::find($report_id);
        $text   = "You Incident Report ( " . $report->name . " ) has been accepted";
        $sender_email = DB::table('settings')->where('key','sender_email')->first()->value;
        
        Mail::send('emails.simple', [ 'report' => $report, 'text' => $text ], function ( $message ) use ( $report, $sender_email) {
          $message->from($sender_email, 'Interacoustics Incident Report');
          $message->to($report->userId->email, $report->userId->name)
                  ->subject('Interacoustics Incident Report');
          
          $report->accepted = 1;
          $report->save();
        });
        
        return collect([ 'success' => true ]);
      } catch ( Exception $e ) {
        return collect([
            'message' => $e->getMessage(),
            'success' => false,
        ]);
      }
    }
    else {
      
      return collect([ 'message' => 'you do not have permission to do this', 'success' => false ]);
    }
  }
  
  public function store( Request $request ) {
    try {
      Log::info('store',$request->all());
    $req = [];
    foreach ($request->all() as $key => $val){
      if($key != 'image') {
        $req[ snake_case($key) ] = $val;
      }
    }
    Log::info('store req',$req);
    $validation = Validator::make($req, [
        'date'        => 'required',
        'description' => 'required',
        'model'       => 'required',
//        'user_id'       => 'required|exists:users,id',
        'sub_brand_id'       => 'required|exists:sub_brands,id',
    
    ]);
    if ( $validation->fails() ) {
      return response()->json([ 'errors' => $validation->messages(),
                                'success' => false
      ]);
    }
      $report           = Report::create($req);
      $report->brand_id = Auth::user()->brand_id;
      $report->user_id = Auth::id();
      $report->save();
      return collect([
          'data'    => collect([ 'report' => $report ]),
          'success' => true,
      ]);
    } catch ( Exception $e ) {
      Log::info('store error', $e->getMessage());
      return collect([
        'error' => $e->getMessage(),
          'message' => 'something went wrong',
          'success' => false,
      ]);
    }
  }
  
  public function update( Request $request, $report_id ) {
    try {
            $request->merge(['report_id' => $report_id]);
      Log::info('store',$request->all());
      $req = [];
      foreach ($request->all() as $key => $val){
        if($key != 'image') {
          $req[ snake_case($key) ] = $val;
        }
      }
      Log::info('store req',$req);
      $validation = Validator::make($req, [
                    'report_id' => 'required|exists:reports,id',
          'date'        => 'required',
          'description' => 'required',
          'model'       => 'required',
          //        'user_id'       => 'required|exists:users,id',
          'sub_brand_id'       => 'required|exists:sub_brands,id',
    
      ]);
      if ( $validation->fails() ) {
        return response()->json([ 'errors' => $validation->messages(),
                                  'success' => false
        ]);
      }
      $report = Report::find($report_id);
      $report->update($req);
      return collect([
          'data'    => collect([ 'report' => $report ]),
          'success' => true,
      ]);
    } catch ( Exception $e ) {
      return collect([
          'message' => 'something went wrong',
          'success' => false,
      ]);
    }
  }
  
  public function uploadimage( Request $request, $report_id) {
    try {
      Log::info($request->all());
      $request->merge(['report_id' => $report_id]);
      Log::info($request->all());
      $validation = Validator::make($request->all(), [
          'report_id' => 'required|exists:reports,id',
          // 'image'     => 'required|image',
      ]);
      if ( $validation->fails() ) {
        return collect([ 'error'   => $validation->messages(),
                         'success' => false,
        ]);
      }
      $report = Report::find($request->get('report_id'));
      if ( $request->hasFile('image') ) {
        if ( $request->file('image')->isValid() ) {
          if ( File::exists($report->image) ) {
            File::delete($report->image);
          }
          
          
          $slug     = 'reports';
          $file     = $request->file('image');
          $filename = Str::random(20);
          $path     = tenantFolder() . '/' . $slug . '/' . date('F') . date('Y') . '/';
          $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();
//          $file->storeAs($path, $filename . '.' . $file->getClientOriginalExtension(), config('voyager.storage.disk',
// 'public'));
          $image = Image::make($file)->encode($file->getClientOriginalExtension(), 75);
          Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public');
          $report->image = $fullPath;
          $report->save();
        }
      }
      return collect([
          'data'    => collect([ 'report' => $report ]),
          'success' => true,
      ]);
    } catch ( Exception $e ) {
      return collect([ 'message' => $e->getMessage(),
                       'success' => false,
      ]);
    }
  }
  public function deleteimage($report_id  ) {
    try{
      $report = Report::find($report_id);
      if ( File::exists($report->image) ) {
        File::delete($report->image);
      }
      Report::whereId($report_id)->update(['image'=>'']);
      return collect([ 'message' => 'There image was deleted',
                       'success' => true,
      ]);
    } catch (Exception $e){
      return collect(['message' => $e->getMessage(),
                      'success' => false ]);
    }
  }
  
}
