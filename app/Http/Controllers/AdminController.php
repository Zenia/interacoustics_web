<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Role;
use App\User;
use http\Exception;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use function redirect;

class AdminController extends Controller
{
  
  use SendsPasswordResetEmails;
  
  
  public function sendUserResetPasswordMail($user_id  )
  {
    
    try{
      $user = User::find($user_id);
      $response = $this->broker()->sendResetLink(
          ['email'=>$user->email]
      );
      return $response == Password::RESET_LINK_SENT ?
          collect(['success' => true])
          :
          collect([
              'message' => 'something went wrong',
              'success' => false]);
      
    } catch(Exception $e){
      return collect([
          'message' => 'something went wrong',
          'success' => false]);
    }
}
  
  public function acceptrequest( $user_id  )
  {
    if($user = User::find($user_id)){
      try{
        $text = "Your request for an account has been accepted.";
        $sender_email = DB::table('settings')->where('key','sender_email')->first()->value;
    
        Mail::send('emails.simple', ['user'=> $user, 'text' => $text], function ( $message ) use ( $user , $sender_email)
        {
          $message->from($sender_email, 'Interacoustics Incident Report');
          $message->to($user->email, $user->name)//
          //          $message->to('dennisbusk@gmail.com', $member->name)
                  ->subject('Interacoustics Incident Report');
      
          $user->accepted = 1;
          $user->save();
        });
        
        return collect(['success' => true]);
      }catch(Exception $e){
        return collect([
            'message' => 'something went wrong',
            'success' => false]);
    
      }
    }
    return collect([
        'message' => 'There wer no user with that ID',
        'success' => false]);
  }
  
  public function rejectrequest( $user_id  )
  {
    if($user = User::find($user_id)){
      try{
        $text = "Your request for an account has been rejected";
        $sender_email = DB::table('settings')->where('key','sender_email')->first()->value;
        Mail::send('emails.simple', ['user'=> $user, 'text' => $text], function ( $message ) use ( $user, $sender_email )
        {
          $message->from($sender_email, 'Interacoustics Incident Report');
          $message->to($user->email, $user->name)//
          //          $message->to('dennisbusk@gmail.com', $member->name)
                  ->subject('Interacoustics Incident Report');
      
          $user->delete();
        });
        return collect(['success' => true]);
      }catch(Exception $e){
        return collect([
            'message' => 'something went wrong',
            'success' => false]);
    
      }
            }
    return collect([
        'message' => 'There wer no user with that ID',
        'success' => false]);
}
  
  public function requestbrandchange($id) {
    try{
    $brands = Brand::where('tenant_id',Auth::user()->tenant_id)->get();
      return view('vendor.voyager.users.request_brand_change', compact('brands'));
  
    } catch (Exception $e){
      return redirect()->back()->withErrors($e->getMessage());    }
}
  
  public function sendrequestbrandchange(Request $request, $id ) {
    try{
      $user = User::find($id);
      $brand = Brand::find($request->brand_id);
      $recipients = explode(',',DB::table('settings')->where('key','request_for_brand')->first()->value);
      
      Mail::send('emails.request_brand_change', ['user'=> $user, 'brand' => $brand], function ( $message ) use (
          $user , $recipients )
      {
        $message->from($user->email, $user->name);
        $message->to($recipients, 'Interacoustics Incident Report')//
        //          $message->to('dennisbusk@gmail.com', $member->name)
                ->subject('Interacoustics Incident Report - Brand Change');
      });
      return redirect('/user/users/'.Auth::id().'/edit')->with([ 'message'    => "Your Request had been sent",
                                                                 'alert-type' => 'success',
      ]);
    } catch (Exception $e){
      return redirect()->back()->withErrors($e->getMessage());
    }
  }
  
  public function index(  ) {
    try{
    $adminUsers = User::where('role_id',Role::where('name','administrator')->first()->id)->get();
    $sender_email = DB::table('settings')->where('key','sender_email')->first()->value;
    $request_for_brand_change_users = User::whereIn('email',explode(',',DB::table('settings')->where('key','request_for_brand')->first()->value))->pluck('id')->toArray();
    $contact_form_users = User::whereIn('email',explode(',',DB::table('settings')->where('key','contact_form_users')->first()->value))->pluck('id')->toArray();
    
    return view('vendor.admin.index',compact('adminUsers','request_for_brand_change_users','sender_email','contact_form_users'));
    }catch(Exception $e){
      Log::error('storeadminsettings',[$e->getMessage()]);
      return redirect()->back()
                       ->with([
                           'message' => $e->getMessage(),
                           'alert-type' => 'error',
                       ]);
    }
  }
  
  public function storeadminsettings(Request $request) {
    try{
      $request_for_brand_change_users = implode(',',User::whereIn('id',$request->get('request_for_brand_change_users'))->pluck('email')->toArray());
      DB::table('settings')->where('key','request_for_brand')->update(['value' => $request_for_brand_change_users]);
      $contact_form_users = implode(',',User::whereIn('id',$request->get('contact_form_users'))->pluck('email')->toArray());
      DB::table('settings')->where('key','contact_form_users')->update(['value' => $contact_form_users]);
      DB::table('settings')->where('key','sender_email')->update(['value' => ($request->has('sender_email')) ? $request->get('sender_email') : '']);
      return redirect()->back()
                       ->with([
                           'message' => 'Succesfully saved',
                           'alert-type' => 'success',
                       ]);
    }catch(Exception $e){
      Log::error('storeadminsettings',[$e->getMessage()]);
      return redirect()->back()
                       ->with([
                           'message' => $e->getMessage(),
                           'alert-type' => 'error',
                       ]);
    }
  }
}
