<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Request;

class ForceSsl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! Request::secure()) {
            return redirect()->secure(Request::path());
        }
        return $next($request);
    }
}
