<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;


class Report extends Model {
  
  use belongsToTenant;
  
  protected $fillable = [
	'user_id',
	'image',
	'description',
	'created_at',
	'updated_at',
	'barcode',
	'brand_id',
	'model',
	'tenant_id',
	'ref_nr',
	'sub_brand_id',
	'persons_injured',
	'product_return',
	'date',
	'steps',
	'accesories',
	'firmware',
	'database',
	'dateofpurchase',
  'costumer_name',
  'costumer_email',
  'costumer_address',
  'suite_version'
  ];
  
  public function brandId()
  {
    return $this->belongsTo('App\Brand', 'brand_id', 'id');
  }
  
  public function modelId()
  {
    return $this->belongsTo('App\BrandModel', 'model_id', 'id');

  }
  
  public function userId()
  {
    return $this->belongsTo('App\User', 'user_id', 'id');
  }
  
  public function subBrandId(  ) {
    return $this->hasOne('App\SubBrand','id','sub_brand_id');
  }
  
  public function getUserId(  ) {
    return Report::find($this->id)->user_id;
  }
  
  public function subbrand(  ) {
    return $this->subBrandId();
  }
  
}
