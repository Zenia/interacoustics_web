<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models as VoyagerModel;
use TCG\Voyager\Models\Translation as VoyagerTranslation;

class Translation extends VoyagerTranslation
{
  use belongsToTenant;
  
}
