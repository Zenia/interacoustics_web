<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;


class SubBrand extends Model
{
use belongsToTenant;
  protected $hidden =[
      'tenant_id'
  ];

  
  public function reports(  ) {
    return $this->belongsToMany('App\Report','reports');
  }
  public function brands(){
    return $this->belongsToMany(Brand::class,'brand_sub_brand');
  }
}
