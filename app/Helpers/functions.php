<?php

use App\Tenant;
use App\TenantApiSetting;
use Illuminate\Support\Facades\Auth;


function nameForTenants( $name){
  if( strpos( $name, '/' ) !== false )
  {
    return rtrim(substr($name, strrpos($name, '/') + 1));
  }
  else{
    return $name;
  }
}
function tenantFolder(){
  
    $tenant_id = (Auth::check()) ? Auth::user()->tenant_id : 1 ;
    $tenant_name = (Auth::check()) ? str_slug(Auth::user()->tenantId->name) : str_slug(Tenant::find($tenant_id)->name) ;
  
  return  ''. $tenant_id . '-' . $tenant_name .'';
}
function isSuperAdmin(){
  return Auth::user()->hasRole('superadmin');
}

/**
 * @param $category
 *
 * @return array
 */
function setCatUrlPath($data){
  foreach ( $data as $cat )
  {
    if ( !isset($cat->api_url) || $cat->api_url === "" )
    {
      $cat->api_url = 'categories/' . $cat->id . '/posts';
    }
  }
  return $data;
}
function setPostUrlPath($data){
  foreach ( $data as $content )
  {
    if ( ! isset($content->api_url) || $content->api_url === "" )
    {
      $content->api_url = 'custom/' . $content->id . '/posts';
    }
  }
  return $data;
}

function getFields( $id )
{
  $fields = TenantApiSetting::where('category_id', $id)->pluck('fields');

  if(count($fields) != 0){
    $fields = explode(',', trim($fields, '\[\]\"'));
    $fields = array_merge($fields, [ 'id', 'tenant_id', 'author_id', 'category_id','api_url','latitude','longitude']);
    $fields = array_unique($fields);
    $fields = array_filter($fields);
  }
  else{
        $fields = [ 'id', 'tenant_id', 'author_id', 'category_id','title','excerpt','address_line1','address_line2',
                    'address_phone','timings_weekdays','timings_saturday','timings_sunday','body','latitude','longitude' ];
    }
return $fields;
}
function getLongAndLatFromAddress($address){

$address = $address.', Denmark';
$prepAddr = str_replace(' ','+',$address);

$geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
$output= json_decode($geocode);

  $coords = [
      'long' => $output->results[0]->geometry->location->lng,
      'lat' => $output->results[0]->geometry->location->lat
  ];
return $coords;
}
function getDanishMonth($number){
  $months =  array("Jan","Feb","Mar","Apr","Maj","Jun","Jul","Aug","Okt","Sep","Nov","Dec");
  return $months[$number];
}
function getDanishWeekday($number){
  $days = array('Man','Tirs','Ons','Tors','Fre','Lør','Søn');
  return $days[$number];
}