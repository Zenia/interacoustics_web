<?php

namespace App;

use App\Notifications\AdminResetPasswordNotification;
use App\Traits\belongsToTenant;
use Illuminate\Notifications\Notifiable;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\User as VoyagerUser;

class User extends VoyagerUser
{
    use Notifiable, belongsToTenant;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','tenant_id','company','address','country','role_id','accepted','brand_id','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];
  
  public function reports()
  {
    return $this->hasMany(Report::class);
  }
  
  public function brandId(  ) {
    return $this->belongsTo('App\Brand','brand_id','id');
  }
  
  public function brand(  ) {
    return $this->brandId();
  }

  
  public function tenant(  )
  {
    return $this->tenantId();
  }
//
////  public function role(  )
////  {
////    return $this->roleId();
////  }
//  public function hasRole($name)
//  {
//    if (!$this->relationLoaded('roleId')) {
//      $this->load('roleId');
//    }
//
//    return in_array($this->roleId->name, (is_array($name) ? $name : [$name]));
//  }
}
