<?php

namespace App;


class Admin extends User
{
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new AdminResetPasswordNotification($token));
  }
}
