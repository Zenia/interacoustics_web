<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class UserDimmer extends AbstractWidget {
  
  /**
   * The configuration array.
   *
   * @var array
   */
  protected $config = [];
  
  /**
   * Treat this method as a controller action.
   * Return view() or other content to display.
   */
  public function run()
  {
    if ( isSuperAdmin() || Auth::user()->hasRole('administrator') )
    {
      if ( ! isSuperAdmin() )
      {
        $count = Voyager::model('User')->where('tenant_id', Auth::user()->tenant_id)->where('role_id','!=',1)->count();
      }
      else
      {
        $count = Voyager::model('User')->count();
      }
      $string = $count == 1 ? 'user' : 'users';
      
      return view('voyager::dimmer', array_merge($this->config, [ 'icon'   => 'voyager-group',
                                                                  'title'  => "{$count} {$string}",
                                                                  'text'   => "You have {$count} {$string} in your database. Click on button below to view all users.",
                                                                  'button' => [ 'text' => 'View all users',
                                                                                'link' => route('voyager.users.index'),
                                                                  ],
                                                                  'image'  => voyager_asset('images/widget-backgrounds/02.png'),
      ]));
    }
  }
}
