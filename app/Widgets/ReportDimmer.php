<?php

namespace App\Widgets;

use App\Report;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class ReportDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
      if ( ! isSuperAdmin() )
      {
        if(!Auth::user()->hasRole('administrator'))
        {
          $count = Auth::user()->reports()->count();
  
        }else{
          $count = Report::where('tenant_id', Auth::user()->tenant_id)->count();
        }
      }
      else
      {
        $count = Report::count();
      }
        $string = $count == 1 ? 'report' : 'reports';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => "You have {$count} {$string} in your database. Click on button below to view all reports.",
            'button' => [
                'text' => 'View all reports',
                'link' => route('voyager.reports.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.png'),
        ]));
    }
}
