<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;


class BrandModel extends Model {
  
  use belongsToTenant;
  
  public function brandId()
  {
    return $this->belongsTo('App\Brand', 'brand_id', 'id');
  }
  
  public function reports(  )
  {
    return $this->hasMany(Report::class);
  }
}
