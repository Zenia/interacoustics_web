<?php
namespace App\Traits;


trait belongsToTenant
{
  
  public function tenantId(  )
  {
    return $this->belongsTo('App\Tenant','tenant_id','id');
  }
}