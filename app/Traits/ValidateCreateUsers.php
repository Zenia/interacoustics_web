<?php
namespace App\Traits;


use App\User as User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;


trait ValidateCreateUsers {
  
  public function validator( array $data )
  {
//    dd($data);
    return Validator::make($data, [
        'name'      => 'required|max:255',
                                    'email'     => [ 'unique:users',
                                                     'required',
                                                     'email',
                                                     'max:255',
//                                                     Rule::unique('users')->whereNull(function ( $query )
//                                                     {
//                                                       $query->where('tenant_id', Input::get('tenant_id'))->where('email', Input::get('email'));
//                                                     }),
                                    ],
                                    'password'  => 'required|min:6|confirmed',
    ]);
  }
  
  public function create( array $data )
  {
    
    return User::create([ 'name'      => $data['name'],
                          'email'     => $data['email'],
                          'tenant_id' => ( Auth::check() ) ? Auth::user()->tenant_id : 1,
                          'password'  => bcrypt($data['password']),
                          'company' => (isset($data['company'])) ? $data['company'] : null,
                          'address' => (isset($data['address'])) ? $data['address'] : null,
                          'country' => (isset($data['country'])) ? $data['country'] : null,
                          'phone' => (isset($data['phone'])) ? $data['phone'] : null
    ]);
  }
}
