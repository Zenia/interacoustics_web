<?php


namespace App\Overrides;

use Doctrine\DBAL\Schema\Table as DoctrineTable;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;

class SchemaManagerOverride extends SchemaManager {
  
  public static function manager()
  {
    return DB::connection()->getDoctrineSchemaManager();
  }
  
  public static function createTable($table)
  {
    if (!($table instanceof DoctrineTable)) {
      $table = TableOverride::make($table);
    }
    
    static::manager()->createTable($table);
  }
  
}