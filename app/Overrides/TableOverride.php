<?php


namespace App\Overrides;

use Doctrine\DBAL\Schema\Comparator;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Database\Schema\Column;
use TCG\Voyager\Database\Schema\ForeignKey;
use TCG\Voyager\Database\Schema\Identifier;
use TCG\Voyager\Database\Schema\Index;
use TCG\Voyager\Database\Schema\Table;

class TableOverride extends Table {
  
  public static function make($table)
  {
    if (!is_array($table)) {
      $table = json_decode($table, true);
    }
    
    $name = Identifier::validate($table['name'], 'Table');
    
    $columns = [];
    $table['columns'][] = [
    "name" => "tenant_id",
    "oldName" => "",
    "type" => [
      "name" => "integer",
      "category" => "Numbers",
      "default" => [
        "type" => "number",
        "step" => "any",
      ],
    ],
    "length" => null,
    "fixed" => false,
    "unsigned" => true,
    "autoincrement" => false,
    "notnull" => true,
    "default" => (isset(Auth::user()->tenant_id)) ? Auth::user()->tenant_id : 0 ,
  ];
//    dd($table['columns']);
    foreach ($table['columns'] as $columnArr) {
      $column = Column::make($columnArr);
      $columns[$column->getName()] = $column;
    }
    
    $indexes = [];
    foreach ($table['indexes'] as $indexArr) {
      $index = Index::make($indexArr);
      $indexes[$index->getName()] = $index;
    }
    
    $foreignKeys = [];
    foreach ($table['foreignKeys'] as $foreignKeyArr) {
      $foreignKey = ForeignKey::make($foreignKeyArr);
      $foreignKeys[$foreignKey->getName()] = $foreignKey;
    }
    
    $options = $table['options'];
    
    return new self($name, $columns, $indexes, $foreignKeys, false, $options);
  }
  
}