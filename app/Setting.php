<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Setting as VoyagerSetting;

class Setting extends VoyagerSetting
{
  use belongsToTenant;

}
