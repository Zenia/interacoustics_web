<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Tenant extends Model {
  
  protected $table      = "tenants";
  public    $timestamps = false;
  
  public function user()
  {
    return $this->hasMany('App\User');
  }
  
  public function category(  )
  {
    return $this->hasMany('App\Category');
  }
  
  
  public function Report()
  {
    return $this->hasMany('App\Report');
  }
  
  
  public function Brand()
  {
    return $this->hasMany('App\Brand');
  }
  
  
  public function Model()
  {
    return $this->hasMany('App\Model');
  }
  
  
  public function BrandModel()
  {
    return $this->hasMany('App\BrandModel');
  }

 }