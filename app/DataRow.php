<?php

namespace App;

use App\Traits\belongsToTenant;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\DataRow as VoyagerDataRow;

class DataRow extends VoyagerDataRow
{
  use belongsToTenant;
    protected $table = 'data_rows';

    protected $guarded = [];

    public $timestamps = false;
}
