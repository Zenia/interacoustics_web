@extends( (isSuperAdmin() || Auth::user()->hasRole('administrator')) ? 'voyager::master' : 'layouts.app'  )
@push('css')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @if(!isSuperAdmin() && !Auth::user()->hasRole('administrator'))
    <link href="{{ asset('css/create.css') }}" rel="stylesheet">
    @endif
@endpush

@section('page_header')
  <h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i> @if(isset($dataTypeContent->id)){{ 'Rediger' }}@else{{ 'New' }}@endif {{ $dataType->display_name_singular }}
  </h1>
@stop

@section('content')
  <div class="page-content container-fluid">
    @include('resources.views.vendor.voyager.alerts')
    <div class="row">
        @if( isSuperAdmin() || Auth::user()->hasRole('administrator'))
          <div class="col-md-6 col-md-offset-3 web-container">

            @else
              <div class="col-md-4 col-md-offset-4  web-container">
                @endif
        <form class="form-edit-add" role="form" action="@if($dataTypeContent->id){{ route('report.update', $dataTypeContent->id) }}@else{{ route('report.store') }}@endif" method="POST" enctype="multipart/form-data">
          @if(isset($dataTypeContent->id))
            {{--{{ method_field("PUT") }}--}}
            @else
            <input type="hidden" id="user_id" name="user_id" value="{{Auth::id()}}">
          @endif
            <input type="hidden" id="tenant_id" name="tenant_id"
                   value="@if(isset($dataTypeContent->tenant_id)){{ old('tenant_id', $dataTypeContent->tenant_id) }}@else{{Auth::user()->tenant_id}}@endif">
          {{ csrf_field() }}
            @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
  
            @if(isset($dataTypeContent->image))
              <img src="{{ Voyager::image( $dataTypeContent->image ) }}" style="width:100%" />
            @endif
            <div class="form-group">
              <div class="input-group">
                <input type="text" class="form-control" readonly>
                <label class="input-group-btn">
                    <span class="btn btn-primary form-control notMe">
                      <i class="voyager-camera"></i> Browse <input type="file" name="image" id="image" style="display: none;">
                    </span>
                </label>
              </div>
              </div>
  
            <div class="form-group">
              <label for="barcode">Stregkode</label>
              <input type="text" class="form-control" id="barcode" name="barcode"
                     placeholder="barcode"
                     value="@if(isset($dataTypeContent->barcode)){{ $dataTypeContent->barcode }}@endif">
            </div>
  
            <div class="form-group">
              <label for="category_id">Complaint Category</label>
              <select class="form-control" name="category_id"
                      placeholder="Category" id="category_id"
                      value="@if(isset($dataTypeContent->category_id)){{ old('category_id', $dataTypeContent->category_id) }}@else{{old('category_id')}}@endif"
                      required>
                @foreach(App\Category::all() as $category)
                  <option value="{{$category->id}}"
                          @if(isset($dataTypeContent->category_id) && ($category->id == $dataTypeContent->category_id))
                          selected="selected"
                      @endif
                  >{{$category->name}}</option>
                @endforeach
              </select>
            </div>
            
            
            
            <div class="form-group">
              <label for="brand_id">Brand</label>
              <select class="form-control" name="sub_brand_id"
                      placeholder="Brand" id="sub_brand_id"
                      value="@if(isset($dataTypeContent->sub_brand_id)){{ old('sub_brand_id', $dataTypeContent->sub_brand_id) }}@else{{old('sub_brand_id')}}@endif"
              required>
                @foreach(App\SubBrand::all() as $sub_brand)
                  <option value="{{$sub_brand->id}}"
                  @if(isset($dataTypeContent->sub_brand_id) && ($sub_brand->id == $dataTypeContent->sub_brand_id))
                    selected="selected"
                    @endif
                  >{{$sub_brand->name}}</option>
                @endforeach
              </select>
            </div>
  
            <div class="form-group">
              <label for="model">Model</label>
              <input class="form-control" name="model"
                      placeholder="Model" id="model"
                      value="@if(isset($dataTypeContent->model)){{ old('model', $dataTypeContent->model) }}@else{{old('model')}}@endif" required>
            </div>
            
            <div class="form-group">
              <label for="description">Beskrivelse</label>
              <textarea class="form-control" name="description" required>@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
            </div>
              <input type="submit" class="btn btn-primary btn-block save" value="Submit">
        </form>
          
      </div>
    </div>
    </div>
  </div>
@stop

@section('javascript')
  <script>
    var models;
    $.ajax({
      type: 'GET',
      url: document.location.origin + '/api/modelOptions',
      success: function (data) {
  models = data;
  console.log(models);
      }
    });
    $('#brand_id').on('change',function(){
      var select = $(this),
      val = select.val(),
          modelSelect = $('#model');
      modelSelect.html(models[val]);
    });

    $(document).on('change', ':file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
    $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
          input.val(log);
        } else {
          if( log ) alert(log);
        }

      });
    });
  </script>
@endsection
